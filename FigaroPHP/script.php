function r($a) {
$c = count($a) -1;
return $a[rand(0, $c)];
}
function comment_figaro_generator() {
	$utilisateurs = array ("ouvrier", "paysan", "patron de ma PME", "chef d'entreprise", "expatrié en Suisse", "militaire");
	$gentils_pluriel = array ("les bons français", "les vrais français", "les français de souche", "les honnêtes français", "les vrais patriotes", "les français qui aiment leur pays");
	$gentils_singulier = array ("Nicolas Sarkozy", "Jean-François Copé", "Nadine Morano", "Alain Juppé", "Marine Le Pen", "Claude Guéant", "Patrick Balkany", "Eric Zemmour", "Alain Soral", "De Gaulle", "Jacques Chirac", "François Fillon");
	$nous = array ("la France qui se lève tôt", "la France qui bosse", "la vraie France", "la France chrétienne", "la France traditionelle");
	$gentils_groupes = array ("de l'UMP", "du FN", "du MEDEF", "de ma PME");
	$mechants_pluriel = array ("les gauchistes", "les bobos", "les gauchos", "les arabes", "les français de papiers", "les assistés", "les chômeurs", "les bien-pensants", "les musulmans", "les gauchos bobos");
	$mechants_singulier = array ("Hollande", "Taubira", "Royal", "Mohammed", "l'immigré", "la france qui fout rien", "la racaille", "la banlieue");
	$mechants_groupes = array ("du PS", "de la LICRA", "du gouvernement", "de la gauche");

	$intro = array (
		'BRAVO!!! On voit où part l’argent de '.r($nous).' !! Pour aider '.r($mechants_pluriel).' et '.r($mechants_pluriel).' grâce aux idiots '.r($mechants_groupes),
		'En tant que '.r($utilisateurs).', ça me fait mal de voir ce qui arrive à '.r($gentils_singulier),
		'En tant que '.r($utilisateurs).', je n’aime pas voir à quel point on persécute '.r($gentils_pluriel),
		'Je suis '.r($utilisateurs).' et la position '.r($mechants_groupes).' me dégoute profondément',
		'Je suis '.r($utilisateurs).' et je soutiens les idées '.r($gentils_groupes).' dans cette histoire'
	);
	$conclusion = array (
		' VIVEMENT que '.r($gentils_singulier).' vienne faire le ménage !!!!!!',
		r($gentils_singulier).' VITE!!!',
		' RAS LE BOL DE '.r($mechants_singulier).' !',
		' '.r($gentils_singulier).' 2017!!!!!!! ON EN A BESOIN !!!'
	);
	return  r($intro).r($conclusion);
}
echo comment_figaro_generator();
