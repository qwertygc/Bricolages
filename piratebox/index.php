<?php
session_start();
if(!is_dir('data')) {
	mkdir('data');
}
if(!is_dir('data/files')) {
	mkdir('data/files');
}
if(!file_exists('data/chat.json')) {
	file_put_contents('data/chat.json', json_encode(array()));
}
else {
	$messages = json_decode(file_get_contents('data/chat.json'), true);
}
function int_to_alph($int) {
	$alph = '';
$chrs = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '-', '_');
	$base = sizeof($chrs);
	do {
		$alph = $chrs[($int % $base)] . $alph;
	}
	while($int = intval($int / $base));
	return $alph;
}
function Size($path) { #http://stackoverflow.com/questions/5501427/php-filesize-mb-kb-conversion
    $bytes = sprintf('%u', filesize($path));
    if ($bytes > 0) {
        $unit = intval(log($bytes, 1024));
        $units = array('B', 'KB', 'MB', 'GB');
        if (array_key_exists($unit, $units) === true) {
            $size = sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
        }
    }

    return array('bytes'=>$bytes, 'size'=>$size);
}
$blacklist_mimetype = array (
	'text/html',
	'text/javascript',
	'text/x-javascript',
	'application/x-shellscript',
	'application/x-php',
	'text/x-php',
	'text/x-python',
	'text/x-perl',
	'text/x-bash',
	'text/x-sh',
	'text/x-csh',
	'text/scriptlet',
	'application/x-msdownload',
	'application/x-msmetafile'
);

if(isset($_POST['pseudo'])) {
	$_SESSION['pseudo'] =  $_POST['pseudo'];
}
if(isset($_POST['message'])) {
	$messages[] = array('hash'=>sha1($_SERVER['REMOTE_ADDR']), 'pseudo' => $_POST['pseudo'], 'date'=>time(), 'message'=>$_POST['message']);
	file_put_contents('data/chat.json', json_encode($messages));
}
if(isset($_POST['upload'])) {
	$files = array();
	$fdata = $_FILES['file'];
	$count = count($fdata['name']);
	if(is_array($fdata['name'])){
		for($i=0;$i<$count;++$i){
			$files[]=array(
				'name'     => int_to_alph(time()).'_'.$fdata['name'][$i],
				'tmp_name' => $fdata['tmp_name'][$i],
				'type' => mime_content_type($fdata['tmp_name'][$i]),
			);
		}
	}
	else {$files[]=$fdata;}
	foreach ($files as $file) {
		if(!is_uploaded_file($file['tmp_name'])) {exit('error');}
		if(in_array($file['type'],$blacklist_mimetype)) { exit('bad mime type');}
		if(!move_uploaded_file($file['tmp_name'], 'data/files/'.$file['name'])) {exit('media.error.cantcreatefile');}
	}
}
?>
<!doctype html>
<html>
	<head>
	<title>Piratebox</title>
	<link href="app/assets/style.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
	<header>
		<h1>🏴‍☠️ Piratebox</h1>
	</header>
<?php
?>
	<section id="upload">
		<h1>Upload</h1>
		<form method="post" enctype="multipart/form-data">
		<input type="file" name="file[]" size="30" multiple="multiple" />
		<input type="submit" name="upload" value="Upload"/>
		</form>
		<table>
			<tr>
				<th>Name</th>
				<th>Last Modified</th>
				<th>Size</th>
				<th>Type</th>
			</tr>
		<?php
		foreach(scandir('data/files') as $content) {
			if(!in_array($content,array('.','..', 'index.html'))) {
				echo '<tr>
						<td><a href="data/files/'.$content.'">'.$content.'</a></td>
						<td>'.date('c',filemtime('data/files/'.$content)).'</td>
						<td>'.Size('data/files/'.$content)['size'].'</td>
						<td>'.mime_content_type('data/files/'.$content).'</td>
					</tr>';
			}
		}
		?>
		</table>
	</section>
	<section id="chat">
		<h1>Chat</h1>
		<div id="messages">
		</div>
		<form method="post" class="message-post">
			<input type="text" name="pseudo" id="pseudo" value="<?php echo (isset($_SESSION['pseudo']) ? $_SESSION['pseudo'] : 'anonymous'); ?>"/>
			<textarea id="message" name="message" placeholder="Type message..."/></textarea>
			<input type="submit" name="submit" id="submit" value="send"/>
		</form>
	</section>


	<section id="about">
	<h1>About</h1>
	<p>Inspired by pirate radio and the free culture movement, PirateBox is a self-contained mobile collaboration and file sharing device. PirateBox utilizes Free, Libre and Open Source software (FLOSS) to create mobile wireless file sharing networks where users can anonymously share images, video, audio, documents, and other digital content.</p>
	</section>
<script>
var input = document.getElementById("message");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById("submit").click();
	document.getElementById("messages").scrollTop = document.getElementById("messages").scrollHeight;
  }
});
setInterval(function() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4){
			document.getElementById('messages').innerHTML = xhr.responseText;
		}
	};
	xhr.open('GET', 'chat.php');
	xhr.send();
}, 500);
</script>

</body>
</html>
