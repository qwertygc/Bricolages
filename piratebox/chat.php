<?php
$messages = json_decode(file_get_contents('data/chat.json'), true);
	foreach($messages as $message) {
		echo '<div class="message">';
		echo '<img src="app/identicon.php?size=60&hash='.$message['hash'].'" class="avatar"/>';
		echo $message['pseudo'];
		echo '<p>'.$message['message'].'</p>';
		echo '<time>'. date('j M H:i',$message['date']).'</time>';
		echo '</div>';
	}
?>
