<!doctype html>
<html lang=fr>
	<head>
		<title><?php echo SITE_TITLE; echo title($title); ?></title>
		<link rel=stylesheet href="<?php echo ROOT; ?>/static/style.css"/>
		<meta charset="utf-8"/>
		<link href="<?php echo ROOT; ?>/feed/feed.xml" type="application/atom+xml" rel=alternate title="ATOM Feed" />
	</head>
	<body>
		<header id=header role=banner>
			<h1><a href="<?php echo ROOT; ?>"><?php echo SITE_TITLE; ?></a></h1>
		</header>
		<main id=main role=main>