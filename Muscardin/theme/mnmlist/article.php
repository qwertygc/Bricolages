			<article>
				<header>
					<h1><?php echo $title; ?></h1>
					Publié le <time datetime="<?php echo $datetime; ?>"><?php echo $time; ?></time> par <span rel=author><a href="author/<?php echo $author; ?>.html"><?php echo $author; ?></a></span>
					<?php echo $tags; ?>
				</header>
					<?php echo $content; ?>
			</article>