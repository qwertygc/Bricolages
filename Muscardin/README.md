# Comment l'utiliser ?

Installez le contenu du logiciel dans votre station de travail locale.
Dans le terminal, tapez php build.php. Cela va générer l'installation. Ouvrez le fichier config/config.php et modifiez le selon vos besoins
Ecrivez vous articles en markdown en respectant la syntaxe suivante :

```
---
title: Votre titre
author: Vous
date: YYYY-MM-DD ou YYYY-MM-DD HH:mm:ss
tag: @un_tag @unautretag @encore_un_tag
---
Votre article en markdown

```

Ce fichier doit se trouver dans posts/. Les fichiers associés (images, vidéos) dans posts/static.
Ouvrez le terminal et retapez php build.php. Vos fichiers seront « compilé » et envoyé sur le serveur. Ouvrez votre navigateur : votre blog s'y trouve !
# Installation d'une station de travail

Il se peut que vous n'avez pas de serveur *AMP sous la main. Voici comment en installer un sur votre ordinateur.

## Installation sous linux
Si ce n'est pas fait, il faut installer apache et PHP :
```sudo aptitude install apache2 php5 libapache2-mod-php5 php5-curl```

Maintenant, on va téléchargez le .zip du projet puis le deziper dans le répertoire de travail :

```
wget -v http://champlywood.dzv.me/muscardin/muscardin.zip
sudo unzip muscardin.zip -d < votrerépertoire >
```

# Installation sous Windows
Il suffit d'installer [Wamp Server](http://www.wampserver.com/) et de mettre dans le dossier C:\wamp\www\votre-blog le script une fois dezipé. 
