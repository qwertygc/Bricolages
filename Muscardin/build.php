<?php
#######################################
# Ce script permet d’écrire un blog statique.
# * Ecrire dans posts/ en markdown. 
# * Dans posts/static se trouve les fichiers à insérer (images, ce genre de chose)
# * Exécuter build.php
# * Votre blog est dans output/
#######################################
header('Content-Type: text/html; charset=UTF-8');
error_reporting(-1);
function md_dir($dir) {
	if(!is_dir($dir)) { mkdir($dir,0755);}
	return $dir;
}
function title($title) {
	if($title != null) {
		echo ' – '.$title;
	}
}
function metadata($file) {
	$metadata = file_get_contents($file);
	foreach (explode("\n", $metadata) as $line) {
		if (strpos($line,": ") !== false) {
			$lineArray = explode(": ", $line, 2);
			$metadataArray[$lineArray[0]] = is_numeric($lineArray[1]) ? intval($lineArray[1]) : $lineArray[1];
		}
	}
	return $metadataArray;
}
function minify_css($text) {
   $text = trim($text);
   $text = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $text);
	$text = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    ', '{ ', ' }', '; ', ', ', ' {', '} ', ': ', ' ,', ' ;'), array('', '', '', '', '', '', '','{', '}', ';', ',', '{', '}', ':', ',', ';'), $text);
   return $text;
}
function minify_html($text) {
    $text = preg_replace(array('/\>[^\S ]+/s','/[^\S ]+\</s', '/ {2,}/', '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'), array('>','<', ' ', ''), $text);
	$text = trim($text);
    return $text;
}
function stripAccents($string) {
	// $string = html_entity_decode($string);
	$string = strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ !;,:','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY-----');
	return trim($string);
}
function write_article($file) {
	$file = basename($file, EXT);
	ob_start();
	$data = explode('---', file_get_contents(POSTS_ROOT.'/'.$file.EXT));
	$metadata = (empty($data[0])) ? $data[1] : $data[1];
	$content = (empty($data[0])) ? $data[2] : $data[0];
	file_put_contents('tmp/'.$file.'.frontpage', "---\n".$metadata."\n---");
	file_put_contents('tmp/'.$file.'.md', $content);
	$md = New ParsedownExtra();
	$frontpage = metadata('tmp/'.$file.'.frontpage');
	$title = (!empty($frontpage['title'])) ? $frontpage['title'] : 'Untitled';
	$author = (!empty($frontpage['author'])) ? $frontpage['author'] : AUTHOR_NAME;
	$time = (!empty($frontpage['date'])) ? utf8_encode(strftime('%A %d %B %Y', strtotime($frontpage['date']))) : utf8_encode(strftime('%A %d %B %Y', time()));
	$tag = (!empty($frontpage['tag'])) ? explode('@', $frontpage['tag']) : null;
	$datetime = (!empty($frontpage['date'])) ? date('c', strtotime($frontpage['date'])) : date('c', time());
	$tags = null;
	$count = count($tag);
	for ($numero = 0; $numero < $tag; ++$numero) {
		$tags .= '<a href="tag/'.stripAccents($tag[$numero]).'.html" rel="tag">'.trim($tag[$numero]).'</a> ';
	}
	unset($count);
	unset($numero);
	$content = $md->text(file_get_contents('tmp/'.$file.'.md'));
	include 'theme/'.THEME.'/header.php';
	include 'theme/'.THEME.'/article.php';
	include 'theme/'.THEME.'/footer.php';
	$page = ob_get_contents();
	ob_end_clean();
	file_put_contents('output/'.$file.'.html',minify_html($page));
}
function fonctionComparaison($a, $b){
    return $a['date'] < $b['date'];
}
function browse() {
	$array = array();
	$posts = scan_dir(POSTS_ROOT);
	foreach($posts as $post) {
		if($post !='static') {
			$post = basename($post, EXT);
			$fp =  metadata('tmp/'.$post.'.frontpage');
			$md = New ParsedownExtra();
			$array[$post]['file'] = $post;
			$array[$post]['date'] = (!empty($fp['date'])) ? strtotime($fp['date']) : time();
			$array[$post]['title'] = (!empty($fp['title'])) ? $fp['title'] : 'Untitled';
			$array[$post]['author'] = (!empty($fp['author'])) ? $fp['author'] : AUTHOR_NAME;
			$array[$post]['tag'] = (!empty($fp['tag'])) ? explode(' @', $fp['tag']) : null;
			$array[$post]['content'] = $md->text(file_get_contents('tmp/'.$post.'.md'));
		}
	}
	usort($array, 'fonctionComparaison');
	return $array;
}
function browse_tags($name) {
	$array = array();
	$posts = scan_dir(POSTS_ROOT);
	foreach($posts as $post) {
		$post = basename($post, EXT);
		if($post !='static') {
			$fp = metadata('tmp/'.$post.'.frontpage');
			$md = New ParsedownExtra();
			if(in_array($name,array_unique(explode('@', $fp['tag'])))) {
				$array[$post]['file'] = $post;
				$array[$post]['date'] = (!empty($fp['date'])) ? strtotime($fp['date']) : time();
				$array[$post]['title'] = (!empty($fp['title'])) ? $fp['title'] : 'Untitled';
				$array[$post]['author'] = (!empty($fp['author'])) ? $fp['author'] : AUTHOR_NAME;
				$array[$post]['tag'] = (!empty($fp['tag'])) ? explode(' @', $fp['tag']) : null;
				$array[$post]['content'] = $md->text(file_get_contents('tmp/'.$post.'.md'));
			}
		}
	}
	usort($array, 'fonctionComparaison');
	return $array;
}
function all_tags() {
	$posts = scan_dir(POSTS_ROOT);
	$list_tags = null;
	foreach($posts as $post) {
		if($post != 'static') {
			$frontpage = metadata('tmp/'.basename($post, EXT).'.frontpage');
			$list_tags .= $frontpage['tag'];
		}
	}
	$all_tags = array_filter(explode('@', $list_tags));
	return array_unique($all_tags);
}
function browse_authors($name) {
	$array = array();
	$posts = scan_dir(POSTS_ROOT);
	foreach($posts as $post) {
		$post = basename($post, EXT);
		if($post !='static') {
			$fp = metadata('tmp/'.$post.'.frontpage');
			$md = New ParsedownExtra();
			if($fp['author'] == $name) {
				$array[$post]['file'] = $post;
				$array[$post]['date'] = (!empty($fp['date'])) ? strtotime($fp['date']) : time();
				$array[$post]['title'] = (!empty($fp['title'])) ? $fp['title'] : 'Untitled';
				$array[$post]['author'] = (!empty($fp['author'])) ? $fp['author'] : AUTHOR_NAME;
				$array[$post]['tag'] = (!empty($fp['tag'])) ? explode(' @', $fp['tag']) : null;
				$array[$post]['content'] = $md->text(file_get_contents('tmp/'.$post.'.md'));
			}
		}
	}
	usort($array, 'fonctionComparaison');
	return $array;
}
function all_authors() {
	$posts = scan_dir(POSTS_ROOT);
	$list_author = array();
	foreach($posts as $post) {
		if($post != 'static') {
			$frontpage = metadata('tmp/'.basename($post, EXT).'.frontpage');
			$list_author[] = $frontpage['author'];
		}
	}
	return array_unique($list_author);
}
function write_index($articles, $file='index.html', $title=null) {
	ob_start();
	include 'theme/'.THEME.'/header.php';
	if(isset($title)) {echo '<h1>'.$title.'</h1>';}
	echo '<ul class="list_article">';
	foreach($articles as $a) {
		if(time() >= $a['date']) {
			echo '<li><time datetime="'.date("c", $a['date']).'">'.date("d/m/Y", $a['date']).'</time> <a href="'.ROOT.'/'.$a['file'].'.html">'.$a['title'].'</a></li>';
		}
	}
	echo '</ul>';
	include 'theme/'.THEME.'/footer.php';
	$page = ob_get_contents();
	ob_end_clean();
	file_put_contents('output/'.$file, minify_html($page));
}
function write_atom($articles, $file='feed.xml', $name='Main feed') {
	ob_start();
	echo '<?xml version="1.0" encoding="utf-8"?>'.PHP_EOL;
	echo '<feed xmlns="http://www.w3.org/2005/Atom">'.PHP_EOL;
	echo "\t\t".'<title>'.SITE_TITLE.' - '.$name.'</title>'.PHP_EOL;
	echo "\t\t".'<id>tag:'.parse_url(ROOT)['host'].','.CREATE_BLOG.':blog</id>'.PHP_EOL;
	echo "\t\t".'<updated>'.date(DATE_ATOM, time()).'</updated>'.PHP_EOL;
	echo "\t\t".'<link href="'.ROOT.'"/>'.PHP_EOL;
	echo "\t\t".'<link href="'.ROOT.'/feed.xml" rel="self" type="application/atom+xml" />'.PHP_EOL;
	echo "\t\t".'<author>'.PHP_EOL;
	echo "\t\t"."\t\t".'<name>'.AUTHOR_NAME.'</name>'.PHP_EOL;
	echo "\t\t"."\t\t".'<email>'.AUTHOR_EMAIL.'</email>'.PHP_EOL;
	echo "\t\t".'</author>'.PHP_EOL;
	$i = 0;
	foreach($articles as $a) {
		if(time() >= $a['date']) {
			echo  "\t\t".'<entry>'.PHP_EOL;
			echo  "\t\t"."\t\t".'<id>tag:'.parse_url(ROOT)['host'].','.date('Y-m-d', $a['date']).':'.$a['file'].'</id>'.PHP_EOL;
			echo  "\t\t"."\t\t".'<link rel="alternate" type="text/html" href="'.ROOT.'/'.$a['file'].'.html"/>'.PHP_EOL;
			echo  "\t\t"."\t\t".'<title>'.$a['title'].'</title>'.PHP_EOL;
			echo  "\t\t"."\t\t".'<link href="'.ROOT.'/'.$a['file'].'.html"/>'.PHP_EOL;
			echo  "\t\t"."\t\t".'<published>'.date(DATE_ATOM, $a['date']).'</published>'.PHP_EOL;
			echo  "\t\t"."\t\t".'<updated>'.date(DATE_ATOM, $a['date']).'</updated>'.PHP_EOL;
			echo  "\t\t"."\t\t".'<content type="xhtml"><div xmlns="http://www.w3.org/1999/xhtml">'.$a['content'].'</div></content>'.PHP_EOL;
			echo  "\t\t".'</entry>'.PHP_EOL;
			    if (++$i == 10) break;
		}
   }
   echo '</feed>';
   $page = ob_get_contents();
	ob_end_clean();
	file_put_contents('output/feed/'.$file.'', $page);
	chmod('output/feed/'.$file.'', 0755);   
}
function ListIn($dir, $prefix = '') {
  $dir = rtrim($dir, '\\/');
  $result = array();
    foreach (array_diff(scandir($dir), array('..', '.')) as $f) {
      if (is_dir("$dir/$f")) {
          $result = array_merge($result, ListIn("$dir/$f", "$prefix$f/"));
        } else {
          $result[] = $prefix.$f;
        }
     }

  return $result;
}
function send_ftp() {
	$ftp = ftp_connect(FTP_HOST, 21);
	ftp_login($ftp, FTP_USER, FTP_PASSWORD);
	$posts = ListIn('output/');
	$list_dir = ftp_nlist($ftp, FTP_ROOT);
	if(!in_array('static', $list_dir)) {
		$dirs = array('author', 'feed', 'feed/author', 'feed/tag', 'static', 'tag');
		foreach($dirs as $dir) {
			if(!in_array($dir, $list_dir)) {ftp_mkdir($ftp, FTP_ROOT.'/'.$dir);}
		}
	}
	foreach($posts as $post) {
		ftp_put($ftp, FTP_ROOT.'/'.$post, 'output/'.$post, FTP_BINARY);
	}
	ftp_close($ftp);
}
function erased_dir($dir) {
	if(!is_dir($dir)) {
		throw new InvalidArgumentException("$dir must be a directory");
	}
	if(substr($dir,strlen($dir)-1,1) != '/') {
		$dir .= '/';
	}
	$files = glob($dir.'*',GLOB_MARK);
	if($files) {
		foreach ($files as $file) {
			if(is_dir($file)) {
				erased_dir($file);
			} 
			else {
				unlink($file);
			}
		}
	}
	rmdir($dir);
}
function scan_dir($dir) { # source = http://stackoverflow.com/questions/11923235/scandir-to-sort-by-date-modified
    $ignored = array('.', '..', '.svn', '.htaccess');
    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);

    return ($files) ? $files : false;
}

function recurse_copy($src,$dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else {
				if(file_exists($dst.'/'.$file)) {
					if(filesize($src.'/'.$file) != filesize($dst.'/'.$file)) {
						copy($src . '/' . $file,$dst . '/' . $file); 
					}
				}
				else {
					copy($src . '/' . $file,$dst . '/' . $file);
				}
			}	
        } 
    } 
    closedir($dir); 
}
#################################################
#						Installation			#
#################################################
if(!is_dir('config/')) {
	md_dir('posts/');
	md_dir('output/');
	md_dir('config/');
	md_dir('lib/');
	file_put_contents('config/config.php', "
	<?php
	setlocale(LC_ALL, 'fr_FR.UTF8', 'fr_FR','fr','fr','fra','fr_FR@euro');
	define('SITE_TITLE', 'A average website');
	define('AUTHOR_NAME', 'John Doe');
	define('AUTHOR_EMAIL', 'john.doe@example.org');
	define('ROOT', 'http://example.org');
	define('CREATE_BLOG', '".date('Y-m-d', time())."');
	define('THEME', 'mnmlist');
	define('FTP_HOST', 'example.org');
	define('FTP_USER', 'john.doe');
	define('FTP_PASSWORD', 'love2world');
	define('FTP_ROOT', 'web/blog');
	define('EXT', '.md');
	define('POSTS_ROOT', 'posts/');
	define('FTP_ACTIVE', false);"
	);
	include 'config/config.php';
	file_put_contents('lib/Parsedown.php', file_get_contents('https://raw.githubusercontent.com/erusev/parsedown/master/Parsedown.php'));
	file_put_contents('lib/ParsedownExtra.php', file_get_contents('https://raw.githubusercontent.com/erusev/parsedown-extra/master/ParsedownExtra.php'));
	exit('Please modify config/config.php file');
}
#################################################
#				Generation						#
#################################################
$begin = microtime(TRUE);
include 'lib/Parsedown.php';
include 'lib/ParsedownExtra.php';
include 'config/config.php';
md_dir('posts/');
md_dir('posts/static/');
erased_dir('output/');
md_dir('output/');
md_dir('output/static/');
md_dir('output/tag/');
md_dir('output/feed/');
md_dir('output/feed/author/');
md_dir('output/feed/tag/');
md_dir('output/author/');
md_dir('config/');
md_dir('theme/');
md_dir('lib/');
md_dir('tmp/');
foreach(scan_dir(POSTS_ROOT) as $post) {
	if($post != 'static') {
		write_article($post);
	}
}
foreach(all_tags() as $post) {
	if($post != 'static') {
		$$post = browse_tags($post);
		write_index($$post, 'tag/'.stripAccents($post).'.html', $post);
		write_atom($$post, 'tag/'.stripAccents($post).'.xml', $post);

	}
}
foreach(all_authors() as $post) {
	if($post != 'static') {
		$$post = browse_authors($post);
		write_index($$post, 'author/'.stripAccents($post).'.html', $post);
		write_atom($$post, 'author/'.stripAccents($post).'.xml', $post);

	}
}
$articles = browse();
write_index($articles);
write_atom($articles);
recurse_copy('theme/'.THEME.'/', 'output/static/');
file_put_contents('output/static/style.css', minify_css(file_get_contents('theme/'.THEME.'/style.css')));
recurse_copy(POSTS_ROOT.'/static/', 'output/static/');
erased_dir('tmp/');
unlink('output/static/article.php');
unlink('output/static/footer.php');
unlink('output/static/header.php');
if(FTP_ACTIVE) {
	send_ftp();
	//erased_dir('output/');
}
$end = microtime(TRUE);
echo round(($end - $begin),6).' seconds';