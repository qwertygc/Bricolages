<?php
session_start();
?>
<!doctype html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<style>
:root {
  --bgcolor: white;
  --maincolor: black;
  --secondcolor: white;
}
body {
	background:var(--bgcolor);
}
footer {
	text-align:center;
}
section, header {
	margin:auto;
	margin-top:2em;
	margin-bottom:2em;
}	
.avatar {
	height:200px;
	width:200px;
	border-radius:100%;
	margin-left: auto;
  	margin-right: auto;
	margin-top:1em;
}
.link {
	display:block;
	border:3px solid var(--maincolor);
	background:var(--maincolor);
	color:var(--secondcolor);
	text-decoration:none;
	text-align:center;
	margin:1em;
	padding:1em;
}
.link:hover {
	text-decoration:none;
	color:var(--maincolor);
	background:var(--secondcolor);
}

</style>
</head>
<body class="container-lg">
<?php
include 'vendor/qwertygc/unicornsystem/sql.functions.php';
include 'vendor/qwertygc/unicornsystem/form.class.php';
include 'vendor/qwertygc/unicornsystem/misc.functions.php';
if(!file_exists('data/database.db')) {
	mkdir('data/');
	mkdir('data/uploads/');
	file_put_contents('data/index.html', '');
	file_put_contents('data/uploads/index.html', '');
	$db = sqliteconnect('data/database.db');
	sql("CREATE TABLE IF NOT EXISTS `users` (
	  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
	  `email` TEXT,
	  `password` TEXT,
	  `slug` TEXT,
	  `avatar` TEXT,
	  `bgcolor` TEXT,
	  `maincolor` TEXT,
	  `secondcolor` TEXT);");
	 sql("CREATE TABLE IF NOT EXISTS `links` (
	  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
	  `id_user` TEXT,
	  `url` TEXT,
	  `label`TEXT,
	  `clic` INT);");
}
else {
	$db = sqliteconnect('data/database.db');
}
if(isset($_GET['do'])) {
	switch ($_GET['do']) {
		case 'subscribe':
			$form = New Unicorn\form(array('method'=>'post', 'action'=>'~subscribe'));
			echo '<div class="form-group">';
			$form->label('email', 'Courriel');
			$form->input(array('type'=>'email', 'id'=>'email', 'name'=>'email', 'class'=>'form-control'));
			echo '</div><div class="form-group">';
			$form->label('password', 'Mot de passe');
			$form->input(array('type'=>'password', 'id'=>'password', 'name'=>'password', 'class'=>'form-control'));
			echo '</div><div class="form-group">';
			$form->label('slug', 'Identifiant');
			$form->input(array('type'=>'text', 'id'=>'slug', 'name'=>'slug', 'pattern'=>'[a-zA-Z0-9]+', 'class'=>'form-control'));
			echo '</div>';
			$form->input(array('type'=>'submit', 'class'=>'btn btn-primary'));
			$form->endform();
			if(isset($_POST['slug'])) {
				$query = sql('SELECT count(slug) as countslug, count(email) as countemail FROM users WhERE slug=:slug OR email=:email', array('slug'=>$_POST['slug'], 'email'=>$_POST['email']));
				$data = $query->fetch();
				print_r($data);
				if((int)$data['countslug'] == 0 AND (int)$data['countemail'] == 0) {
					sql('INSERT INTO users(email, password, slug) VALUES (?,?,?)', array($_POST['email'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST['slug']));
					redirect('~login');
				}
				else {
					echo '<p class="alert alert-warning" role="alert">Courriel ou identifiant déjà existant dans la base de données</p>';
				}
			}
			
		break;
		case 'login':
			if(isset($_SESSION['login'])) {redirect('~edit');}
			$form = New Unicorn\form(array('method'=>'post', 'action'=>'~login'));
			echo '<div class="form-group">';
			$form->label('email', 'Courriel');
			$form->input(array('type'=>'email', 'id'=>'email', 'name'=>'email', 'class'=>'form-control'));
			echo '</div><div class="form-group">';
			$form->label('password', 'Mot de passe');
			$form->input(array('type'=>'password', 'id'=>'password', 'name'=>'password', 'class'=>'form-control'));
			echo '</div>';
			$form->input(array('type'=>'submit', 'class'=>'btn btn-primary'));
			$form->endform();
			if(isset($_POST['password'])) {
				$query = sql('SELECT count(slug) as nb, password, id FROM users WHERE email=:email', array('email'=>$_POST['email']));
				$data = $query->fetch();
				if($data['nb'] == 1) {
					if(password_verify($_POST['password'], $data['password'])) {
						echo 'Yolo';
						$_SESSION['login'] = true;
						$_SESSION['user_id'] = $data['id'];
						redirect('~edit');
					}
					else {
						echo '<p class="alert alert-warning" role="alert">Mauvais mot de passe</p>';
					}
				}
				else {
					echo '<p class="alert alert-warning" role="alert">Courriel inexistant dans la base de données</p>';
				}
			}
		break;
		case 'logout':
		    unset($_SESSION['login']);
		    unset($_SESSION['user_id']);
			session_destroy();
			redirect('index.php');
			exit();
		break;
		case 'edit':
			if(!isset($_SESSION['login'])) {redirect('~login'); exit();}
			$query = sql('SELECT * FROM users WHERE id=:id', array('id'=>$_SESSION['user_id']));
			$data = $query->fetch();
			echo '<h1>Bienvenue <a href="@'.$data['slug'].'">@'.$data['slug'].'</a></h1>';
			echo '<a href="~logout">Déconnexion</a>';
			
			echo '<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="liens-tab" data-toggle="tab" href="#liens" role="tab" aria-controls="liens" aria-selected="true">Gérer ses liens</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Mettre à jour ses informations</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="personalisation-tab" data-toggle="tab" href="#personalisation" role="tab" aria-controls="personalisation" aria-selected="false">Personnalisation</a>
  </li>
</ul>';
			echo '<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="liens" role="tabpanel" aria-labelledby="liens-tab">';
			echo '<h1>Gérer ses liens</h1>';
			$form = New Unicorn\form(array('method'=>'post', 'action'=>'~edit'));
			echo '<div class="form-group">';
			$form->label('url', 'Lien');
			$form->input(array('type'=>'url', 'id'=>'url', 'name'=>'url', 'class'=>'form-control'));
			echo '</div><div class="form-group">';
			$form->label('label', 'Libellé');
			$form->input(array('type'=>'text', 'id'=>'label', 'name'=>'label', 'class'=>'form-control'));
			echo '</div>';
			$form->input(array('type'=>'submit', 'class'=>'btn btn-primary', 'value'=>'Ajouter un lien'));
			$form->endform();
			if(isset($_POST['url'])) {
				sql('INSERT INTO links(url, label, clic, id_user) VALUES (?,?,0,?)', array($_POST['url'], $_POST['label'], $_SESSION['user_id']));
				redirect('~edit');
			}
			$query = sql('SELECT url, label, id, clic FROM links WHERE id_user=?', array($_SESSION['user_id']));
			echo '<div class="card"> <ul class="list-group list-group-flush">';
			while($links = $query->fetch()) {
				echo '<li class="list-group-item"><h5 class="card-title">'.$links['label'].'</h5>('.$links['clic'].') - <a href="~edit&delink='.$links['id'].'" class="text-danger">Supprimer le lien</a><br><a href="'.$links['url'].'">'.$links['url'].'</a></li>';
			}
			echo '</ul></div>';
			if(isset($_GET['delink'])) {
				sql('DELETE FROM links WHERE id=?', array($_GET['delink']));
				redirect('~edit');
			}
			echo '</div>';
			echo '<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">';
			echo '<h1>Mettre à jour ses informations</h1>';
			$form = New Unicorn\form(array('method'=>'post', 'action'=>'~edit', 'enctype'=>'multipart/form-data'));
			echo '<div class="form-group">';
			$form->label('email', 'Courriel');
			$form->input(array('type'=>'email', 'id'=>'email', 'name'=>'email', 'class'=>'form-control', 'value'=>$data['email']));
			echo '</div><div class="form-group">';
			$form->label('password', 'Mot de passe (laisser vide pour garder le même)');
			$form->input(array('type'=>'password', 'id'=>'password', 'name'=>'password', 'class'=>'form-control'));
			echo '</div><div class="form-group">';
			$form->label('avatar', 'Photo de profil');
			$form->input(array('type'=>'file', 'id'=>'avatar', 'name'=>'avatar', 'class'=>'form-control-file'));
			echo '</div><div class="form-group">';
			$form->label('slug', 'Identifiant');
			$form->input(array('type'=>'text', 'id'=>'slug', 'name'=>'slug', 'class'=>'form-control', 'disabled'=>'disabled', 'value'=>$data['slug']));
			echo '</div>';
			$form->input(array('type'=>'submit', 'class'=>'btn btn-primary'));
			$form->endform();
			if(isset($_POST['email'])) {
				sql('UPDATE users SET email=? WHERE id=?', array($_POST['email'], $_SESSION['user_id']));
				if(isset($_FILES['avatar'])) {
					if (in_array(mime_content_type($_FILES['avatar']['tmp_name']), array ('image/gif', 'image/jpeg','image/png'))) {
						$filename = 'data/uploads/'.md5(microtime()).'_'.basename($_FILES['avatar']['name']);
						move_uploaded_file($_FILES['avatar']['tmp_name'], $filename);
						sql('UPDATE users SET avatar=? WHERE id=?', array($filename, $_SESSION['user_id']));
					}
					else {
						echo '<p class="alert alert-warning" role="alert">Une image (gif, jpg, png), pas autre chose !</p>';
					}
				}
				//if(isset($_POST['password'])) {sql('UPDATE users SET password=? WHERE id=?', array(password_hash($_POST['password'], PASSWORD_DEFAULT), $_SESSION['user_id']));}
				//redirect('~edit');
			}
		echo '<hr>';
		echo '<a href="~edit&deleteaccount=true" class="text-danger">Supprimer mon compte</a>';
		echo '</div>';
		echo '<div class="tab-pane fade" id="personalisation" role="tabpanel" aria-labelledby="personalisation-tab">';
		echo '<h1>Personnalisation</h1>';
		
			$form = New Unicorn\form(array('method'=>'post', 'action'=>'~edit'));
			echo '<div class="form-group">';
			$form->label('bgcolor', 'Couleur de fond');
			$form->input(array('type'=>'color', 'id'=>'bgcolor', 'name'=>'bgcolor', 'class'=>'form-control', 'value'=>$data['bgcolor']));
			echo '</div><div class="form-group">';
			$form->label('maincolor', 'Couleur principale');
			$form->input(array('type'=>'color', 'id'=>'maincolor', 'name'=>'maincolor', 'class'=>'form-control', 'value'=>$data['maincolor']));
			echo '</div><div class="form-group">';
			$form->label('secondcolor', 'Couleur secondaire');
			$form->input(array('type'=>'color', 'id'=>'secondcolor', 'name'=>'secondcolor', 'class'=>'form-control', 'value'=>$data['secondcolor']));
			echo '</div><div class="form-group">';
			echo '</div>';
			$form->input(array('type'=>'submit', 'class'=>'btn btn-primary'));
			$form->endform();
			if(isset($_POST['bgcolor'])) {
				sql('UPDATE users SET bgcolor=?, maincolor=?, secondcolor=? WHERE id=?', array($_POST['bgcolor'], $_POST['maincolor'], $_POST['secondcolor'], $_SESSION['user_id']));
				redirect('~edit');
			}
			
		echo '</div></div>';

		if(isset($_GET['deleteaccount'])) {
			sql('DELETE FROM links WHERE id_user=?', array($_SESSION['user_id']));
			sql('DELETE FROM users WHERE id=?', array($_SESSION['user_id']));
			redirect('~logout');
		}
		break;
	}
}

elseif(isset($_GET['user'])) {
	$query = sql('SELECT count(slug) as nb FROM users WHERE slug=:slug', array('slug'=>$_GET['user']));
	$nb = $query->fetch();
	if($nb['nb'] == 1) {
		$query = sql('SELECT id, slug, email, avatar, bgcolor, maincolor, secondcolor FROM users WHERE slug=:slug', array('slug'=>$_GET['user']));
		$data = $query->fetch();
		echo '<style>
			:root {
			  --bgcolor: '.$data['bgcolor'].';
			  --maincolor: '.$data['maincolor'].';
			  --secondcolor: '.$data['secondcolor'].';
			}</style>';
		echo '<header class="col text-center">
				<img src="'.$data['avatar'].'" alt="" class="avatar"/>
				<h1>@'.$data['slug'].'</h1>
			</header>';
		echo '<section>';
		$query = sql('SELECT * FROM links WHERE id_user=?', array($data['id']));
		while($links = $query->fetch()) {
			echo '<a href="&'.$links['id'].'" class="link">'.$links['label'].'</a>';
		}
		echo '</section>';
	}
	else {
		redirect('index.php');
	}
}
elseif(isset($_GET['clic'])) {
	$query = sql('SELECT id, url, clic FROM links WHERE id=?', array($_GET['clic']));
	$data = $query->fetch();
	$clic = $data['clic']+1;
	sql('UPDATE links SET clic=? WHERE id=?', array($clic, $_GET['clic']));
	redirect($data['url']);
}
else {
	echo '<div class="alert alert-dark"><p>Cet outil permet d\'afficher une page avec une compilation de liens. Pratique pour partager plusieurs liens sur les réseaux sociaux, notamment Instagram.</p></div>';
	echo '<section>';
		$query = sql('SELECT slug FROM users ORDER BY slug ASC');
		while($user = $query->fetch()) {
			echo '<a href="@'.$user['slug'].'" class="link">@'.$user['slug'].'</a>';
		}
		echo '</section>';
}


echo '<footer>';
if(!isset($_GET['user'])) {
	echo '<p>';
	if(!isset($_SESSION['login'])) {echo '<a href="~subscribe">Pas encore de compte ?<a> <a href="~login">Se connecter</a>'; } else {echo '<a href="~edit">Administration</a>';}
	echo '</p>';
}
echo '<p><a href="index.php">Accueil</a> <a href="https://framagit.org/qwertygc/bricolages/-/tree/master/Bildumak">Bildumak</a></p>';
echo '</footer>';
?>
<body>
</html>
