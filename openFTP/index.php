<?php
session_start();
@include 'config.php';
$bdd = new PDO('sqlite:data.sqlite');
function sql($query, $array=array()) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
}
function stripAccents($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
        $clean;
}
function bytesToSize1024($bytes) {
	$unit = array('B','KB','MB');
	return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), 1).' '.$unit[$i];
}
function create_hash($pwd) {
	$salt = base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(), mt_rand()));
	$salt = str_replace('+', '.', $salt);
	$salt = substr($salt, 0, 22);
	$hash = crypt($pwd, '$2x$11$'.$salt);
	return $hash;
}
function feed() {
	global $bdd;
	$atom = '<?xml version="1.0" encoding="utf-8"?><feed xmlns="http://www.w3.org/2005/Atom"><title>Les fichiers</title><link href="http://example.org/"/><updated>'.date('c', time()).'</updated><id>urn:uuid:60a76c80-d399-11d9-b91C-0003939e0af6</id>';
		$something = $bdd->query('SELECT * FROM files WHERE validate=1 ORDER BY time DESC LIMIT 10');
		while($data = $something->fetch()) {
			$atom .= '<entry><title>'.$data['title'].'</title><link href="'.ROOT.'/public/'.$data['namefile'].'"/><id></id><updated>'.date('c', $data['time']).'</updated><summary>Un nouveau fichier a été ajouté</summary><author><name>'.$data['author'].'</name></author></entry>';
		}
	$atom .='</feed>';
	file_put_contents('feed.xml',$atom);
}
function afflicence($int) {
	switch($int) {
		case 0:
			$array['text'] = 'Domaine public';
			$array['code'] = 'CC-Zero';
			$array['url'] = 'http://creativecommons.org/publicdomain/zero';
		break;
		case 1:
			$array['text'] = 'Paternité';
			$array['code'] = 'CC-BY';
			$array['url'] = 'https://creativecommons.org/licenses/by/4.0/';
		break;
		case 2:
			$array['text'] = 'Paternité et partage des conditions initiales à l’identique';
			$array['code'] = 'CC-BY-SA';
			$array['url'] = 'https://creativecommons.org/licenses/by-sa/4.0/';
		break;
		case 3:
			$array['text'] = 'Paternité et pas de modification';
			$array['code'] = 'CC-BY-ND';
			$array['url'] = 'https://creativecommons.org/licenses/by-nd/4.0/';
		break;
		case 4:
			$array['text'] = 'Paternité et pas d’utilisation commerciale';
			$array['code'] = 'CC-BY-NC';
			$array['url'] = 'https://creativecommons.org/licenses/by-nc/4.0/';
		break;
		case 5:
			$array['text'] = 'Paternité, pas d’utilisation commerciale, partage des conditions initiales à l’identique';
			$array['code'] = 'CC-BY-NC-SA';
			$array['url'] = 'https://creativecommons.org/licenses/by-nc-sa/4.0/';
		break;
		case 6:
			$array['text'] = 'Paternité, pas d’utilisation commerciale, pas de modification';
			$array['code'] = 'CC-BY-NC-ND';
			$array['url'] = 'https://creativecommons.org/licenses/by-nc-nd/4.0/';
		break;
		case 7:
			$array['text'] = 'tous droits réservés';
			$array['code'] = 'copyright';
			$array['url'] = 'https://fr.wikipedia.org/wiki/Copyright';
		break;
	}
	return '<a rel="licence" href="'.$array['url'].'" title="'.$array['text'].' ('.$array['code'].')">'.$array['code'].'</a>';
}
###### INSTALL ##########
if(!is_dir('public')) {
	$bdd->query('CREATE TABLE files (id INTEGER PRIMARY KEY,title TEXT, mimetype TEXT, author TEXT, ip TEXT, time int(20), validate INTEGER, namefile TEXT, licence INTEGER);');
	mkdir('public', 0755);
	file_put_contents('config.php', "<?php".PHP_EOL."define('PWD', ''); //echo create_hash('your password');".PHP_EOL."define('ROOT', 'http://localhost/');");
}
######## CODE ##########
if(isset($_POST['password'])) {
	if(crypt($_POST['password'], PWD) === PWD) {
		$_SESSION['login'] = true;
		$_SESSION['token'] = md5(time());
		header('Location: index.php');
	}
	else {
		$_SESSION['login'] = false;
		$_SESSION['token'] = null;
		header('Location: index.php');
	}
}
if(isset($_GET['logout'])) {
	$_SESSION = array();
	session_destroy();
	header('Location: index.php');
}
if(isset($_POST['submit'])) {
	$files = array();
	$fdata = $_FILES['file'];
	$count = count($fdata['name']);
	if(is_array($fdata['name'])){
		for($i=0;$i<$count;++$i){
			$files[]=array(
				'name'     => $fdata['name'][$i],
				'tmp_name' => $fdata['tmp_name'][$i],
				'type' => $fdata['type'][$i],
			);
		}
	}
	else {$files[]=$fdata;}
	foreach ($files as $file) {
		if(!is_uploaded_file($file['tmp_name'])) {exit('upload');}
		$file['name']=md5_file($file['tmp_name']).'.'.pathinfo($file['name'])['extension'];
		$file['mimetype'] = mime_content_type($file['tmp_name']);
		if(!move_uploaded_file($file['tmp_name'], 'public/'. $file['name']) ) {exit('move');}
		$title = ($_POST['title'] != '') ? $_POST['title'] : $file['name'];
		$author = ($_POST['author'] !='') ? $_POST['author'] : 'Anonyme';
		sql('INSERT INTO files(title, author, mimetype, time, validate, namefile, licence, ip) VALUES(?,?, ?,?,?,?,?,?);',array($title, $author, $file['mimetype'], time(), 0, $file['name'], $_POST['licence'],$_SERVER['REMOTE_ADDR']));
		echo '<script>alert("Le fichier a été envoyé et est maintenant en validation !");</script>';
	}
}
if(isset($_SESSION['login'])) {
	if($_SESSION['login'] == true) {
		if(isset($_GET['valid']) AND $_GET['t'] == $_SESSION['token']) {
			sql('UPDATE files SET validate=1 WHERE id=?', array($_GET['valid']));
			feed();
			header('Location: index.php');
		}
		if(isset($_GET['unvalid']) AND $_GET['t'] == $_SESSION['token']) {
			sql('UPDATE files SET validate=0 WHERE id=?', array($_GET['unvalid']));
			feed();
			header('Location: index.php');
		}
		if(isset($_GET['delete']) AND $_GET['t'] == $_SESSION['token']) {
			sql('DELETE FROM files WHERE id=?', array($_GET['delete']));
			unlink('public/'.$_GET['f']);
			feed();
			header('Location: index.php');
		}
	}
}

?>
<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<link href="feed.xml" type="application/atom+xml" rel="alternate"/>
<?php if(!file_exists('sorttable.js')) {
	file_put_contents('sorttable.js', file_get_contents('http://www.kryogenix.org/code/browser/sorttable/sorttable.js'));
	echo '<script src="http://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>';
}
else {
	echo '<script src="sorttable.js"></script>';
}
?>
<title>Répertoire public</title>
<?php if(!file_exists('style.css')): ?>
<style>
* {
	margin:0;
	padding:0;
	box-sizing: border-box; 
}

html {
	min-height:100%;
	color:#61666c;
	font-weight:400;
	font-size:1em;
	font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
	width:90%;
	margin:auto;
}
header {
	text-align:center;
}
label {display:block;}
a {
	color:#61666c;
	text-decoration:none;
}
a:hover {
	color:#2a2a2a;
}
table {
	width:100%;
	border-collapse:collapse;
	font-size:.875em;
	margin: 2em 0 2em;
}
tr {
	outline:0;
	border:0;
}
tr:hover td {
	background:#f6f6f6;
}
th {
	text-align:left;
	font-size:.75em;
	padding-right:20px;
}
/* 2nd Column: Filename */

td {
	padding:5px 0;
	outline:0;
	border:0;
	border-bottom:1px solid #edf1f5;
	vertical-align:middle;
	text-align:left;
	transition:background 300ms ease;
}
td:first-of-type  {
	text-align:left;
}
td a{
	display: block;
}
p {	line-height:1.2em;}
</style>
<?php else: ?>
 <link rel="stylesheet" media="all" type="text/css" href="style.css"/>
 <?php endif; ?>
</head>
<body>
<header>
<h1>Bibliothèque libre</h1>
</header>
<!-- BLOC POUR ADMINISTRER LES FICHIERS -->
<?php
if(isset($_SESSION['login']) && $_SESSION['login'] == true) {
		$something = $bdd->query('SELECT * FROM files ORDER BY time DESC');
}
else {
	$something = $bdd->query('SELECT * FROM files WHERE validate=1 ORDER BY time DESC');
}
	?>
	<table class="sortable">
	<tr>
		<?php if(isset($_SESSION['login']) AND $_SESSION['login'] == true) { ?><th>Supprimer</th><?php } ?>
		<?php if(isset($_SESSION['login']) AND $_SESSION['login'] == true) { ?><th>Statut</th><?php } ?>
		<th>Fichier</th>
		<th>Extension</th>
		<th>Taille</th>
		<th>Auteur</th>
		<?php if(isset($_SESSION['login']) AND $_SESSION['login'] == true) { ?><th>IP</th><?php } ?>
		<th>Date</th>
		<th>Licence</th>
	</tr>
	<?php
		$total_files_admin = 0;
		$total_files = 0;
		$total_size = 0;
		$total_size_admin = 0;
		while($data = $something->fetch()) {
			$total_files = ($data['validate'] == 1) ? $total_files+1 : $total_files;
			$total_size = ($data['validate'] == 1) ? $total_size+filesize('public/'.$data['namefile']) : $total_size;
			$total_files_admin++;
			$total_size_admin +=filesize('public/'.$data['namefile']);
			$urlvalidate = ($data['validate'] == 0) ? '?valid='.$data['id'].'&t='.$_SESSION['token'] : '?unvalid='.$data['id'].'&t='.$_SESSION['token'];
			$urldelete = '?delete='.$data['id'].'&t='.$_SESSION['token'].'&f='.$data['namefile'];
			$icon = ($data['validate'] == 0) ? '<span style="color:green;">&#9745;</span>' : '&#9746;';
			echo '<tr>';
			if(isset($_SESSION['login']) AND $_SESSION['login'] == true) {echo '<td><a href="'.$urldelete.'">&#10060;</a></td>';}
			if(isset($_SESSION['login']) AND $_SESSION['login'] == true) {echo '<td><a href="'.$urlvalidate.'">'.$icon.'</a></td>';}
			echo '<td><a href="public/'.$data['namefile'].'" download="'.stripAccents($data['title']).'_'.stripAccents($data['author']).'.'.pathinfo('public/'.$data['namefile'], PATHINFO_EXTENSION).'">'.$data['title'].'</a></td>';
			echo '<td>'.$data['mimetype'].'</td>';
			echo '<td sorttable_customkey='.filesize('public/'.$data['namefile']).'>'.bytesToSize1024(filesize('public/'.$data['namefile'])).'</td>';
			echo '<td>'.ucwords(strtolower($data['author'])).'</td>';
			if(isset($_SESSION['login']) AND $_SESSION['login'] == true) {echo '<td><a href="http://www.localiser-ip.com/?ip='.$data['ip'].'">'.$data['ip'].'</a></td>';}
			echo '<td sorttable_customkey='.$total_files.'>'.date('c',$data['time']).'</td>';
			echo '<td>'.afflicence($data['licence']).'</td>';
			echo '</tr>';
		}
	?>
	</table>
	<hr>
<?php
	echo '<p>'.$total_files.' fichier(s) disponible(s) pour un poids de '.bytesToSize1024($total_size).'</p>';
	if(isset($_SESSION['login']) AND $_SESSION['login'] == true) {echo '<p>'.$total_files_admin.' fichier(s) total pour un poids de '.bytesToSize1024($total_size_admin).'</p>';}
?>
<a onclick="javascript:toggle('formupload')" style="cursor:pointer;">Envoyer un document</a>
<!-- BLOC POUR ENVOYER DES FICHIERS -->
<form action="index.php" enctype="multipart/form-data" method="post" id="formupload" style="display:none;overflow:hidden;">
<h2>Envoyer un document</h2>
<?php echo '<p>taille maximum des fichiers : '.min((int)(ini_get('upload_max_filesize')), (int)(ini_get('post_max_size')), (int)(ini_get('memory_limit'))).'MB</p>'; ?>
<label for="title">Titre du document<input type="text" name="title" id="title"/></label>
<label for="upload">Fichier : <input type="file" name="file[]" multiple required id="file"/></label>
<label for="author">Auteur : <input type="text" name="author" id="author"/></label>
<label for="licence">
	<select name="licence" id="licence">
		<option value="0">Domaine public</option>
		<option value="1">Paternité</option>
		<option value="2">Paternité et partage des conditions initiales à l’identique</option>
		<option value="3">Paternité et pas de modification</option>
		<option value="4">Paternité et pas d’utilisation commerciale</option>
		<option value="5">Paternité, pas d’utilisation commerciale, partage des conditions initiales à l’identique</option>
		<option value="6">Paternité, pas d’utilisation commerciale, pas de modification</option>
		<option value="7" selected="selected">tous droits réservés</option>
	</select>
</label>
<label for="cgu"><input type="checkbox" name="cgu" id="cgu" required /> <b>En cliquant sur envoyer, je prends conscience des conditions suivantes : </b><br>
<b>Version courte</b> : Les fichiers seront publiés sur Internet. Ne mettez rien d’illégal, dangereux, sous copyright… Bref, qui peut nous faire avoir des ennuis. Essayez de mettre des trucs en accord avec notre philosophie et notre ethique.</p>
<p><b>Version longue</b> : En cliquant sur envoyer, je prends conscience que le fichier sera rendu disponible sur Internet, toute personne utilisant ce site web pouvant le consulter sans restrictions, et que je suis responsable de sa diffusion. De ce fait, l’usager s’engage à ne pas publier des données illégales, c’est à dire de nature injurieux, diffamatoire, xénophobe, raciste, antisémite, pornographique, révisionniste, homophobe, sexiste, d’incitation à la haine, à la violence ou tout simplement contraire aux valeurs humanistes, et tout contenu susceptible de revêtir les caractères des infractions visées aux cinquième et huitième alinéas de l’article 24 de la loi du 29 juillet 1881 sur la liberté de presse et à l’article 227-23 du Code pénal et en incohérence avec notre éthique. De plus, il est strictement interdit de publier du contenu dont l’utilisateur n’a pas les droits d’auteurs ou sous licence non libre. De plus, tout fichier ayant pour vocation d’altérer le matériel informatique (vers, virus, chevaux de troie…) est strictement interdit. Tout manquement à cette règle entraînera irrémédiablement la suppression du ou des fichiers en question.</p></label>
<input type="submit" name="submit" value="Envoyer"/>
</form>
<!-- BLOC DE CONNEXION -->
<?php
	if(!isset($_SESSION['login']) OR $_SESSION['login'] == false) { echo '<a onclick="javascript:toggle(\'formadmin\')" style="cursor:pointer;">Administration</a>
	<form action="index.php" method="post" id="formadmin" style="display:none;overflow:hidden;"><h2>Administration</h2><label for="password">Mot de passe : <input type="password" name="password"/></label><input type="submit"/></form>'; }
	if(isset($_SESSION['login']) AND $_SESSION['login'] == true) { echo '<a href="?logout=true">Déconnexion</a>'; }
?>
<script>
function toggle(object_id){
 var obj=document.getElementById(object_id)	
 if(obj.style.display == 'block')
  obj.style.display='none'
 else
  obj.style.display='block'
 }
 </script>
 <p>Crédits : Propulsé par <a href="http://champlywood.free.fr/bidouilles/dl/openftp.phps">OpenFTP</a>, utilise <a href="http://www.kryogenix.org/code/browser/sorttable/">sorttable.js</a></p>
</body>
</html>