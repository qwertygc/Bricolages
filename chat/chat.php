<meta http-equiv="refresh" content="5">
<?php
include 'inc/app.php';
include 'inc/header.php';
$md = new ParsedownExtra();
$fetch = $bdd->query('SELECT pseudo, content, timestamp FROM chat WHERE id_chat = "'.$_GET['id'].'" ORDER BY id ASC');
while ($data = $fetch->fetch()) {
	echo '<div><time style="float:right; font-size:0.7em">'.timeAgo($data['timestamp']).'</time><b>'.$data['pseudo'].'</b> : '.$md->text(strip_tags($data['content'])).'</div>';
}
$fetch->closeCursor();
?>
<div id="bottom"></div>
</body>
</html>
