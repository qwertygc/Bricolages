<?php
	include 'inc/app.php';
	include 'inc/header.php';
?>
<form method="post" action="upload.php" enctype="multipart/form-data">        
    <input type="file" name="fichier[]" size="60" multiple="multiple"><br>
	<input type="submit" value="Envoyer" name="upload">   
</form>
<?php
if(isset($_POST['upload'])) {
	$files = array();
	$fdata = $_FILES['fichier'];
	$count = count($fdata['name']);
	if(is_array($fdata['name'])){
		for($i=0;$i<$count;++$i){
			$files[]=array(
				'name'     => $fdata['name'][$i],
				'tmp_name' => $fdata['tmp_name'][$i],
				'type' => $fdata['type'][$i],
			);
		}
	}
	else {$files[]=$fdata;}
	foreach ($files as $file) {
		if(!is_uploaded_file($file['tmp_name'])) {exit('error');}
		$file['name']=date('YmdHis', time()).'_'.$file['name'];
		if(!move_uploaded_file($file['tmp_name'], 'data/upload/'. $file['name']) ) {exit('error');}
		echo ROOT.'data/upload/'.$file['name'].'<br>';
	}
	
}
