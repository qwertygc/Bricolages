<?php
$cookie = (isset($_COOKIE['pseudo'])) ? $_COOKIE['pseudo'] : 'Anonymous';
if(isset($_POST['pseudo'])) {setcookie('pseudo', $_POST['pseudo'], time() + 365*24*3600, null, null, false, true);}
include 'inc/app.php';
include 'inc/header.php';
?>
<script>
      function insertText(elemID, text)
      {
        var elem = document.getElementById(elemID);
        elem.innerHTML += text;
      }
</script>
<?php
if(isset($_GET['id'])) {
	if(isset($_POST['content']) and $_POST['content'] !='') {
		$req = $bdd->prepare('INSERT INTO chat (pseudo, content, timestamp, id_chat) VALUES(?, ?, ?, ?)');
		$req->execute(array(strip_tags($_POST['pseudo']), strip_tags($_POST['content']), time(), strip_tags($_POST['id'])));
		header('Location: index.php?id='.$_POST['id']);
	}
	?>
	<div class="conversation">
		<iframe src="chat.php?id=<?php echo $_GET['id']; ?>#bottom" style="border:none;width: 100%; height: 400px;" id="chat" class="conversation"></iframe>
	</div>
	<div class="write">
		<form action="index.php?id=<?php echo $_GET['id']; ?>" method="post">
			<label for="pseudo">Pseudo<input type="text" name="pseudo" value="<?php echo $cookie; ?>"></label>
            <a href="upload.php" target="_blank">Téléverser un fichier</a>
			<textarea name="content" id="content" onkeydown="if (event.keyCode == 13) { this.form.submit(); return false; }"></textarea>
			<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"/>
			<input type="submit">
		</form>
	</div>
<?php
}
else {
	echo '<nav><a href="?id='. md5(time()) .'"><span class="icon">&#127752;</span>Créer une conversation</a></nav>';
}
?>
</body>
</html>
