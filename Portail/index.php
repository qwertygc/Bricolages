<?php
if(!file_exists('lib_opml.php')) {file_put_contents('lib_opml.php', file_get_contents('https://raw.githubusercontent.com/marienfressinaud/lib_opml/master/src/marienfressinaud/lib_opml.php'));} else { include 'lib_opml.php';}
$allfeed = array();
function feed_reader($url) {
	global $allfeed;	
	if(@preg_match('~<rss(.*)</rss>~si', file_get_contents($url))){$type='rss';}
	elseif(@preg_match('~<feed(.*)</feed>~si', file_get_contents($url))){$type='atom';}
	else return false;
	$feed = simplexml_load_file($url);
	if($type == 'rss') {
		$hostname = $feed->channel->title;
		$root = $feed->channel->link;
		foreach ($feed->channel->item as $item) {
			$allfeed[strtotime($item->pubDate)] = [
				"title" => $item->title,
				"hostname" => $hostname,
				"root" => $root,
				"link" => $item->link,
				"date" =>strtotime($item->pubDate),
				"desc" => substr(strip_tags($item->description),0,100)
			];
		}
	}
	elseif($type == 'atom') {
		$hostname = $feed->title;
		$root = $feed->link;
		foreach ($feed->entry as $item) {
			$allfeed[strtotime($item->updated)] = [
				"title" => $item->title,
				"hostname" => $hostname,
				"root" => $root,
				"link" => $item->link['href'],
				"date" =>strtotime($item->updated),
				"desc" => substr(strip_tags($item->content),0,100)
			];
		}

	}
	else {}
	return $allfeed;
}
function french($time) {
	return utf8_encode(strftime("%A %d %B %Y", $time));
}
#############################################
define('PROD', false);
define('OPML', false);
define('OPML_FILE', 'feeds.xml');
setlocale(LC_TIME, "fr_FR");
if(OPML == true) {
	$opml = libopml_parse_file(OPML_FILE)['body'];
	foreach($opml as $cat) {
		foreach(@$cat['@outlines'] as $feed) {
			$list_feed[] = array (
				'name' => $feed['text'],
				'homepage' => $feed['htmlUrl'],
				'feed' => $feed['xmlUrl'],
			);
		}
	}
}
else {
	$list_feed = array (
		array ('name'=>'Phigger', 'homepage' => 'https://www.lepouete.fr/','feed' =>'https://www.lepouete.fr/blog/feed/'),
	);
}

#############################################
if((file_exists('cache.html') && filemtime('cache.html') > time() -3600*6) AND PROD == true) {
        readfile('cache.html');
}
else {
	$friends = '';
	foreach ($list_feed as $f) {
		feed_reader($f['feed']);
		$friends .='<a href="'.$f['homepage'].'">'.$f['name'].'</a>';
	}
    $maxfeed = 50;
	krsort($allfeed);
	$i = 0;
	ob_start();
	echo '<!doctype html>
	<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width"/>
		<title>Portail</title>
		<link rel="stylesheet" href="material-style.css"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
	</head>
	<body>
	<nav><a href="index.php">Le portail des copains</a>'.$friends.'</nav>
		<main id="main">';
	foreach ($allfeed as $key => $entry) {
		echo '<div class="articlecard">
		<a href="'.$entry["link"].'">'.$entry["title"].'</a>
		<span class="metadata"><a href="'.$entry['root'].'">'.$entry['hostname'].'</a> - <time datetime="'.date('c', $entry['date']).'" class="dt-published entry-date" pubdate="pubdate">'.french($entry['date']).'</time></span>
		</div>';
		if (++$i == $maxfeed) break;
	}
	echo '</main>
	</body>
	</html>';
	$buffer = ob_get_contents();
	file_put_contents('cache.html', $buffer);
}
