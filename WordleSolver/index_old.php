<?php
error_reporting(-1);
function toslug($string) {
    $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '', ' ' => ''
    );
    $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);
    return strtolower(strtr($string, $table));
}
if (! function_exists('str_starts_with')) {
	function str_starts_with( $haystack, $needle ) {
	  return strpos( $haystack , $needle ) === 0;
	} 
}
if (! function_exists('str_ends_with')) {
    function str_ends_with(string $haystack, string $needle): bool
    {
        $needle_len = strlen($needle);
        return ($needle_len === 0 || 0 === substr_compare($haystack, $needle, - $needle_len));
    }
}

function check_start($string, $pattern) {
	if($pattern != '') {
		if(str_starts_with($string, $pattern)) {
			return $string;
		}
		else {
			return '';
		}
	}
	else {
		return $string;
	}
}
function check_end($string, $pattern) {
	if($pattern != '') {
		if(str_ends_with($string, $pattern)) {
			return $string;
		}
		else {
			return '';
		}
	}
	else {
		return $string;
	}
}
function check_letter_ok($string, $pattern) {
	if($pattern != '') {
		preg_match('/['.$pattern.']/', $string, $matches);
		if(!empty($matches)) {
			return $string;
		}
		else {
			return '';
		}
	}
	else {
		return $string;
	}
}
function check_letter_nok($string, $pattern) {
	if($pattern != '') {
		preg_match('/['.$pattern.']/', $string, $matches);
		if(!empty($matches)) {
			return '';
		}
		else {
			return $string;
		}
	}
	else {
		return $string;
	}
}
function check_format_word($string, $pattern) {
	$pattern = str_replace('?', '.', $pattern);
	if($pattern != '') {
		preg_match('/'.$pattern.'/', $string, $matches);
		if(!empty($matches)) {
			return $string;
		}
		else {
			return '';
		}
	}
	else {
		return $string;
	}
}
#####################
if(!file_exists('dico-fr.json')) { file_put_contents('dico-fr.json', file_get_contents('https://raw.githubusercontent.com/words/an-array-of-french-words/master/index.json'));}
if(!file_exists('dico-en.json')) { file_put_contents('dico-en.json', file_get_contents('https://raw.githubusercontent.com/words/an-array-of-english-words/master/index.json'));}

?>
<!doctype html>
<html>
	<head>
		<title>Liste de mots</title>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
	</head>
	<body>
		<header>
			<h1>🟩🟨🟦 Liste de mots</h1>
			<p><a href="index.php">Réinitialiser</a></p>
		</header>
		<main>
			<?php
				$_GET['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'fr';
				$_GET['number_letter'] = isset($_GET['number_letter']) ? $_GET['number_letter'] : 5;
				$_GET['ok_letter'] = isset($_GET['ok_letter']) ? $_GET['ok_letter'] : '';
				$_GET['nok_letter'] = isset($_GET['nok_letter']) ? $_GET['nok_letter'] : '';
				$_GET['start_letter'] = isset($_GET['start_letter']) ? $_GET['start_letter'] : '';
				$_GET['end_letter'] = isset($_GET['end_letter']) ? $_GET['end_letter'] : '';
				$_GET['format_word'] = isset($_GET['format_word']) ? $_GET['format_word'] : '';
			?>
			<p>
				<ul>
					<li>En anglais : <a href="https://www.powerlanguage.co.uk/wordle/">Wordle</a></li>
					<li>En français : <a href="https://www.solitaire-play.com/lemot/">Le mot</a> <a href="https://sutom.nocle.fr/">SUTOM</a> <a href="https://wordle.louan.me/">Wordle-FR</a></li>
				</ul>
			</p>
			<form method="get" action="index.php">
			<label for="lang">Choix de la langue</label>
			<select name="lang">
				<option value="fr" <?=($_GET['lang']=='fr' ? 'selected' : '');?>>Français</option>
				<option value="en" <?=($_GET['lang']=='en' ? 'selected' : '');?>>Anglais</option>
			</select>

			<label for="number_letter">Nombre de lettres</label> <input type="number" name="number_letter" id="number_letter" value="<?=$_GET['number_letter'];?>"/>
			<label for="ok_letter">Contenant les lettres</label> <input type="text" name="ok_letter" id="ok_letter" value="<?=$_GET['ok_letter'];?>"/>
			<label for="nok_letter">Ne contenant pas les lettres</label> <input type="text" name="nok_letter" id="nok_letter" value="<?=$_GET['nok_letter'];?>"/>
			<label for="start_letter">Commence par</label> <input type="text" name="start_letter" id="start_letter" value="<?=$_GET['start_letter'];?>"/>
			<label for="end_letter">Finissant par</label> <input type="text" name="end_letter" id="end_letter" value="<?=$_GET['end_letter'];?>"/>
			<label for="format_word">Recherche de mots (utiliser <kbd>?</kbd> pour les lettres inconnues)</label> <input type="text" name="format_word" id="format_word" value="<?=$_GET['format_word'];?>"/>
			<input type="submit">
			</form>
			<samp>
			<?php
			$all_dico = [
				'fr' => 'dico-fr.json',
				'en' => 'dico-en.json'
			];
			$dico = json_decode(file_get_contents($all_dico[$_GET['lang']]), true);
			foreach($dico as $mot) {
				$mot = toslug($mot);
				if((isset($_GET['number_letter']) AND strlen($mot) == (int)$_GET['number_letter'])) {
					echo check_format_word(check_letter_nok(check_letter_ok(check_end(check_start($mot, toslug($_GET['start_letter'])), toslug($_GET['end_letter'])), toslug($_GET['ok_letter'])), toslug($_GET['nok_letter'])), toslug($_GET['format_word'])).PHP_EOL;
					//echo $mot.PHP_EOL;
				}
			 }
			
			?>
			</samp>
		</main>
		<footer>
			<p><a href="https://github.com/words/">dictionnaires</a></p>
			</footer>
	</body>
</html>

