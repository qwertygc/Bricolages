<?php
function random_gpg($nbr) {
   $str = "";
   $chaine = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPSRSTUVWXYZ=+/";
   srand((double)microtime()*1000);
	$linechar = null;
   for($i=0; $i<$nbr; ++$i) {
	   if($linechar == 64) { $str .= PHP_EOL; $linechar = 0; } else {$linechar = $linechar;}
       $str .= $chaine[rand()%strlen($chaine)];
	   $linechar++;
   }

   return  '-----BEGIN PGP MESSAGE-----'.PHP_EOL.'Charset: utf-8'.PHP_EOL.'Version: GnuPG v1'.PHP_EOL.$str.PHP_EOL.'-----END PGP MESSAGE-----';
}

echo '<pre>';
echo random_gpg(rand(800, 8000));
echo '</pre>';
?>
