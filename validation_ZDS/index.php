<!doctype html>
<html>
<head>
<style>
html {
	background: #F7F7F7;
}
body {
	margin:auto;
	width:800px;
	background:#fff;
	font-family:"Source Sans Pro", "Segoe UI", "Trebuchet MS", Helvetica, "Helvetica Neue", Arial, sans-serif;
}
label, textarea {
display:block;
}
textarea {
	min-width:800px;
	min-height:300px;
	font-family:"Merriweather", "Liberation Serif", "Times New Roman", Times, Georgia, FreeSerif, serif;
}
h1 {
	background:#084561;
	color:#F8AD32;
}
</style>
<title>Asistant du validateur</title>
</head>
<body>
<h1>L’assistant du validateur :-)</h1>
<?php
$anwser = "Bonjour,

Je suis en charge de la validation de ton contenu. 

Je souhaite te remercier pour le travail que tu as apporté, et ton intérêt à vouloir enrichir la bibliothèque des contenus du site.".PHP_EOL.PHP_EOL;

if(!isset($_POST['plagiat'])) {
	$anwser .='Malheureusement ton contenu semble plagié, et tu peux comprendre que n’ont puisse pas accepter ce tutoriel.'.PHP_EOL.PHP_EOL;
}
elseif(!isset($_POST['CGU'])) {
	$anwser .='Malheureusement ton contenu semble contraire aux CGU, et tu peux comprendre que n’ont puisse pas accepter ce tutoriel.'.PHP_EOL.PHP_EOL;
}
else {
	if(!isset($_POST['TEA'])) {
		$anwser .= 'Ton contenu semble être un « truc et astuce ». Ce format est relativement court, et ne semble pas correspondre à la ligne éditoriale du site. Je te conseille de rédiger un [billet](https://zestedesavoir.com/contenus/nouveau-billet/) pour en faire profiter à la communauté !'.PHP_EOL.PHP_EOL;
	}
	else {
		if(!isset($_POST['intro_conclu'])
		OR !isset($_POST['contenu_juste_pertinent'])
		OR !isset($_POST['biblio'])
		OR !isset($_POST['orthotypo'])
		OR !isset($_POST['texte_alternatif'])
		OR !isset($_POST['images_zds'])
		OR !isset($_POST['code_indente'])
		OR !isset($_POST['miniature'])
		OR !isset($_POST['tags_section'])
		OR !isset($_POST['tags_section'])) {
			$anwser .= 'Je vais être honnête avec toi, et ne vais pas faire planer le suspens : le contenu n’est encore pas publiable en l’état. Il reste du travail à faire, retoucher quelques retouches… pour qu’il soit aux petits oignons !'.PHP_EOL.PHP_EOL;
			$anwser .='# Remarques sur le fond'.PHP_EOL.PHP_EOL;
			if(!isset($_POST['intro_conclu'])) {
				$anwser .='Je trouve ton introduction et ta conclusion un peu trop léger. Voici quelques conseils pour les étoffer. Concernant l’introduction, commence par une entrée en matière, une phrase d’accroche qui nous donne envie de lire ton article. Puis, présente ton sujet et ton déroulé de ton contenu. Pour la conclusion, récapitulent les idées phares de ton contenu ! Inspire toi d’autres contenus du site au besoin.'.PHP_EOL.PHP_EOL;
			}
			if(!isset($_POST['contenu_juste_pertinent'])) {
				$anwser .='Concernant ton contenu en tant que tel, j’ai quelques remarques :'.PHP_EOL.PHP_EOL;
				$anwser .=$_POST['contenu_juste_pertinent'].PHP_EOL.PHP_EOL;
			}
			if(!isset($_POST['biblio'])) {
				$anwser .='Est-il possible de mettre une petite bibliographie/liste de références pour permettre d’aller plus loin ?'.PHP_EOL.PHP_EOL;
			}
			$anwser .='# Remarques sur la forme'.PHP_EOL.PHP_EOL;
			if(!isset($_POST['orthotypo'])) {
				$anwser .='J’ai repéré quelques coquilles. Pense à vérifier avec Grammalecte ;-)'.PHP_EOL.PHP_EOL;
			}
			if(!isset($_POST['texte_alternatif'])) {
				$anwser .='Dans un souci d’accessibilité du contenu, il me semble pertinent de mettre à tes images un texte alternatif !'.PHP_EOL.PHP_EOL;
			}
			if(!isset($_POST['images_zds'])) {
				$anwser .='Dans un souci de pérennité du contenu, il me semble pertinent de téléverser sur Zeste de Savoir tes images !'.PHP_EOL.PHP_EOL;
			}
			if(!isset($_POST['code_indente'])) {
				$anwser .='Vérifie que ton code soit bien indenté, testé et commenté ;-)'.PHP_EOL.PHP_EOL;
			}
			if(!isset($_POST['miniature'])) {
				$anwser .='Tu n’as pas mis de miniature pour ton contenu. Il me semble intéressant d’en mettre une, afin de personnaliser ton contenu !'.PHP_EOL.PHP_EOL;
			}
			if(!isset($_POST['tags_section'])) {
				$anwser .='Concernant ton choix des catégories et des tags, voici mes remarques :'.PHP_EOL.PHP_EOL;
				$anwser .=$_POST['tags_section'].PHP_EOL.PHP_EOL;
			}
			if(!isset($_POST['autres'])) {
				$anwser .='# Autres remarques'.PHP_EOL.PHP_EOL;
				$anwser .=$_POST['autres'].PHP_EOL.PHP_EOL;
			}
		}
		else {
				$anwser .= 'Félicitation, ton texte est publiable en l’état. Prends un smoothie pour fêter ça !'.PHP_EOL.PHP_EOL;
		}
	}
}
$anwser .='Qwerty';
?>
<form method="post">
<label for="plagiat"><input type="checkbox" name="plagiat" id="plagiat"> Ton contenu n’est pas plagié ;</label>
<label for="CGU"><input type="checkbox" name="CGU" id="CGU"> Ton contenu n’est pas contraire aux CGU ;</label>
<label for="TEA"><input type="checkbox" name="TEA" id="TEA">Ton contenu n’est un TEA ;</label>
<label for="intro_conclu"><input type="checkbox" name="intro_conclu" id="intro_conclu"> Tu as rédigée une introduction et une conclusion consistante ;</label>
<label for="contenu_juste_pertinent">Ton contenu est assez clair et pédagogique. Le jargon est expliqué  : <textarea name="contenu_juste_pertinent" id="contenu_juste_pertinent"></textarea></label>
<label for="biblio"><input type="checkbox" name="biblio" id="biblio"> Tu as mis une bibliographie ;</label>
<label for="orthotypo"><input type="checkbox" name="orthotypo" id="orthotypo"> Il n’y a pas de fautes orthotypographiques. Dans le doute, vérifie avec Grammalecte ;-) ;</label>
<label for="texte_alternatif"><input type="checkbox" name="texte_alternatif" id="texte_alternatif"> Il y a un texte alternatif à tes images ;
</label>
<label for="images_zds"><input type="checkbox" name="images_zds" id="images_zds"> Tes images sont hébergées sur ZDS ;</label>
<label for="code_indente"><input type="checkbox" name="code_indente" id="code_indente"> Ton code est testé, indenté et commenté ;</label>
<label for="miniature"><input type="checkbox" name="miniature" id="miniature"> Tu as mis une miniature ;</label>
<label for="tags_section">Tu as renseigné les bons tags et les bonnes sections : <textarea name="tags_section" id="tags_section"></textarea></label>
<label for="autres">Autres remarques : <textarea name="autres" id="autres"></textarea></label>
<input type="submit" name="submit" id="submit" value="Envoyer"/>
</form>
<?php if(isset($_POST['submit'])) { echo '<textarea>'.$anwser.'</textarea>'; }?>
</body>
</html>
