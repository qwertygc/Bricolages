#Code déontologique
1. L’auteur doit s’assurer à répondre à sa mission : informer et respecter le lecteur, doit servir l’intérêt public et le droit au savoir. Tous les articles publiés traitent de faits vérifiés par la rédaction sur le fond ; ils sont corrigés et enrichis, si nécessaire, sur la forme. Il en résultera un contenu de qualité, incitant au débat (constructif) et à la réflexion. L’auteur doit faire preuve d’une grande intégrité et honnêteté intellectuelle, afin de ne pas tomber dans le piège du sensationnel.
2. Les contenus doivent être compréhensible par la majorité des lecteurs. Le jargon technique et les concepts difficiles à appréhender doivent être expliqué pour être compris par le plus grand nombre.
3. L’auteur à une responsabilité vis-à-vis de la liberté d’expression. Cette liberté peut-être utilisée dans les limites de la loi. Il a la liberté de s’exprimer, mais aussi la responsabilité d’assumer les conséquences de ses propos face à la loi et les critiques qui pourraient être faites.
4. Toute information possède un contexte. Il est du devoir de l’auteur d’en prendre compte et d’en informer ses lecteurs. L’auteur, comme le lecteur, doit pouvoir exercer son droit à l’esprit critique en connaissant dans sa globalité le contexte, afin de ne pas tomber dans le piège de la désinformation.
5. L’auteur doit s’assurer à respecter les droits internationaux et nationaux en vigueurs, notamment concernant le respect de la vie privée. Est interdit, notamment, la violence ou l’incitation à la violence, raciste ou xénophobe, la pornographie, la pédophilie, le révisionnisme et le négationnisme. Est prohibé la publication de contenus contrevenant aux droits d’autrui ou à caractère diffamatoire, les propos injurieux, obscènes ou offensants. Est aussi interdit propagande (directe ou indirecte), prosélytisme ou dogmatisme, notamment à but idéologique, politique, professionnelle, commerciale religieuse ou sectaire. Est également interdit la contrefaçon de marques déposés et le plagiat.
6. L’auteur ne doit pas chercher à porter atteinte à l’image d’une personne en raillant ses caractéristiques physiques ou en lui associant toute représentation dégradante.
7. L’auteur peut émettre un jugement, même partiel et partial, en respect avec la déontologie.
8. L’auteur doit s’assurer une indépendance des pouvoirs politiques, économiques ou tout autre forme de pouvoir en contradiction avec la volonté d’accomplir sa mission.
9. L’auteur doit citer ces sources et des « matériaux » nécessaire à l’élaboration de l’article, hormis dans le cadre exceptionnel de la protection des sources d’information du journalisme. Ainsi, toutes sources d’information doit être vérifiable par le lecteur et permettre de vérifier son authenticité.

#Publication
1. La publication doit utiliser au maximum des logiciels libres dans des formats ouverts et standardisés.
2. La publication doit être, si possible, sous contenu libre, par respect de la culture libre et du logiciel libre, afin de faciliter la lecture, l’étude, la modification et la diffusion.
3. La publication doit offrir un moyen au lecteur, soit par exemple via le biais de commentaires ou de courriel, un moyen de contacter l’auteur, pour apporter des précisions, un avis…
4. La publication doit permettre notamment un espace de « droit de réponse » selon les obligations fournit par la loi.

#Qualité du contenu
1. L’auteur doit respecter les règles issues des rectifications orthographiques françaises de la réforme de 1990.
2. L’auteur doit respecter les règles typographiques en vigueur en France.
3. Il sera honni toute utilisation abusive des majuscules, du langage SMS, d’une utilisation non modérée des smileys.
4. L’auteur doit respecter, dans la limite du possible, les règles sémantiques relatif à l’utilisation du HTML5 et d’accessibilité dans le cas de publication sur le web.
