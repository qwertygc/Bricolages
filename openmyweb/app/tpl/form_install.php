<h1>Gérer vos dossiers</h1>
<form method="post" action="?action=install">
	<fieldset>
		<legend>Blog :</legend>
		<?php echo value_software('blogotext', 'Blogotext', '//lehollandaisvolant.net/blogotext'); ?>
		<?php echo value_software('pluxml', 'PluXML', '//pluxml.org'); ?>
	</fieldset>
	<fieldset>
		<legend>Galerie :</legend>
		<?php echo value_software('minigalmono', 'Minigal Mono', '//sebsauvage.net/wiki/doku.php?id=minigal_nano'); ?>
		<?php echo value_software('sykgalius', 'Sykgalius', '//sykius.fr/dev/sykgalius'); ?>
		<?php echo value_software('sharimg', 'Sharimg', '//bleiddwn.com/'); ?>
	</fieldset>
	<fieldset>
		<legend>Lecteur de flux RSS : </legend>
		<?php echo value_software('krissfeed', 'KrISS Feed', '//tontof.net/kriss/feed'); ?>
	</fieldset>
	<fieldset>
		<legend>Gestionnaire de fichiers : </legend>
		<?php echo value_software('dropcenter', 'DropCenter', '//projet.idleman.fr/dropcenter'); ?>
		<?php echo value_software('picloud', 'PiCloud', '//dmeloni.github.io/picloud'); ?>
	</fieldset>
	<fieldset>
		<legend>Wiki : </legend>
		<?php echo value_software('dokuwiki', 'DokuWiki', '//www.dokuwiki.org/'); ?>
	</fieldset>
	<fieldset>
		<legend>Agrégateur de liens : </legend>	
		<?php echo value_software('shaarli', 'Shaarli', '//sebsauvage.net/wiki/doku.php?id=php:shaarli'); ?>
	</fieldset>
	<fieldset>
		<legend>Anti-censure :</legend>
		<?php echo value_software('respawn', 'Respawn', '//github.com/broncowdd/respawn'); ?>
		<?php echo value_software('autoblog', 'Projet autoblog', '//github.com/mitsukarenai/Projet-Autoblog'); ?>
		<?php echo value_software('zerobin', 'Zerobin', '//sebsauvage.net/wiki/doku.php?id=php:zerobin'); ?>
	</fieldset>
	<input type="password" placeholder="votre mot de passe" id="password" name="password" required />
	<input type="hidden" value="<?php echo $_SESSION['name']; ?>" />
	<input type="submit" />
</form>