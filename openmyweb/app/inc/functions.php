<?php
##########Divers fonctions##########
###Manipulation JSON#####
function store($file,$datas){file_put_contents($file,gzdeflate(json_encode($datas)));}
function unstore($file){return json_decode(gzinflate(file_get_contents($file)),true);}
		 
		 
function download_soft($namefile, $namesoft, $urlfile, $dirfile, $dirname) { #download and install the software
	 if(!file_exists($namefile)) {
		// file_put_contents($namefile, get_web_page($urlfile)); 
		file_put_contents($namefile, get_web_page($urlfile)); 
	}
	$zip = new ZipArchive;
	if ($zip->open($namefile) === TRUE AND filesize($namefile) != 0) {
		$zip->extractTo('./'.$dirname.'/'.$dirfile);
		$zip->close();
		echo 'Téléchargement de '.$namesoft.' : OK <a href="'.$dirname.'/'.$dirfile.'">'.$dirname.'/'.$dirfile.'</a><br>';
		mylog('Téléchargement de '.$namesoft.' : OK', $dirname);
	}
	else {
		echo 'Téléchargement de '.$namesoft.' : ECHEC<br>';
		mylog('Téléchargement de '.$namesoft.' : ECHEC', $dirname);
		unlink($namefile);
	}
}

function value_software($id, $name, $website) { # create a input
	return '<label for="'.$id.'"><input type="checkbox" name="'.$id.'" id="'.$id.'">'.$name.'</label> '.str_replace('/index.php','','http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].'/'.$_SESSION['name'].'/').'<input type="text" placeholder="/nom du dossier" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$" value="'.$id.'" name="dir_'.$id.'" />'.' (<a href="'.$website.'">site officiel</a>)<br>';
}

function create_account($login, $password, $mail) {
	if(!file_exists($login."/.config") AND $login != "app") {
		echo "création du compte  <br>";
		mkdir($login); //création du dossier
		$config = array('password'=>sha1($password), 'mail'=>$mail);
		store($login.'/.config', $config); //création du fichier
		file_put_contents($login.'/index.php', file_get_contents('index.php.txt'));
		mylog('Création compte '.$login.' : OK', '.');
	}
	else {
		echo "compte déjà existant !<br>";
		mylog('Création compte '.$login.' : ECHEC', '.');
	}
}

function verif_account($login, $password) {
	$config = unstore($login.'/.config');
	if($config['password'] == sha1($password)) {
		return true;
		mylog('Connexion compte '.$login.' : OK', '.');
	}
	else {
		return false;
		mylog('Connexion compte '.$login.' : ECHEC', '.');
	}
}
function mylog($post, $dir) {
	error_log(date("d/m/Y - H:i:s",time())." ".$post, 3, $dir."/journal.log");
}
function cleardir($dossier) { #SRC : http://www.wikistuce.info/doku.php/php/supprimer_un_dossier_et_son_contenu
	$ouverture=@opendir($dossier);
	if (!$ouverture) return;
	while($fichier=readdir($ouverture)) {
		if ($fichier == '.' || $fichier == '..') continue;
			if (is_dir($dossier."/".$fichier)) {
				$r=clearDir($dossier."/".$fichier);
				if (!$r) return false;
			}
			else {
				$r=@unlink($dossier."/".$fichier);
				if (!$r) return false;
			}
	}
closedir($ouverture);
$r=@rmdir($dossier);
if (!$r) return false;
	return true;
}
function get_web_page($url) { #think Alex
	$options = array(
		CURLOPT_RETURNTRANSFER => true, // return web page
		CURLOPT_HEADER => false, // don't return headers
		CURLOPT_FOLLOWLOCATION => true, // follow redirects
		CURLOPT_ENCODING => "", // handle all encodings
		CURLOPT_USERAGENT => "spider", // who am i
		CURLOPT_AUTOREFERER => true, // set referer on redirect
		CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
		CURLOPT_TIMEOUT => 120, // timeout on response
		CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
		CURLOPT_SSL_VERIFYPEER => false // Disabled SSL Cert checks
);

	$ch = curl_init( $url );
	curl_setopt_array( $ch, $options );
	$content = curl_exec( $ch );
	$err = curl_errno( $ch );
	$errmsg = curl_error( $ch );
	$header = curl_getinfo( $ch );
	curl_close( $ch );

$header['errno'] = $err;
$header['errmsg'] = $errmsg;
$header['content'] = $content;
return $header; 
}
function foldersize($dir) {
	$count_size = 0;
	$count = 0;
	$dir_array = scandir($dir);
	foreach($dir_array as $key=>$filename) {
		if($filename!=".." && $filename!=".") {
			if(is_dir($dir."/".$filename)) {
				$new_foldersize = foldersize($dir."/".$filename);
				$count_size = $count_size + $new_foldersize;
			}
			else if(is_file($dir."/".$filename)) {
				$count_size = $count_size + filesize($dir."/".$filename);
				$count++;
			}
		}
	}
	return $count_size;
}
function octectsconvert($size) { 
	if($size/1024 >= 0 AND $size/1024 <= 1) {
		return ceil($size)." octets";
	}
	if($size/1024/1024 >= 0 AND $size/1024/1024 <= 1) {
		return ceil($size/1024) ." Kio";
	}
	if($size/1024/1024/1024 >= 0 AND $size/1024/1024/1024 <= 1) {
		return ceil($size/1024/1024) ." Mio";
	}
	if($size/1024/1024/1024/1024 >= 0 AND $size/1024/1024/1024/1024 <= 1) {
		return ceil($size/1024/1024/1024) ." Gio";
	}
	if($size/1024/1024/1024/1024/1024 >= 0 AND $size/1024/1024/1024/1024/1024 <= 1) {
		return ceil($size/1024/1024/1024/1024) ." Tio";
	}
	if($size/1024/1024/1024/1024/1024/1024 >= 0 AND $size/1024/1024/1024/1024/1024/1024 <= 1) {
		return ceil($size/1024/1024/1024/1024/1024) ." Pio";
	}
}
function sendmail($mail, $subject="", $body="") {
	return '<a href="mailto:'.$mail.'?subject='.$subject.'&body='.$body.'">Send a e-mail</a>';
}
?>