<?php
echo '<ul>';
$listdir = scandir($_SESSION['name']);
foreach($listdir as $dir) {
	if($dir !='.config' and $dir !='.' and $dir !='..' and $dir !='journal.log' and $dir !='index.php') {
		echo '<li><a href="'.$_SESSION['name'].'/'.$dir.'">'.$dir.'</a> <a href="?token='.$_SESSION['token'].'&dir='.$dir.'">[DELETE]</a></li>';
	}
}
echo '</ul>';
if(isset($_GET['token']) AND isset($_GET['dir']) AND $_GET['token'] == $_SESSION['token']) {
	cleardir($_SESSION['name'].'/'.$_GET['dir'].'/');
	echo "SUPRESSION DU FICHIER";
	mylog('SUPRESSION de '.$_GET['dir'].' : OK', $_SESSION['name']);
	header('Location: index.php');
}
?>