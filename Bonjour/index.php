<?php
#CONFIG TITLE #
define("TITLE", "BONJOUR TOI");
define("ROOT", "http://mybonjour/");
define("PASS", "mypassword");
#IS THE END ! DON'T TOUCH AFTER#
function stripAccents($string) {
	return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
							'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}
function affimg($data) {
	return '<img src="img/'.$data.'" alt="IMG" style="max-width:1000px;"/>';
}
function affimgurl($id, $data) {
	return '<a href="index.php?id='.$id.'"><img src="img/'.$data.'" alt="IMG" style="max-width:1000px; display:bloc;"/></a>';
}
function affdate($data) {
	return "<time>".date('d m Y', $data)."</time>";
}
function buttonbefore($data) {
	$id = $data-1;
	return '<a href="?id='.$id.'">«</a>';
}
function buttonafter($data) {
	$id = $data+1;
	return '<a href="?id='.$id.'">»</a>';
}
try {
	$bdd = new PDO('sqlite:data.db');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 
catch (PDOException $e) {
	echo "<p>Erreur : " . $e->getMessage() . "</p>";
	exit();
}
if(!file_exists('upload.txt')) {
	$bdd->query("CREATE TABLE bonjour (
		id INTEGER PRIMARY KEY,
		img text NOT NULL,
		time int(20) NOT NULL default '0');");
	$bdd->query("INSERT INTO `bonjour` (`id`, `img`, `time`) VALUES (1, 'picture.png', '".time()."');");
	file_put_contents('upload.txt', 1);
	mkdir('img');
	file_put_contents('img/picture.png', file_get_contents('https://s3.amazonaws.com/placekitten.com/homepage-samples/408/287.jpg'));
}
if(isset($_GET['rss'])) {
	header("Content-type: application/xml");
	// $url= dirname("http://". $_SERVER['HTTP_HOST']. $_SERVER["PHP_SELF"]. "/");
	$sql = $bdd->query('SELECT * FROM bonjour ORDER BY time DESC');
	$rss ='<?xml version="1.0" encoding="utf-8"?>'. "\n";
	$rss.='<rss version="2.0">'. "\n";
	$rss.='<channel>'."\n";
	$rss.='<title>'. TITLE. '</title>'. "\n";
	//$rss.='<description><![CDATA['.DESCRIPTION. ']]></description>'. "\n"; Facultatif
	$rss.='<description></description>'. "\n";
	$rss.='<link>'.ROOT.'</link>'. "\n";
	$sql->setFetchMode(PDO::FETCH_BOTH);
	while($donnees = $sql->fetch()) {
		if(time()> $donnees['time']) {
			$rss.='<item>'. "\n";
			$rss.='<title>'.affdate($donnees['time']). '</title>'. "\n"; 
			$rss.='<link>'. ROOT. '/?id='. $donnees['id']. '</link>'. "\n";
			$rss.='<description><![CDATA['.affimg($donnees['img']). ']]></description>'. "\n";
			$rss.='<pubDate>'.date('D, d M Y H:i:s O', $donnees['time']).'</pubDate>'. "\n";
			$rss.='</item>'. "\n";
		}
	}
	$rss.='</channel>'. "\n";
	$rss.='</rss>';
	echo  stripslashes($rss);
}
else {
	include("tpl/header.php");
	if(isset($_GET['action'])) {
	switch($_GET['action']) {
		case 'archive':
			$retour = $bdd->query("SELECT * FROM `bonjour` ORDER BY id DESC");
			while($donnees = $retour->fetch()) {
				if(time() >= $donnees['time']) {
					echo affimgurl($donnees['id'],$donnees['img']);
				}
			}
		break;
		case 'admin':
			if(isset($_POST['pass']) AND $_POST['pass'] == PASS) {
				$_SESSION['session'] = true;
			}
			if(isset($_SESSION['session'])) {
				if(isset($_POST['upload'])) {
					// Selection pour la date précédente
					$retourbefore = $bdd->query("SELECT * FROM bonjour ORDER BY id DESC LIMIT 1");
					$donneesbefore = $retourbefore->fetch();
					$content_dir = 'img/'; 
					$tmp_file = $_FILES['fichier']['tmp_name'];
					if( !is_uploaded_file($tmp_file) ) {
						exit("fichier non trouvé");
					}
					$type_file = $_FILES['fichier']['type'];
					$nb_img = file_get_contents('upload.txt');
					$_FILES['fichier']['name']=$nb_img."".$_FILES['fichier']['name'];
					$nb_img++;
					file_put_contents('upload.txt', $nb_img);
					$_FILES['fichier']['name'] = preg_replace('# #isU', '-', $_FILES['fichier']['name']);
					$_FILES['fichier']['name'] = preg_replace('#[!;,:]#isU', '-', $_FILES['fichier']['name']); 
					$_FILES['fichier']['name']= stripAccents($_FILES['fichier']['name']);
					$name_file = $_FILES['fichier']['name'];
					if( !move_uploaded_file($tmp_file, $content_dir . $name_file) ) {
						exit("Erreur dans l'envoi du fichier");
					}
					$time = $donneesbefore['time']+(24*60*60);
					$retour = $bdd->prepare("INSERT INTO `bonjour` (`img`, `time`) VALUES (:img, :time);");
					$retour->execute(array('img'=>$name_file, 'time'=>$time));
				}
				echo '<form method="post" enctype="multipart/form-data" action="?action=admin"><p><input type="file" name="fichier" size="30"><input type="submit" name="upload" value="upload"></p></form><a href="?verif=logout">Logout</a>';
			}
		else {
			echo '<form method="post" action="?action=admin"><p><input type="password" name="pass"><input type="submit" value="GO"></p></form>';
		}
		break;
	}
	}
	else {
		if(empty($_GET['id'])) {
			$retour = $bdd->query("SELECT * FROM bonjour WHERE  time < ".time()." ORDER BY id DESC LIMIT 1 ");
			$donnees = $retour->fetch();
				echo affdate($donnees['time']);
				echo buttonbefore($donnees['id']);
				echo affimg($donnees['img']);
		}

		else {
			$retour = $bdd->prepare("SELECT * FROM bonjour WHERE id=:id");
			$retour->execute(array('id'=>$_GET['id']));
			$donnees = $retour->fetch();
			if(time() >= $donnees['time']) {
				echo buttonbefore($donnees['id']).' '.buttonafter($donnees['id']);
				echo affimg($donnees['img']);
				echo affdate($donnees['time']);
			}
		}
	}
	include("tpl/footer.php");
}
?>