mkdir public
cp -r templates/assets public/assets
cp -r source/files public/files
cd source
for i in *.md; do
	echo $i
	output=${i%%.*}.html
	pandoc --standalone -i $i -o "../public/$output"  --template=../templates/template.html --toc

done
