<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<style>
label{display:block}
</style>
<body>
<form method="post">
<fieldset>
<legend>Calcul du temps passé sur le projet</legend>
<label>Temps (en h) de préparation (recherche d'information…) : <input type="number" name="p" id="p" value="0"/></label>
<label>Temps (en h) de conception  : <input type="number" name="c" id="c" value="0"/></label>
<label>Temps (en h) de réalisation  : <input type="number" name="r" id="r" value="0"/></label>
<label>Temps (en h) de finition (égal généralement au temps de réalisation)  : <input type="number" name="f" id="f" value="0"/></label>
<label>Temps (en h) de test  : <input type="number" name="t" id="t" value="0"/></label>
</fieldset>
<fieldset>
<legend>Calcul du coût de réalisation</legend>
<label>Salaire (minimal) horaire  : <input type="number" name="s" id="s" value="9.53"/></label>
</fieldset>
<legend>Matériel</legend>
<label>Coût des consommables (papier, essence…) : <input type="number" name="consommable" id="consommable" value="0"/></label>
</fieldset>
<input type="submit" name="submit"/>
</form>
</body>
</html>
<?php
if(isset($_POST['submit'])) {
	echo '<pre>';
	$t1 = $_POST['p']+$_POST['c']+$_POST['r']+$_POST['f']+$_POST['t'];
	$annexe = $t1 * 0.25; // Prise en compte des tâches annexes
	$t = $t1 + $annexe;
	$imprevu = $t*$_POST['s']*0.1;
	$avimpot = (($t+$imprevu)*$_POST['s'])+$_POST['consommable'];
	echo '<b>Temps passé sur le projet</b> : '.$t.'h'.PHP_EOL;
	echo 'Dont :'.PHP_EOL;
	echo "\t".'Préparation : '.$_POST['p'].PHP_EOL;
	echo "\t".'Conception : '.$_POST['c'].PHP_EOL;
	echo "\t".'Réalisation : '.$_POST['r'].PHP_EOL;
	echo "\t".'Finition : '.$_POST['f'].PHP_EOL;
	echo "\t".'Tests : '.$_POST['t'].PHP_EOL;
	echo "\t".'Communication avec le client : '.$annexe.PHP_EOL;
	echo 'Coût des consomables : '.$_POST['consommable'].PHP_EOL;
	echo 'Prise en compte des imprévus : '.$imprevu.PHP_EOL;
	echo 'Rénumération horaire : '.$_POST['s'].PHP_EOL;
	echo '<b>Coût avant impôt : </b>'.$avimpot.PHP_EOL;
	echo '</pre>';
}