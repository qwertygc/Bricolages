﻿<?php
############# CREATE database.sqlite #############
// CREATE TABLE "book" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "author" VARCHAR, "leaf" TEXT)
##################################################
################CONFIGURATION#####################
define('PASSWORD', 'demo');
##################################################
error_reporting(-1);
try{
    $db = new PDO('sqlite:database.sqlite');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
}
catch(Exception $e) {
    echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
    die();
}
session_start();
###### POST #######
if(isset($_POST['textarea'])) {
	$author = (!empty($_POST['author'])) ? $_POST['author'] : "anonymous";
	// echo $author;
	// echo $_POST['textarea'];
	$query = $db->prepare("INSERT INTO book(author, leaf) VALUES(:author, :leaf)");
	$query->execute(array('author' => $author, 'leaf'=>$_POST['textarea']));
}
###### ADMIN CONNEXION ######
if(isset($_POST['password']) AND $_POST['password'] == PASSWORD) {
	$_SESSION['login'] = true;
	$_SESSION['token'] = md5(uniqid(rand(), true));
}
if(isset($_GET['logout']) && isset($_SESSION['token']) && $_GET['logout'] == $_SESSION['token']) {
	$_SESSION = array();
	session_destroy();
}
##### ADMIN MODERATION #####
if (isset($_GET['id']) && !empty($_SESSION['token']) && $_SESSION['token']==$_GET['token'] && $_SESSION['login'] == true) {
	$query = $db->prepare("DELETE FROM book WHERE id=?");
	$query->execute(array($_GET['id']));
}
include 'inc/header.php';
##### Progress bar block #######
$query = $db->query('SELECT id, author, leaf FROM book ORDER BY id ASC');
$nb_word = 0;
while($data = $query->fetch()) {$nb_word += str_word_count($data['leaf']);}
$nb_word_percent = round($nb_word/50000*100, 2);
echo '<section id="stat">';
echo number_format($nb_word).' mots écrits sur 50,000 soit '.$nb_word_percent.' pourcent.';
echo '<progress id="avancement" value="'.$nb_word_percent.'" max="100"></progress>';
echo "</section>";

##### Think You block ######
echo "<section>";
echo '<h2 style="text-align:center;font-size:1.2em;">Contributeurs</h2>';
$query = $db->query('SELECT DISTINCT author FROM book ORDER BY id');
$list_authors = null;
while($data = $query->fetch()) {
	$list_authors .= ''.$data['author'].', ';
}
echo '<p>'.substr($list_authors, 0, -2).'</p>';
echo "</section><hr>";

##### Book block ######
echo '<div class="book" id="book">';
$query = $db->query('SELECT id, author, leaf FROM book ORDER BY id ASC');
while($data = $query->fetch()) {include 'inc/article.php';}
echo '</div>';
##### Form block ######
include 'inc/form.php';
include 'inc/footer.php';
?>
