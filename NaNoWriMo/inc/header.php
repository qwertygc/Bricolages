﻿<!DOCTYPE HTML>
<html lang="fr">
<head>
    <title>NaNoWriMo collaboratif</title>
<link href="inc/style.css" rel="stylesheet"  type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
</head>
<body>
<header class="header">
	<h1>NaNoWriMo collaboratif</h1>
</header>
<main>
<h2>Préface</h2>
<article>
<p>Que dirais-tu de rédiger un roman de 50 000 mots - en référence au concours NaNoWriMo - de manière collaborative, au fil de l’eau ? Sans aucun plan de départ ? Une écriture spontanée, libre, et avec un résultat final surprenant ?</p>
<p>Cette participation est collaborative. Chacun pourra écrire la suite de l’histoire. Ainsi, le roman ne sera pas l’œuvre d’une seule personne, mais de plusieurs, et sera donc issu de l’écriture collaborative. C’est du crowdwriting !</p>
<a class="link" href="#author"><h2>Je veux participer !</h2></a>
</article>
<hr>
