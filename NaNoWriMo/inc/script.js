﻿	function notif(color, message) {
		document.getElementById('notif').innerHTML = message;
		document.getElementById('notif').style.backgroundColor = color;
		setTimeout(function() { document.getElementById('notif').innerHTML = ''; document.getElementById('notif').style.backgroundColor = "transparent";}, 7000);
	}
   function count_word() {
       var chaine = document.getElementById('textarea').value; 
		chaine = chaine.replace(/--/, "—");
		chaine = chaine.replace(/<</, "«");
		chaine = chaine.replace(/>>/, "»");
		chaine = chaine.replace(/éé/, "É");
		chaine = chaine.replace(/èè/, "È");
		chaine = chaine.replace(/çç/, "Ç");
		chaine = chaine.replace(/àà/, "À");
		chaine = chaine.replace(/'/, "’");
		chaine = chaine.replace(/\.\.\./, "…");
		nb = chaine.split(/\b\w+\b/).length-1; 
		document.getElementById('textarea').value = chaine;
        document.getElementById("result").innerHTML = nb;
		if(nb == "10") {notif('#2ECC71','Bravo, tu a atteint les 10 mots !');}
		if(nb == "100") {notif('#2ECC71', 'Bravo, tu a atteint les 100 mots !');}
		if(nb == "250") {notif('#2ECC71', 'Bravo, tu a atteint les 250 mots !');}
		if(nb == "400") {notif('#2ECC71', 'Bravo, tu a atteint les 400 mots !');}
		if(nb == "500") {notif('#2ECC71', 'Bravo, tu a atteint les 500 mots !');}
		if(nb == "750") {notif('#9B59B6', '<b>Trophé 3Pages.fr</b> : Bravo, tu a atteint les 750 mots !');}
		if(nb == "1000") {notif('#2ECC71', 'Bravo, tu a atteint les 1000 mots !');}
		if(nb == "1667") {notif('#9B59B6', '<b>Un jour NaNoWriMo</b> : Bravo, tu a atteint les 1667 mots !');}
		if(nb == "50000") {notif('#9B59B6', '<b>Trophé NaNoWriMo</b> : Bravo, tu a atteint les 50 000 mots !');}
	}