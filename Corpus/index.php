<!doctype html>
<html>
<head><meta charset="utf-8"/><title>Analyse fréquentielle textes</title></head>
<body>
<h1>Analyse fréquentielle</h1>
<p>J’essaye de faire une analyse fréquentielle de textes, avec les mots revenant le plus souvent. Malheureusement, il faut virer des mots trop courants, comme « le », « alors »… Pouvez-vous m’aider ? Il suffit de tester un texte en dessous, et de supprimer les mots inutiles ! Merci !</p>
<form method="post" action="index.php">
<a href="?reset=true">Remettre à zéro</a>
<textarea name="corpus" id="corpus" rows="15O" cols="50"></textarea>
<input type="submit">
</form>
<?php
if(isset($_POST['corpus'])) {
	$txt = file_get_contents('corpus.txt');
	file_put_contents('corpus.txt', $txt.' '.$_POST['corpus']);
}
		$words_blacklist = json_decode(file_get_contents('blacklist.json'),true);
		$array = array_count_values(str_word_count(strip_tags(file_get_contents('corpus.txt')), 1, 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'));
		arsort($array);
		foreach($array as $words => $number) {
			if(!in_array($words, $words_blacklist) AND $number > 1) {
				echo '<span title="'.$number.'">'.$words.'</span><a href="?blacklist='.$words.'" style="text-decoration:none; color:red; vertical-align:top;">&#10060;</a> ';
			}
		}
		
if(isset($_GET['blacklist'])) {
	$words_blacklist[] = $_GET['blacklist'];
	file_put_contents('blacklist.json', json_encode($words_blacklist));
	echo '<script>window.location = "'.'http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].'"</script>';
}
if(isset($_GET['reset'])) {
	unlink('corpus.txt');
	unlink('blacklist.json');
}
?>
</body>
</html>