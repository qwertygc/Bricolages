<?php
error_reporting(-1);
session_start();
define('SALT', 'aoirze661SD4ertertZEertert54151er65414ze6rterer');
$bdd = new PDO('sqlite:database.db');
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
function file_get_contents_safe($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}
if(!is_dir('lib')) {
	mkdir('lib');
	file_put_contents('lib/form.class.php', file_get_contents_safe('http://champlywood.free.fr/bidouilles/dl/form.class.phps'));
	$bdd->query('CREATE TABLE "books" ("id" INTEGER PRIMARY KEY  NOT NULL ,"isbn" TEXT DEFAULT (null), "title" TEXT DEFAULT (null), "author" TEXT DEFAULT (null));');
	$bdd->query('CREATE TABLE "clients" ("id" INTEGER PRIMARY KEY  NOT NULL ,"name" TEXT DEFAULT (null));');
	$bdd->query('CREATE TABLE "loans" ("id" INTEGER PRIMARY KEY  NOT NULL ,"id_client" TEXT DEFAULT (null), "id_book" TEXT DEFAULT (null), "time" INTEGER);');
	file_put_contents('config.php', "<?php define('PASSWORD', '".biblio_pwd_hash('helloworld')."');");
	redirect('index.php');
}
include 'lib/form.class.php';
include 'config.php';
function sql($query, $array=array()) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
}
function search_isbn($isbn, $db) {
	$array = array();
	$array['isbn'] = $isbn;
	$openlibrary = json_decode(file_get_contents('https://openlibrary.org/api/books?bibkeys=ISBN:'.$isbn.'&jscmd=details&format=json'), true);
	$array['title'] = $openlibrary['ISBN:'.$isbn]['details']['title'];
	$array['author'] = $openlibrary['ISBN:'.$isbn]['details']['authors'][0]['name'];
	return $array;
}
function redirect($url) {
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	header('Location: '.$url);
	die;
}
function msg($txt) {
	echo '<div class="msg">'.$txt.'</div>';
}
function biblio_pwd_hash($pwd) {
	if(PHP_VERSION_ID >=50616) {
		return password_hash($pwd, PASSWORD_DEFAULT);
	}
	else {
		return crypt($pwd, SALT);
	}
}
function biblio_pwd_verify($pwd, $hash) {
	if(PHP_VERSION_ID >=50616) {
		return password_verify($pwd, $hash);
	}
	else {
		return crypt($pwd, SALT)==$hash;
	}
}
include 'inc/tpl/header.php';
if(isset($_POST['password'])) {
	if(biblio_pwd_verify($_POST['password'], PASSWORD)) {
		$_SESSION['login'] = true;
		$_SESSION['token'] = md5(uniqid(rand(), true));
		// if (password_needs_rehash($_POST['password'], PASSWORD_DEFAULT)) {
			// file_put_contents('config.php', "<?php define('PASSWORD', '".biblio_pwd_hash($_POST['password'])."');");
		// }
		redirect('index.php');
	}
	else {
		msg('Try again ;-)');
		echo biblio_pwd_hash($_POST['password']);
	}
}
if(isset($_GET['action']) and $_SESSION['login'] == true){
	switch ($_GET['action']) {
		case 'logout':
			$_SESSION = array();
			session_destroy();
			redirect('index.php');
		break;
		case 'listbooks':
			$form = New form(array('action'=>'index.php?action=searchbook', 'method'=>'get'));
			$form->startfieldset();
			$form->legend('Rechercher un livre');
			$form->label('', 'ISBN 13');
			$form->input(array('type'=>'text', 'name'=>'isbn', 'required'=>'required', 'pattern'=>'.*([0-9]{1}[^0-9]*){9}.*'));
			$form->endfieldset();
			$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
			$form->endform();
			$sql = $bdd->query('SELECT * from books ORDER BY id desc');
			echo '<table class="sortable"><tr><th>Supprimer</th><th>modifier</th><th>Prêter</th><th>Titre</th><th>Auteur</th><th>ISBN</th></tr>';
			while($data = $sql->fetch()) {
				$statut = $bdd->query('SELECT count(*) as count from loans WHERE id_book = '.$data['id']);
				$data_count = $statut->fetch();
				$pret = ($data_count['count'] == 0) ? '<a href="?action=loan&id_book='.$data['id'].'">Prêter</a>' : 'Prêté !';
				echo '<tr><td><a href="?action=addbook&delete='.$data['id'].'&token='.$_SESSION['token'].'">&#10060;</a></td><td><a href="?action=addbook&modify='.$data['id'].'">&#9999;</a></td><td>'.$pret.'</td><td>'.$data['title'].'</td><td>'.$data['author'].'</td><td><a href="http://www.books-by-isbn.com//cgi-bin/isbn-lookup.pl?isbn='.$data['isbn'].'">'.$data['isbn'].'</a></td></tr>';
			}
			echo '</table>';
		break;
		case 'listloans':
		echo '<table><tr><th>Rendre le livre</th><th>Livre</th><th>Nom</th><th>Date d’emprunt</th><th>Date de retour</th></tr>';
		$sql = $bdd->query('SELECT b.id, b.title as title, c.id, c.name as name, l.id as id, l.id_client, l.id_book, l.time as time FROM books b, clients c, loans l WHERE  c.id = l.id_client AND  l.id_book = b.id ORDER BY time desc');
		while($data = $sql->fetch()) {
			$warning = (time() >= $data['time']+31536000) ? 'background-color:#f2dede;border-color:#eed3d7;color:#b94a48;' : '';
			echo '<tr><td><a href="?action=loan&delete='.$data['id'].'&token='.$_SESSION['token'].'">&#10060;</a><td>'.$data['title'].'</td><td>'.$data['name'].'</td><td>'.date('d-m-Y', $data['time']).'</td><td style="'.$warning.'">'.date('d-m-Y', $data['time']+31536000).'</td></tr>';
		}
		echo '</table>';
		break;
		case 'listclients':
			$sql = $bdd->query('SELECT * from clients');
			echo '<table><tr><th>Supprimer</th><th>#</th><th>Nom</th></tr>';
			while($data = $sql->fetch()) {
				echo '<tr><td><a href="?action=addclient&delete='.$data['id'].'&token='.$_SESSION['token'].'">&#10060;</a></td><td>'.$data['id'].'</td><td>'.$data['name'].'</td></tr>';
			}
			echo '<table>';
		break;
		case 'searchbook':
			if(isset($_GET['isbn'])) {
				$statut = $bdd->prepare('SELECT id from books WHERE isbn = ?');
				$statut->execute(array($_GET['isbn']));
				$data = $statut->fetch();
				if($data['id'] == null) {
					redirect('index.php?action=addbook&isbn='.$_GET['isbn']);
				}
				else {
					echo $data['id'];
					redirect('index.php?action=loan&id_book='.$data['id']);
				}
			}
		break;
		case 'addbook':
			echo '<a href="zxing://scan/?ret='.urlencode('http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].'?action=searchbook&isbn={RAWCODE}').'" class="button">Scanner</a>';
			if(isset($_GET['delete']) AND $_GET['token'] == $_SESSION['token']) {
				sql('DELETE FROM books WHERE id=?', array($_GET['delete']));
				redirect('?action=listbooks');
			}
			$_GET['isbn'] = (isset($_GET['isbn'])) ? $_GET['isbn'] : null;
			// $isbn = 9780631188179;
			if(isset($_GET['modify'])) {
				$sql = $bdd->prepare('SELECT * from books where id=?');
				$sql->execute(array($_GET['modify']));
				$data = $sql->fetch();
				$_GET['isbn'] = $data['isbn'];
				$title = $data['title'];
				$author = $data['author'];
				$id_book = $data['id'];
			}
			else {
				$isbn = $_GET['isbn'];
				$title = null;
				$author = null;
				$id_book = 0;
			}
			$form = New form(array('action'=>'index.php?action=addbook&isbn='.$_GET['isbn'], 'method'=>'post'));
			$form->startfieldset();
			$form->legend('Ajouter un livre');
			$form->label('', 'ISBN 13');
			$form->input(array('type'=>'text', 'name'=>'isbn', 'value'=>$_GET['isbn'], 'required'=>'required','pattern'=>'.*([0-9]{1}[^0-9]*){9}.*'));
			$form->endfieldset();
			$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
			$form->endform();
			if(isset($_POST['isbn'])) {
				$i = search_isbn($_POST['isbn'],DB);
				if(isset($i['title'])) {
					if(isset($_GET['modify'])) {
						sql('UPDATE books SET isbn=?, title=?, author=? WHERE id=?', array($i['isbn'], $i['title'], $i['author'], $i['id_book']));
						redirect('index.php?action=addbook');
					}
					else {
						sql('INSERT INTO books(isbn, title, author) VALUES(?,?,?)', array($i['isbn'], $i['title'],$i['author']));
						redirect('index.php?action=listbooks');
					}
				}
				else {
					msg('Le livre n’est pas dans la base de donnée !');
					$isbn = $_GET['isbn'];
				}
			}
			$form = New form(array('action'=>'index.php?action=addbook', 'method'=>'post'));
			$form->startfieldset();
			$form->legend('Ajouter un manuellement un livre');
			$form->label('', 'ISBN 13');
			$form->input(array('type'=>'text', 'name'=>'isbn_edit', 'value'=>$_GET['isbn'], 'required'=>'required','pattern'=>'.*([0-9]{1}[^0-9]*){9}.*'));
			$form->label('', 'Titre');
			$form->input(array('type'=>'text', 'name'=>'title', 'value'=>$title));
			$form->label('', 'Auteur');
			$form->input(array('type'=>'text', 'name'=>'author', 'value'=>$author));
			$form->input(array('type'=>'hidden', 'name'=>'id_book', 'value'=>$id_book));
			$form->endfieldset();
			$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
			$form->endform();
			if(isset($_POST['id_book'])) {
				if($_POST['id_book'] == 0) {
					sql('INSERT INTO books(isbn, title, author) VALUES(?,?,?)', array($_POST['isbn_edit'], $_POST['title'], $_POST['author']));
					redirect('index.php?action=listbooks');
				}
				else {
					sql('UPDATE books SET isbn=?, title=?, author=? WHERE id=?', array($_POST['isbn_edit'], $_POST['title'], $_POST['author'], $_POST['id_book']));
					redirect('index.php?action=listbooks');
				}
			}
		break;
		case 'addclient':
			if(isset($_GET['delete']) AND $_GET['token'] == $_SESSION['token']) {
				sql('DELETE FROM clients WHERE id=?', array($_GET['delete']));
				redirect('?action=listclients');
			}
			$form = New form(array('action'=>'index.php?action=addclient', 'method'=>'post'));
			$form->startfieldset();
			$form->legend('Ajouter un client');
			$form->label('', 'Nom');
			$form->input(array('type'=>'text', 'name'=>'name', 'required'=>'required'));
			$form->endfieldset();
			$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
			$form->endform();
			if(isset($_POST['name'])) {
				sql('INSERT INTO clients(name) VALUES(?)', array($_POST['name']));
				redirect('index.php?action=listclients');
			}
		break;
		
		case 'loan':
			if(isset($_GET['delete']) AND $_GET['token'] == $_SESSION['token']) {
				sql('DELETE FROM loans WHERE id=?', array($_GET['delete']));
				redirect('?action=listloans');
			}
			$listbooks = array();
			$listclients = array();
			$sqlbook = $bdd->query('SELECT id, title from books');
			while($data = $sqlbook->fetch()) {
				$listbooks[$data['id']] = $data['title'];
			}
			$sql = $bdd->query('SELECT id, name from clients');
			while($data = $sql->fetch()) {
				$listclients[$data['id']] = $data['name'];
			}
			$form = New form(array('action'=>'index.php?action=loan', 'method'=>'post'));
			$form->startfieldset();
			$form->legend('Ajouter un prêt');
			$form->label('id_client', 'Emprunteur');
			$_GET['id_client'] = isset($_GET['id_client']) ? $_GET['id_client'] : 0;
			$_GET['id_book'] = isset($_GET['id_book']) ? $_GET['id_book'] : 0;
			$form->select(array('name'=>'id_client'), $listclients, (int)$_GET['id_client'], true);
			$form->label('id_book', 'Livre');
			$form->select(array('name'=>'id_book'), $listbooks, (int)$_GET['id_book'], true);
			$form->startfieldset();
			$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
			$form->endform();
			if(isset($_POST['id_book'])) {
				sql('INSERT INTO loans(id_client, id_book, time) VALUES(?,?,?)', array($_POST['id_client'], $_POST['id_book'], time()));
				redirect('index.php?action=listloans');
			}
		break;
		case 'config':
			if(!function_exists('password_hash')){msg('La fonction password_hash n’est pas disponible !');}
			$form = New form(array('action'=>'index.php?action=config', 'method'=>'post'));
			$form->label('old_password', 'Ancien mot de passe');
			$form->input(array('type'=>'password', 'name'=>'old_password', 'required'=>'required'));
			$form->label('new_password', 'Nouveau mot de passe');
			$form->input(array('type'=>'password', 'name'=>'new_password', 'required'=>'required'));
			$form->label('check_password', 'Confirmation du nouveau mot de passe');
			$form->input(array('type'=>'password', 'name'=>'check_password', 'required'=>'required'));
			$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
			$form->endform();
			if(isset($_POST['new_password'])) {
				if(biblio_pwd_verify($_POST['old_password'], PASSWORD)) {
					if($_POST['new_password'] == $_POST['check_password']) {
						file_put_contents('config.php', "<?php define('PASSWORD', '".biblio_pwd_hash($_POST['new_password'])."');");
						msg('Votre mot de passe a bien été changé.');
					}
					else {
						msg('Les mots de passes ne concordent pas.');
					}
				}
				else {
					msg('Votre mot de passe est incorrect.');
				}
			}
		break;
	}
}
else {
	if(!isset($_SESSION['login'])) {
		$form = New form(array('action'=>'index.php', 'method'=>'post'));
		$form->label('password', 'Mot de passe');
		$form->input(array('type'=>'password', 'name'=>'password', 'required'=>'required'));
		$form->input(array('type'=>'submit', 'value'=>'Envoyer'));
		$form->endform();
	}
	else {
		redirect('index.php?action=listloans');
	}
}
include 'inc/tpl/footer.php';
?>
