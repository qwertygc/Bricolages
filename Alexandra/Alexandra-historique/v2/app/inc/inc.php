<?php
include 'form.class.php';
function sql($query, $array) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
}
function list_emprunteur() {
global $bdd;
	$retour = $bdd->query('SELECT * FROM emprunteur ORDER BY id DESC');
	echo '<select name="pret_emprunteur">'.PHP_EOL;
	while($donnees = $retour->fetch()) {echo '<option value="'.$donnees['id'].'">'.$donnees['emprunteur'].'</option>'.PHP_EOL;}
	echo '</select>';
}
function list_book($book) {
	global $bdd;
	$retour = $bdd->query('SELECT * FROM livre ORDER BY id DESC');
	echo '<select name="pret_livre">'.PHP_EOL;
	$select = '';
	while($donnees = $retour->fetch()) {
		if($donnees['id'] == $book) {$select='selected'; } else {$select ='';}
		echo '<option value="'.$donnees['id'].'" '.$select.'>'.$donnees['titre'].' - '.$donnees['auteur'].'</option>'.PHP_EOL;
	}
	echo '</select>';
}