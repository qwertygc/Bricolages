CREATE TABLE `pret` (
  `id` INTEGER PRIMARY KEY,
  `id_livre` INTEGER  NOT NULL,
  `id_emprunteur` INTEGER  NOT NULL,
  `timestamp` bigint(20) NOT NULL
);
CREATE TABLE  `emprunteur` (
  `id` INTEGER PRIMARY KEY,
  `emprunteur` varchar(255) NOT NULL
);

CREATE TABLE  `livre` (
  `id`INTEGER PRIMARY KEY,
  `titre` varchar(255) NOT NULL,
  `auteur` varchar(255) NOT NULL
);

