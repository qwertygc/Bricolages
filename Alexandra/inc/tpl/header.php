<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<link rel="stylesheet" href="inc/assets/style.css">
<title>Bibliothèque</title>
<script src="inc/assets/sorttable.js"></script>
</head>
<body>
<header id="header">
<h1><a href="?">Bibliothèque</a></h1>
<nav>
<?php
if(isset($_SESSION['login'])) {
?>
<a href="?action=listbooks">Liste des livres</a>
<a href="?action=listloans">Liste des prêts</a>
<a href="?action=listclients">Liste des clients</a>
<a href="?action=addbook">Ajouter un livre</a>
<a href="?action=addclient">Ajouter un client</a>
<a href="?action=loan">Ajouter un prêt</a>
<a href="?action=config">Configuration</a>
<a href="?action=logout">Déconnexion</a>
<?php
}
?>
</nav>
</header>
<main id="main">