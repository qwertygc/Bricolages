<?php
#######################################
# Ecrire dans posts/ en markdown. Le format du fichier doit être YYYYMMDD_le-titre-avec-les-tirets-qui-sera-transforme-en-espace
# Pour envoyer sur le serveur, voici un script en .sh
/*
#!/bin/sh

HOST="ftp.monsite.tld"
USER="webmaster@monsite.tld"
PASS="vive les bisounours"
LCD="/Users/bisounours/mon_site_de_la_mort/...."
RCD="www"

lftp -c "set ssl:verify-certificate false; set ftp:list-options -a;
open ftp://$USER:$PASS@$HOST;
lcd $LCD;
cd $RCD;
mirror --reverse \
       --delete \
       --verbose"
*/
#######################################
function md_dir($dir) {
	if(!is_dir($dir)) { mkdir($dir,0755);}
	return $dir;
}
function write_article($file) {
	$name = explode("_", $file);
	$md = New Parsedown();
	$page = file_get_contents('config/tpl/header.html');
	$page .= '<article>';
	$page .='<header><h1>'.format_title($name[1]).'</h1>';
	$page .='<time>'.date('d m Y', strtotime($name[0])).'</time></header><main>';
	$page .= $md->text(file_get_contents('posts/'.$file));
	$page .= '</main></article>';
	$page .= file_get_contents('config/tpl/footer.html');
	file_put_contents('output/'.$file.'.html', $page);
}
function write_atom($post) {
	$md = New Parsedown();
	$name = explode("_", $post);
	return '<entry>
   <title>'.format_title($name[1]).'</title>
   <link href="'.ROOT.'/'.$post.'.html"/>
   <updated>'.date(DATE_ATOM, strtotime($name[0])).'</updated>
   <content type="xhtml">'.$md->text(file_get_contents('posts/'.$post)).'</content></entry>';
}
function erased_dir($dir) {
	if(!is_dir($dir)) {
		throw new InvalidArgumentException("$dir must be a directory");
	}
	if(substr($dir,strlen($dir)-1,1) != '/') {
		$dir .= '/';
	}
	$files = glob($dir.'*',GLOB_MARK);
	if($files) {
		foreach ($files as $file) {
			if(is_dir($file)) {
				erased_dir($file);
			} 
			else {
				unlink($file);
			}
		}
	}
	rmdir($dir);
}
function scan_dir($dir) { # source = http://stackoverflow.com/questions/11923235/scandir-to-sort-by-date-modified
    $ignored = array('.', '..', '.svn', '.htaccess');

    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);

    return ($files) ? $files : false;
}
function format_title($str) { #format title _ is space
	return strtr($str,'-',' ');
}

#################################################
#						Installation			#
#################################################
if(!is_dir('config/')) {
	md_dir('posts/');
	md_dir('output/');
	md_dir('config/');
	md_dir('config/tpl/');
	md_dir('lib/');
		file_put_contents('config/config.php', "
	<?php
	define('SITE_TITLE', 'A average website');
	define('ROOT', 'http://example.org');
	define('AUTHOR_NAME', 'John Doe');
	define('AUTHOR_EMAIL', 'john.doe@example.org');");
	include 'config/config.php';
	file_put_contents('lib/Parsedown.php', file_get_contents('https://raw.githubusercontent.com/erusev/parsedown/master/Parsedown.php'));
	file_put_contents('config/tpl/header.html', '<!doctype html>
<html>
<head>
<title'.SITE_TITLE.'</title>
<style>
@charset "UTF-8";
/* RESET */
article,aside,details,figcaption,figure,footer,header,hgroup,main,nav,section,summary{display:block;}
audio,canvas,video{display:inline-block;}
summary{margin:0 0 .75em;}
html{font-size:100%;}
body{background:#fff;color:#333;font-family:Arial, helvetica, sans-serif;font-size:1.4em;line-height:1.5;margin:0;}
h1{display:block;font-size:1.8571em;line-height:1.6154;margin:1.6154em 0 .8077em;}
h2{display:block;font-size:1.7143em;line-height:1.75;margin:1.75em 0 .875em;}
h3{display:block;font-size:1.5714em;line-height:1.909;margin:1.909em 0 .9545em;}
h4{display:block;font-size:1.4286em;line-height:1.05;margin:2.1em 0 1.05em;}
h5{display:block;font-size:1.2857em;line-height:1.1667;margin:2.3334em 0 1.1667em;}
h6{display:block;font-size:1.1429em;line-height:1.3125;margin:2.625em 0 1.3125em;}
blockquote,q{quotes:none;}
blockquote:before,blockquote:after,q:before,q:after{content:"";}
pre{white-space:pre-wrap;word-wrap:break-word;}
code,kbd,pre,samp,tt,var{font-family:menlo,monaco,consolas,"courier new",monospace;font-style:normal;line-height:normal;}
abbr,acronym,dfn{border-bottom:1px dotted;cursor:help;font-style:normal;font-variant: small-caps;}
a:focus{outline:1px dotted;}
img{border:0;}
ul,ol{padding:0 0 0 2em;}
li{margin:0 0 0 2em;}
li ul,li ol{margin:0;}
hr{border:0;border-bottom:1px solid;}
big{font-size:1.25em;}
small,sub,sup{font-size:.875em;}
sub{vertical-align:bottom;top:.5ex;}
sup{vertical-align:top;bottom:1ex;}
del,s,strike{text-decoration:line-through;}
mark{background:#ff0;color:#000;}
fieldset{border:1px solid;padding:1em;}
legend{font-weight:700;padding:0 .25em;}
input,textarea,select,button{font-family:inherit;font-size:1em;line-height:normal;}
input[type=button],input[type=image],input[type=reset],input[type=submit],button[type=button],button[type=reset],button[type=submit]{cursor:pointer;}
table{border:0;border-collapse:collapse;border-spacing:0;table-layout:fixed;margin-bottom:1.5em;}
th,td{border:1px solid;padding:.25em .5em;}
caption{padding:0 0 1em;}
audio,canvas,details,figure,video,address,blockquote,dl,fieldset,form,hr,menu,ol,p,pre,q,table,ul{margin:0 0 1.25em;}
dd{margin:1px 0 1.25em;}
b,strong,dt,th{font-weight:700;}
textarea,caption,th,td{text-align:left;vertical-align:top;}
q:lang(fr){quotes:"« " " »" "“" "”";}
q:lang(it){quotes:"«" "»" "“" "”" "‘" "’";}
q:lang(en){quotes:"“" "”" "‘" "’";}
q::before{content:open-quote;}
q::after{content:close-quote;}
sup,sub{vertical-align:0;position:relative;}
h1:first-child,h2:first-child,h3:first-child,h4:first-child,h5:first-child,h6:first-child{margin-top:0;}
p:last-child,ul:last-child,ol:last-child,dl:last-child,blockquote:last-child,pre:last-child,table:last-child{margin-bottom:0;}
li p,li ul{margin-bottom:0;margin-top:0;}
textarea,table,td,th,code,pre,samp,div,p{word-wrap:break-word;hyphens:auto;}
code,pre,samp{white-space:pre-wrap;}
kbd{border:solid 1px;border-top-left-radius:.5em;border-top-right-radius:.5em;padding:0 .25em;}
abbr[title]{border-bottom:dotted 1px;cursor:help;}
a:link img,a:visited img,img{border-style:none;}
blockquote,q,cite,i{font-style:italic;}
sub,sup,code{line-height:1;}
ins,u{text-decoration:underline;}
img,table,td,blockquote,code,pre,textarea,input{height:auto;box-sizing: border-box;}
/*STRUCTURE */
html, body {
	font-family: Futura, "Trebuchet MS", Arial, sans-serif;
	font-weight: 400;
	line-height: 1.5;
	font-size:100%;
	max-width:500px;
	margin:auto;
}
a {color:#000;}
</style>
</head>
<body>
<h1><a href="'.ROOT.'>'.SITE_TITLE.'</a></h1>');
	file_put_contents('config/tpl/footer.html', '<footer><a href="feed.xml">Atom Feed</a> - '.AUTHOR_NAME.'</footer>
</body>
</html>');
}
#################################################
#				Generation						#
#################################################
include 'lib/Parsedown.php';
include 'config/config.php';
md_dir('posts/');
md_dir('output/');
md_dir('config/');
md_dir('config/tpl/');
md_dir('lib/');
$posts = scan_dir('posts/');
$index = file_get_contents('config/tpl/header.html');
$index .= '<ul>';
$feed = '<?xml version="1.0" encoding="utf-8"?><feed xmlns="http://www.w3.org/2005/Atom"><title>'.SITE_TITLE.'</title><link href="'.ROOT.'"/><author><name>'.AUTHOR_NAME.'</name><email>'.AUTHOR_EMAIL.'</email></author>';
foreach($posts as $post) {
	$name = explode("_", $post);
	$index .= '<li><time>'.date("d m Y", strtotime($name[0])).'</time> <a href="'.$post.'.html">'.format_title($name[1]).'</a></li>';
	$feed .=write_atom($post);
	write_article($post);
}
$index .='</ul>';
$index .= file_get_contents('config/tpl/footer.html');
$feed .='</feed>';
file_put_contents('output/index.html', $index);
file_put_contents('output/feed.xml', $feed);
