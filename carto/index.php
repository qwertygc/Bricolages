<style>
.block {
	height:2em;
	width:2em;
	display:inline-block;
	}
	input[type="range"] {
    position: relative;
    margin-left: 0px;
	width:150px;
}
img[src="gradient.jpg"] {
	width:150px;
}
</style>
<?php
    function fGetRGB($iH, $iS, $iV) {
        if($iH < 0)   $iH = 0;   // Hue:
        if($iH > 360) $iH = 360; //   0-360
        if($iS < 0)   $iS = 0;   // Saturation:
        if($iS > 100) $iS = 100; //   0-100
        if($iV < 0)   $iV = 0;   // Lightness:
        if($iV > 100) $iV = 100; //   0-100
        $dS = $iS/100.0; // Saturation: 0.0-1.0
        $dV = $iV/100.0; // Lightness:  0.0-1.0
        $dC = $dV*$dS;   // Chroma:     0.0-1.0
        $dH = $iH/60.0;  // H-Prime:    0.0-6.0
        $dT = $dH;       // Temp variable
        while($dT >= 2.0) $dT -= 2.0; // php modulus does not work with float
        $dX = $dC*(1-abs($dT-1));     // as used in the Wikipedia link
        switch(floor($dH)) {
            case 0:
                $dR = $dC; $dG = $dX; $dB = 0.0; break;
            case 1:
                $dR = $dX; $dG = $dC; $dB = 0.0; break;
            case 2:
                $dR = 0.0; $dG = $dC; $dB = $dX; break;
            case 3:
                $dR = 0.0; $dG = $dX; $dB = $dC; break;
            case 4:
                $dR = $dX; $dG = 0.0; $dB = $dC; break;
            case 5:
                $dR = $dC; $dG = 0.0; $dB = $dX; break;
            default:
                $dR = 0.0; $dG = 0.0; $dB = 0.0; break;
        }
        $dM  = $dV - $dC;
        $dR += $dM; $dG += $dM; $dB += $dM;
        $dR *= 255; $dG *= 255; $dB *= 255;
        return array (
		round($dR),
		round($dG),
		round($dB));
    }
?>
<form method="post">
<label><p>Choississez votre couleur</p><img src="gradient.jpg"/><br><input type="range" name="teinte" min="0" max="360" value="297"/></label>
<label><p>Choississez le nombre de classes</p><input type="number" name="nb_class" min="1" value="5"/></label>
<label><p>Choississez le niveau de contraste</p><input type="range" name="nuance" min="0" max="100" value="100"/></label>
<br><input type="submit"/>
</form>
<?php
$teinte = (isset($_POST['teinte'])) ? $_POST['teinte'] : 297;
$nb_class = (isset($_POST['nb_class'])) ? $_POST['nb_class'] : 5;
$nuance = (isset($_POST['nuance'])) ? $_POST['nuance'] : 100;

$diff = intval($nuance/$nb_class);
for($i = 1; $i<= $nb_class; $i++) {
	$s = (0+($diff*$i));
	$l = (100-($diff*$i));
	echo '<div class="block" style="background:hsl('.$teinte.', '.$s.'%, '.$l.'%);">
	'.fGetRGB($teinte, $s, $l)[0].fGetRGB($teinte, $s, $l)[1].fGetRGB($teinte, $s, $l)[2].'
	</div>';
}
?>
