<?php
function sql($query, $array=array()) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
}
#http://snipplr.com/view/20671/short-url-function/
$chrs = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
function int_to_alph($int) {
	global $chrs;
	$alph = null;
	$base = sizeof($chrs);
	do {
		$alph = $chrs[($int % $base)] . $alph;
	}
	while($int = intval($int / $base));
	return $alph;
}
$bdd = new PDO('sqlite:database.db');
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if(!file_exists('Parsedown.php')) {
	$bdd->query('CREATE TABLE "content" ("id" INTEGER PRIMARY KEY  NOT NULL, "content" TEXT DEFAULT (null), "pwd" TEXT, "time" TEXT, "ip" TEXT);');
	file_put_contents('Parsedown.php', file_get_contents('https://raw.githubusercontent.com/erusev/parsedown/master/Parsedown.php'));
	file_put_contents('ParsedownExtra.php', file_get_contents('https://raw.githubusercontent.com/erusev/parsedown-extra/master/ParsedownExtra.php'));
}
else {
	include 'Parsedown.php';
	include 'ParsedownExtra.php';
}
error_reporting(-1);
define('ROOT', 'http://localhost/anonpost/');
?>
<!doctype html>
<html>
<head>
	<title>AnonPost</title>
	<style>
body {margin:auto; max-width:900px;}
label {display:block;}
	</style>
	<meta charset="utf-8"/>
</head>
<body>
<?php
if(isset($_GET['admin'])) {
	$something = $bdd->prepare('SELECT * FROM content');
	$something->execute();
	while($data = $something->fetch()) {
		echo date('c', $data['time']).' - '.$data['ip'].' - '.'<a href="'.ROOT.'?id='.$data['id'].'&edit='.$data['pwd'].'">Modifier le lien</a> <a href="?delete='.$data['id'].'&pwd='.$data['pwd'].'">Supprimer le lien</a><br>';
		$md = new ParsedownExtra();
		echo $md->text($data['content']);
		echo '<hr>';
	}
}
if(isset($_GET['id']) AND !isset($_GET['edit'])) {
	$something = $bdd->prepare('SELECT content FROM content WHERE id=?');
	$something->execute(array($_GET['id']));
	$data = $something->fetch();
	$md = new ParsedownExtra();
	echo $md->text($data['content']);
}
else {
	if(isset($_GET['edit'])) {
		$something = $bdd->prepare('SELECT * FROM content WHERE id=?');
		$something->execute(array($_GET['id']));
		$data = $something->fetch();
	}
	else {
		$data = array(
			'content' => null,
			'pwd' => null,
		);
	}
?>
	<h1>Create a post</h1>
	<form action="index.php" method="post">
		<label for="content">Contenu (formatage markdown)</label><textarea id="content" name="content" required><?php echo $data['content']; ?></textarea>
		<?php if(isset($_GET['edit'])) {?>
			<input value="<?php echo $_GET['edit']; ?>" type="hidden" name="pwd" id="pwd"/>
			<input value="<?php echo $_GET['id']; ?>" type="hidden" name="id" id="id"/>
			<br><a href="?delete=<?php echo $_GET['id'];?>&pwd=<?php echo $_GET['edit']; ?>">Supprimer le post</a><br>
		<?php } ?>
		<input type="submit"/>
	</form>
<?php
	if(isset($_GET['delete'])) {
		$something = $bdd->prepare('SELECT * FROM content WHERE id=?');
		$something->execute(array($_GET['delete']));
		$data = $something->fetch();
		if($data['pwd'] == $_GET['pwd']) {
			sql('DELETE FROM content WHERE id=?', array($_GET['delete']));
			echo 'Ok, le contenu a été supprimé !';
		}
	}
	if(isset($_POST['content'])) {
		if(isset($_POST['pwd'])) {
			$something = $bdd->prepare('SELECT * FROM content WHERE id=?');
			$something->execute(array($_POST['id']));
			$data = $something->fetch();
			if($data['pwd'] == $_POST['pwd']) {
			sql('UPDATE content SET content=?;', array($_POST['content']));
			echo 'votre page : <b>'.ROOT.'?id='.$_POST['id'].'</b><br>';
			echo 'lien pour modifier votre page : <b>'.ROOT.'?id='.$_POST['id'].'&edit='.$_POST['pwd'].'</b>';

			}
		}
		else {
			$pwd = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),0,10);
			sql('INSERT INTO "content"(content,pwd,time,ip) VALUES (?,?,?,?)', array($_POST['content'],$pwd,time(),$_SERVER['REMOTE_ADDR']));
			echo 'votre page : <b>'.ROOT.'?id='.$bdd->lastInsertId().'</b><br>';
			echo 'lien pour modifier votre page : <b>'.ROOT.'?id='.$bdd->lastInsertId().'&edit='.$pwd.'</b>';
		}
	}
}
?>
</body>
</html>