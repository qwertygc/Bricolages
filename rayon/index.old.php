<?php
function geoloc($loc) {
$options = array(
	'http'=>array(
		'method'=>"GET",
		'header'=>"Accept-language: en\r\n".
				  "Cookie: foo=bar\r\n".
				"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\r\n")
		);
		$geocodage = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search/?q='.$loc.'&format=json&addressdetails=1&limit=1', false,  stream_context_create($options)), true);
		$lat = $geocodage[0]['lon'];
		$lon =  $geocodage[0]['lat'];
		/*$geocodage = json_decode(file_get_contents('https://api-adresse.data.gouv.fr/search/?q='.$loc), true);
		$lat = isset($geocodage['features'][0]['geometry']['coordinates']['0']) ? $geocodage['features'][0]['geometry']['coordinates']['0'] : '44.366';
		$lon = isset($geocodage['features'][0]['geometry']['coordinates']['1']) ? $geocodage['features'][0]['geometry']['coordinates']['1'] : '4.5332';*/


		return array('lat'=>$lat, 'lon'=>$lon);
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js"></script>
    <title>Tutoriel Leaflet OSM</title>
  </head>

  <body>
<?php
	$loc = (isset($_POST['loc'])) ? $_POST['loc'] : '';
	$rayon = (isset($_POST['rayon'])) ? $_POST['rayon'] : 1;
?>
	<form method="post" action="index.php">
		<input type="text" name="loc" id="loc" value="<?php echo $loc; ?>">
		<input type="number" name="rayon" id="rayon" value="<?php echo $rayon; ?>"/>
		<input type="submit"/>
	</form>
    <div id="macarte" style="width:100%; height:90vh"></div>
<?php
if(isset($_POST['loc'])) {
	$loc = geoloc($_POST['loc']);
}
?>
    <script>
      var carte = L.map('macarte').setView([46.3630104, 2.9846608], 6);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(carte);
	<?php
if(isset($_POST['loc'])) {
	$rayon = $_POST['rayon'] * 1000;
	echo 'var marker = L.marker(['.$loc['lon'].', '.$loc['lat'].']).addTo(carte);
	var influence = L.circle(['.$loc['lon'].', '.$loc['lat'].'], '.$rayon.').addTo(carte);';
}
	?>
    </script>
  </body>

</html>
