<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8" />
  <link rel="stylesheet" media="screen, handheld, tv, projection" type="text/css" href="design.css"/>
  <link rel="alternate" type="application/rss+xml" title="" href="rss.php" />
</head>
<body>
<header>
<h1>Services<sup style="color:green">bêta</sup></h1>
<a class="button" href="#soumettre">Contribuer</a>
<a href="rss.php"><img src="rss.png" alt="rss"></a>
</header>
