<?php
header ("Content-type: image/png");
$image = imagecreate(200,30);
 
$orange = imagecolorallocate($image, 255, 128, 0); // Le fond est orange (car c'est la première couleur)
$bleu = imagecolorallocate($image, 0, 0, 255);
$bleuclair = imagecolorallocate($image, 156, 227, 254);
$noir = imagecolorallocate($image, 0, 0, 0);
$blanc = imagecolorallocate($image, 255, 255, 255);
imagestring($image, 4, 5, 10, $_GET['mail'], $noir);
imagecolortransparent($image, $orange); // On rend le fond orange transparent
imagepng($image);
?>