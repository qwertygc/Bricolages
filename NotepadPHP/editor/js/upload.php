<?php
error_reporting(-1); // report ALL the errors.
function bytesToSize1024($bytes) {
	$unit = array('B','KB','MB');
	return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), 1).' '.$unit[$i];
}
if(!is_dir('../../upload')) { mkdir('../../upload');}
if (isset($_FILES['myfile'])) {
	$sFileName = date('YmdHis', time()).'_'.$_FILES['myfile']['name'];
	$sFileType = $_FILES['myfile']['type'];
	$sFileSize = bytesToSize1024($_FILES['myfile']['size']);
	move_uploaded_file($_FILES['myfile']['tmp_name'], '../../upload/'.$sFileName);

	echo "
<div class= \"success\">
	<p>
		Your file: {$sFileName} has been successfully received. (<a href= \"javascript:insertTag('http://".str_replace('/editor/js/upload.php','','http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'])."/{$sFileName}', '', 'textarea')\">link</a>)<br/>
		Type: {$sFileType}<br/>
		Size: {$sFileSize}
	</p>
</div>";
} else {
	echo '<div class="failure">An error occurred</div>';
}