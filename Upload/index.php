<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<style>
/* RESET */
article,aside,details,figcaption,figure,footer,header,hgroup,main,nav,section,summary{display:block;}
audio,canvas,video{display:inline-block;}
summary{margin:0 0 .75em;}
html{font-size:100%;}
body{background:#fff;color:#333;font-family:Arial, helvetica, sans-serif;font-size:1.4em;line-height:1.5;margin:0;}
h1{display:block;font-size:1.8571em;}
h2{display:block;font-size:1.7143em;}
h3{display:block;font-size:1.5714em;}
h4{display:block;font-size:1.4286em;}
h5{display:block;font-size:1.2857em;}
h6{display:block;font-size:1.1429em;}
blockquote,q{quotes:none;}
blockquote:before,blockquote:after,q:before,q:after{content:"";}
blockquote em, blockquote i {font-style:normal;}
pre{white-space:pre-wrap;word-wrap:break-word;}
code,kbd,pre,samp,tt,var{font-family:menlo,monaco,consolas,"courier new",monospace;font-style:normal;line-height:normal;}
abbr,acronym,dfn{border-bottom:1px dotted;cursor:help;font-style:normal;font-variant: small-caps;}
a:focus{outline:1px dotted;}
img{border:0;}
ul,ol{padding:0 0 0 2em;}
li{margin:0 0 0 2em;}
li ul,li ol{margin:0;}
hr{border:0;border-bottom:1px solid;}
big{font-size:1.25em;}
small,sub,sup{font-size:.875em;}
sub{vertical-align:bottom;top:.5ex;}
sup{vertical-align:top;bottom:1ex;}
del,s,strike{text-decoration:line-through;}
mark{background:#ff0;color:#000;}
fieldset{border:1px solid;padding:1em;}
legend{font-weight:700;padding:0 .25em;}
input,textarea,select,button{font-family:inherit;font-size:1em;line-height:normal;}
input[type=button],input[type=image],input[type=reset],input[type=submit],button[type=button],button[type=reset],button[type=submit]{cursor:pointer;}
table{border:0;border-collapse:collapse;border-spacing:0;table-layout:fixed;margin-bottom:1.5em;}
th,td{border:1px solid;padding:.25em .5em;}
caption{padding:0 0 1em;}
audio,canvas,details,figure,video,address,blockquote,dl,fieldset,form,hr,menu,ol,p,pre,q,table,ul{margin:0 0 1.25em;}
dd{margin:1px 0 1.25em;}
b,strong,dt,th{font-weight:700;}
textarea,caption,th,td{text-align:left;vertical-align:top;}
q:lang(fr){quotes:"« " " »" &#8220; &#8221;;}
q:lang(it){quotes:« » &#8220; &#8221; &#8216; &#8217;;}
q:lang(en){quotes:&#8220; &#8221; &#8216; &#8217;;}
q::before{content:open-quote;}
q::after{content:close-quote;}
sup,sub{vertical-align:0;position:relative;}
h1:first-child,h2:first-child,h3:first-child,h4:first-child,h5:first-child,h6:first-child{margin-top:0;}
p:last-child,ul:last-child,ol:last-child,dl:last-child,blockquote:last-child,pre:last-child,table:last-child{margin-bottom:0;}
li p,li ul{margin-bottom:0;margin-top:0;}
textarea,table,td,th,code,pre,samp,div,p{word-wrap:break-word;hyphens:auto;}
code,pre,samp{white-space:pre-wrap;}
kbd{border:solid 1px;border-top-left-radius:.5em;border-top-right-radius:.5em;padding:0 .25em;}
abbr[title]{border-bottom:dotted 1px;cursor:help;}
a:link img,a:visited img,img{border-style:none;}
blockquote,q,cite,i{font-style:italic;}
sub,sup,code{line-height:1;}
ins,u{text-decoration:underline;}
img,table,td,blockquote,code,pre,textarea,input{height:auto;max-width:100%;}/*STRUCTURE */
body {
	font-family: Palatino, "Palatino Linotype", "Palatino LT STD", "Book Antiqua", Georgia, serif;
	/*font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;*/
	margin:auto;
	max-width:800px;
	margin-top: 1em;
}
</style>
</head>
<body>
<?php
define('PASSWORD', 'p4ssw0rd');
define('UPLOAD_DIR', 'upload');
define('ROOT', 'http://localhost/upload');
?>
<h1>Téléversement</h1>
<form method="post" action="index.php" enctype="multipart/form-data">        
    Fichier : <input type="file" name="fichier[]" size="60" multiple="multiple"><br>
    Fichier distant : <input type="url" name="wget" size="60" placeholder="http://example.org/myfile.ext"><br>
   Phrase Passe : <input type="password" name="password"><br>
	<input type="submit" value="Send" name="upload">   
</form>

<?php
if(!file_exists(UPLOAD_DIR)) {mkdir(UPLOAD_DIR);}
if(isset($_POST['upload']) && PASSWORD == $_POST['password']) {
	if(isset($_POST['wget'])) {
		$namefile = date('YmdHis', time()).'_'.basename($_POST['wget']);
		file_put_contents(UPLOAD_DIR.'/'.$namefile, file_get_contents($_POST['wget']));
		echo ROOT.'/'.UPLOAD_DIR.'/'.$namefile;
	}
	else {
		$files = array();
		$fdata = $_FILES['fichier'];
		$count = count($fdata['name']);
		if(is_array($fdata['name'])){
			for($i=0;$i<$count;++$i){
				$files[]=array(
					'name'     => $fdata['name'][$i],
					'tmp_name' => $fdata['tmp_name'][$i],
					'type' => $fdata['type'][$i],
				);
			}
		}
		else {$files[]=$fdata;}
		foreach ($files as $file) {
			if(!is_uploaded_file($file['tmp_name'])) {exit('error');}
			$file['name']=date('YmdHis', time()).'_'.$file['name'];
			if(!move_uploaded_file($file['tmp_name'], UPLOAD_DIR. $file['name']) ) {exit('error');}
			echo ROOT.'/'.UPLOAD_DIR.$file['name'].'<br>';
		}
	}
}
?>
</body>
</html>