<?php
function sanitize_output($buffer) {
    $a = array(
        '/\>[^\S ]+/s' => '>',
        '/[^\S ]+\</s' => '<',
        '/(\s)+/s' => '\\1'
		);
    $buffer = preg_replace(array_keys($a), array_values($a), $buffer);
    return $buffer;
}
header('Content-type: text/html; charset=utf-8');
ob_start("sanitize_output");
// ob_start("ob_gzhandler");
$a0 = array(
	'Mesdames, messieurs,',
	'Citoyennes, citoyens,',
	'Mes chers concitoyens,',
	'Habitantes, habitants,',
	'Compatriotes,',
	'Habitants du monde libre,',
	'Françaises,  Français,',
	'Européennes, européens,',
	'Amoureuses et amoureux de la liberté,',
	'Concitoyens,',
	'Patriotes !',
	'Mes chers sujets,',
	'Peuple soumis !',
	'Peuple libre !',
	'Camarades !'
	
);
$a1 = array(
	'Je reste fondamentalement persuadé que', 
	'Sachez que je me battrai pour faire admettre que',
	'Par ailleurs, c’est en toute connaissance de cause que je peux affirmer aujourd’hui que',
	'Je tiens à vous dire ici ma détermination sans faille pour clamer haut et fort que',
	'J’ai depuis longtemps (ai-je besoin de vous le rappeler ?), défendu l’idée que',
	'Depuis toujours',
	'Depuis la fondation de notre pays',
	'Moi, Républicain, ',
	'C’est en toute conscience que je déclare avec conviction que',
	'Ce n’est certainement pas vous, mes chers compatriotes, qui me contredirez si je vous dis que',
	'Nous devons tous, et c’est une nécessité des plus pressantes, sans plus attendre répondre à',
	'Il est une chose fondamentalement républicaine et des plus morales : c’est déclarer que',
	'Il faut que nous tous, citoyens européens, clamions que',
	'Il est une chose fondamentalement républicaine et des plus morales : c’est déclarer que',
	'Il faut que nous tous, citoyens européens, clamions que',
	'Si j’ai fait de la politique, c’est dans un dessein, un seul : celui de faire comprendre à tous que',
	'Mon statut de citoyen français, européen et mondial, de défenseur de la liberté, perdrait tout son sens si je m’abstenais de dire que'
);
$a2 = array(
	'la conjoncture actuelle',
	'face au risque du terrorisme',
	'la situation d’exclusion que certains d’entre vous connaissent',
	'l’acuité des problèmes de la vie quotidienne',
	'la volonté farouche de sortir notre pays de la crise',
	'l’effort prioritaire en faveur du statut précaire des exclus',
	'le particularisme dû à notre histoire unique',
	'l’aspiration plus que légitime de chacun au progrès social', 
	'la nécessité de répondre à votre inquiétude journalière, que vous soyez jeunes ou âgés,',
	'la volonté de mettre fin aux difficultés auxquelles les plus humbles et les plus louables de nos citoyens doivent faire face et notre désir fraternel',
	'l’ambition de tout Français de privilégier les valeurs conformes aux principes constitutionnels',
	'l’urgence de la revigoration nécessaire des valeurs républicaines au sein de notre grand et beau pays face à cette situation de crise',
	'l’effondrement de l’Europe telle que nous la connaissons',
	'l’ambition de tout Français de privilégier les valeurs conformes aux principes constitutionnels',
	'le désir de solidarité, le désir que la famille humaine soit rassemblée',
	'l’idéal auquel nous tendons tous, la véritable égalité et l’absence totale de discrimination'
);
$a3 = array(
	'doit s’intégrer à la finalisation globale',
	'doit permettre un contrôle plus strict d’Internet',
	'oblige à la prise en compte encore plus effective',
	'nous devons mettre en place des mesures exceptionnelles',
	'nous devons mettre en place des lois d’exceptions',
	'interpelle le citoyen que je suis et nous oblige tous à aller de l’avant dans la voie',
	'doit nous amener au choix réellement impératif',
	'doit prendre en compte les préoccupations de la population de base dans l’élaboration',
	'entraîne une mission somme toute des plus exaltantes pour moi : l’élaboration',
	'nous fait prendre conscience d’une réalité, mais aussi d’une mission, celle dictée par notre conscience collective et inébranlable, celle',
	'nous oriente vers un chemin radieux, plein d’avenir : celui',
	'a pour condition sine qua non de combattre cette crise pour marcher vers un avenir radieux, défenseurs',
	'nous mets en face d’un terrible choix. Ou alors le refus de la réalité, ou la volonté de la changer en quelque chose de meilleur, grâce à l’élaboration',
	'nous ouvre les yeux, nous faisant comprendre le devoir qui est le notre, à nous citoyens, d’agir en avocat zélé',
	'nous suggère de suivre un idéal, concrétisé en un projet, une ambition, si simple et pourtant si exaltante',
	'nous mets en face d’un terrible choix. Ou alors le refus de la réalité, ou la volonté de la changer en quelque chose de meilleur, grâce à 
l’élaboration',
	'oblige tout citoyen à désirer l’avènement',
	'nous mène à la poursuite du bonheur, pour reprendre les mots de Jefferson, génie démocratique, mais surtout à la poursuite de l’abondance, à travers la création'
);
$a4 = array(
	'd’un processus allant vers plus d’égalité.',
	'd’une volonté générale de démocratisation.',
	'dans une volonté de lutter contre le risque d’un terrorisme mondial.',
	'd’un avenir s’orientant vers plus de progrès et plus de justice.',
	'd’une restructuration dans laquelle chacun pourra enfin retrouver sa dignité.',
	'd’une valorisation sans concession de nos caractères spécifiques.',
	'd’un plan correspondant véritablement aux exigences légitimes de chacun.',
	'de solutions rapides correspondant aux grands axes sociaux prioritaires.',
	'd’un programme plus humain, plus fraternel et plus juste.',
	'd’un projet porteur de véritables espoirs, notamment pour les plus démunis.',
	'du désir profond et irréversible de la nécessité de la justice sociale, qui est voulue par les glorieux principes de 1789.',
	'd’un projet de promotion des idéaux démocratiques, idéaux vivant depuis des siècles. C’est à nous de garder le flambeau de la République allumé, coûte que coûte !',
	'd’un projet de reconstruction et de consolidation de la nationalité européenne dans la population.',
	'd’une campagne qui défende les droits les plus élémentaires, comme ceux d’avoir une nation forte et en qui on peut avoir confiance.',
	'd’une Agriculture performante sachant subvenir aux besoins d’une population en croissance exponentielle.'
);
$a5 = array(
	'Vive la République',
	'Vive la Démocratie',
	'Vive la France',
	'Vive l’Europe',
	'Vive le peuple français',
	'Vive la Liberté',
	'Vive la Fraternité',
	'Vive l’Égalité',
	'Vive Kim Jong-un',
	'Vive le monde libre',
	'Vive la Révolution',
	'Vive la Paix',
	'Vive le pinard',
	'Nous sommes Charlie',
	'Vive la gauche caviar et la droite cassoulet',
	'Vive le socialisme'
	
);
function proba($nb) {
	global $a0;
	global $a1;
	global $a2;
	global $a3;
	global $a4;
	global $a5;
	$i = 1; 
	$t = 0;
	$s = array();
	while($i <= $nb) {
		$s[$i] = (count($a1) + count($a2) + count($a3) + count($a4)) * $i;
		$i++;
	}
	$f = count($a0) * count($a5)*array_product($s);
	return $f;
	// return number_format($f, 0, ',', ' ');
}

function r($a) {
		$c = count($a) -1;
		return $a[rand(0, $c)];
}
function langdebois($b) {
	global $a0;
	global $a1;
	global $a2;
	global $a3;
	global $a4;
	global $a5;
	 $i = 0;
	 while($i <= $b) {
		if($i == 0) {
			$t = '<p>';
			$t .= r($a0)."\n";
			$t .= r($a1).' ';
			$t .= r($a2).' ';
			$t .= r($a3).' ';
			$t .= r($a4).' '."\n";
			$t .= '</p>';
		}
		if($i == 1) {
			$t .= '<p>';
			$t .= 'C’est pourquoi ';
			$t .= strtolower(r($a1)).' ';
			$t .= r($a2).' ';
			$t .= r($a3).' ';
			$t .= r($a4).' '."\n";
			$t .= '</p>';
		}
		if($b>1 and $i >1) {
			$t .= '<p>';
			$t .= r($a1).' ';
			$t .= r($a2).' ';
			$t .= r($a3).' ';
			$t .= r($a4).' '."\n";
			$t .= '</p>';
		}
		if($i == $b) {
			$t .= '<p>';
			$t .= 'Finalement, ';
			$t .= strtolower(r($a1)).' ';
			$t .= r($a2).' ';
			$t .= r($a3).' ';
			$t .= r($a4).' '."\n";
			$t .= '</p>';
		}
		$i++;
	 }
	 $t .='<p>'.r($a5).' !</p>';
	 return $t;
}
function base64($path) {
	$type = pathinfo($path, PATHINFO_EXTENSION);
	$data = file_get_contents($path);
	return'data:image/' . $type . ';base64,' . base64_encode($data);
}
function compress_css($f) {
	$texte=file_get_contents($f);
	$texte= preg_replace("(\r\n|\n|\r)",'',$texte);
	$texte = trim($texte);
	$texte = str_replace("\t", "", $texte);
	echo $texte;

}
