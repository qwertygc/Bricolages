<?php
/* MASTOFEED : Afficher une timeline Mastodon */

date_default_timezone_set('Europe/Paris');
setlocale(LC_ALL, array('fr_FR.UTF-8','fr_FR@euro','fr_FR','french'));
ini_set('user_agent','Mozilla/4.0 (compatible; MSIE 6.0)');

function parse_toot_uri($uri) {
	$parse_url = parse_url($uri);
	$explode = explode('/', $parse_url['path']);
	$segment['instance'] = $parse_url['host'];
	$segment['user'] = $explode[1];
	$segment['id'] = $explode[2];
	return $segment;
	
}

function page_title($url) { # https://stackoverflow.com/questions/399332/fastest-way-to-retrieve-a-title-in-php
    $fp = file_get_contents($url);
    if (!$fp) 
        return null;

    $res = preg_match("/<title>(.*)<\/title>/siU", $fp, $title_matches);
    if (!$res) 
        return null; 

    // Clean up title: remove EOL's and excessive whitespace.
    $title = preg_replace('/\s+/', ' ', $title_matches[1]);
    $title = trim($title);
    return $title;
}    
function show_toot($uri) {
	$toot = parse_toot_uri($uri);
	$json = json_decode(file_get_contents('https://'.$toot['instance'].'/api/v1/statuses/'.$toot['id']), true);
	$html = '<blockquote>';
	$html .= '<img src="'.$json['account']['avatar'].'" alt="avatar" style="width:50px; border-radius:50px;float:left; margin-right:1em;"/>';
	$html .= '<span style="font-weight:bold">'.$json['account']['display_name'].'</span><br><span style="color:gray">'.$json['account']['username'].'@'.$toot['instance'].'</span>';
	$html .= $json['content'];
	$html .= '<a href="'.$json['url'].'">'.date('Y-m-d H:i:s', strtotime($json['created_at'])).'</a> ↩️ '.$json['replies_count'].' ♲ '.$json['reblogs_count'].' ⭐ '.$json['favourites_count'].'';
	$html .= '</blockquote>';
	return $html;
}

function show_feed($username, $tag=null) {
// username = uid@server.tld tag = mytag (without hashtag)
	$explode = explode('@', $username);
	$uid = $explode[0];
	$server = $explode[1];
	$html = null;
	$url = ($tag != null) ? 'https://'.$server.'/@'.$uid.'/tagged/'.$tag.'.rss' : 'https://'.$server.'/@'.$uid.'.rss';
	$rss = json_decode(json_encode(simplexml_load_string(file_get_contents($url))), true);
	foreach($rss['channel']['item'] as $toot) {
		$html .= show_toot($toot['guid']);
	}
	return $html;
}

function export_opml($username, $tag=null) {
	$opml = '<!DOCTYPE NETSCAPE-Bookmark-file-1>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<TITLE>Export Mastodon @'.$username.'/'.$tag.' ('.date('T-m-d H:i:s', time()).')</TITLE>
<H1>Export Mastodon @'.$username.'/'.$tag.' ('.date('T-m-d H:i:s', time()).')</H1>
<DL><p>';
	$explode = explode('@', $username);
	$uid = $explode[0];
	$server = $explode[1];
	$html = null;
	$url = ($tag != null) ? 'https://'.$server.'/@'.$uid.'/tagged/'.$tag.'.rss' : 'https://'.$server.'/@'.$uid.'.rss';
	$rss = json_decode(json_encode(simplexml_load_string(file_get_contents($url))), true);
	foreach($rss['channel']['item'] as $pouet) {
		$toot = parse_toot_uri($pouet['guid']);
		$json = json_decode(file_get_contents('https://'.$toot['instance'].'/api/v1/statuses/'.$toot['id']), true);
		foreach($json['tags'] as $tags) {
			$tag .= $tags['name'].',';
		}
		$opml .= '<DT><A HREF="'.$json['card']['url'].'" ADD_DATE="'.strtotime($json['created_at']).'" LAST_MODIFIED="'.time().'" PRIVATE="0" TAGS="'.$tag.'">'.$json['card']['title'].'</A>'.PHP_EOL.'<DD>'.strip_tags($json['content']).PHP_EOL;
		$tag = null;
	}
	$opml .= '</DL><p>';
	return $opml;
}
/*
if(!file_exists('cache.html') || time() - filemtime('cache.html') >= 3600) {
	$cache .= show_feed('qwerty@framapiaf.org', 'écologie');
	$cache .= show_feed('qwerty@framapiaf.org');
	file_put_contents('cache.html', $cache);
}
else {
	echo file_get_contents('cache.html');
}
*/

function ActivityPub2Markdown($file, $hashtag) {
	$json = json_decode(file_get_contents($file), true);
	$return = '';
	$list_toots = array();
	foreach($json['orderedItems'] as $toots) {
		if($toots['type'] == 'Create') {
			$toot = strip_tags(html_entity_decode($toots['object']['content']));
			if($hashtag != '' AND str_contains($toot, $hashtag)) {
				$list_toots[] = array('content' => strip_tags(html_entity_decode($toots['object']['content'])), 'date'=> $toots['object']['published'], 'url' => $toots['object']['url']);
			}
		}
	}
	$list_toots_reverse = array_reverse($list_toots);
	foreach($list_toots_reverse as $content) {
		$return .= '> '.$content['content'].PHP_EOL.'> ['.$content['date'].']('.$content['url'].')'.PHP_EOL.PHP_EOL.'==='.PHP_EOL.PHP_EOL;
	}
	return $return;
}

function ActivityPub2OPML($file, $hashtag) {
	$json = json_decode(file_get_contents($file), true);
	$return = '<!DOCTYPE NETSCAPE-Bookmark-file-1>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<TITLE>Export Mastodon ('.date('T-m-d H:i:s', time()).')</TITLE>
<H1>Export Mastodon ('.date('T-m-d H:i:s', time()).')</H1>
<DL><p>';
	$list_toots = array();
	foreach($json['orderedItems'] as $toots) {
		if($toots['type'] == 'Create') {
			$toot = strip_tags(html_entity_decode($toots['object']['content']));
			if($hashtag != '' AND str_contains($toot, $hashtag)) {
				$list_toots[] = array('content' => strip_tags(html_entity_decode($toots['object']['content'])), 'date'=> $toots['object']['published'], 'url' => $toots['object']['url']);
			}
		}
	}
	$list_toots_reverse = array_reverse($list_toots);
	foreach($list_toots_reverse as $content) {
		preg_match_all('/https?\:\/\/[^\" ]+/i', html_entity_decode(strip_tags($content['content'])), $match);
		if($match[0][0] != '') {
			$text = str_replace($match[0][0], '', strip_tags($content['content']));
			$text = str_replace($hashtag, '', $text);
			$hashtag_without_croizillon = str_replace('#', '', $hashtag);
			$return .= '<DT><A HREF="'.$match[0][0].'" ADD_DATE="'.strtotime($content['date']).'" LAST_MODIFIED="'.time().'" PRIVATE="0" TAGS="'.$hashtag_without_croizillon.'">'.page_title($match[0][0]).'</A>'.PHP_EOL.'<DD>'.$text.PHP_EOL;
			//$return .= '<DT><A HREF="'.$match[0][0].'" ADD_DATE="'.strtotime($content['date']).'" LAST_MODIFIED="'.time().'" PRIVATE="0" TAGS=""></A>'.PHP_EOL.'<DD>'.$text.PHP_EOL;
		}
	}
	$return .= '</DL><p>';
	return $return;
}



