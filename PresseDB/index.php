<?php
session_start();
error_reporting(-1);
include 'app/functions.php';


if(isset($_GET['project']) AND $_GET['project'] != '') {
	$_SESSION['project'] = $_GET['project'];
	db_project($_SESSION['project']);
}
else {
	db_project('');
}
?>
<!doctype html>
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <style>
@media screen {
    main {
        max-width:800px;
        margin:auto;
    }
}
@media print {
	 * {
		text-shadow: none !important;
		color: #000 !important;
		background: transparent !important;
		box-shadow: none !important;
	}
	body {
		width: auto!important;
		margin: auto!important;
        margin: 2cm;
		font-family: serif;
		font-size: 12pt;
		background-color: #fff!important;
		color: #000!important;
	}
    h1 {
        font-size:14pt;
    }
    main {
       text-align:justify;
        font-size: 12pt;
    }
}
    </style>
</head>
<body class="container-fluid">
<nav class="navbar navbar-expand-lg navbar-light bg-light d-print-none">
    <ul class="navbar-nav">
    <a class="navbar-brand" href="#">Projet « <?php echo $_SESSION['project']; ?> »</a>
      <li class="nav-item">
        <a class="nav-link" href="index.php">Base de données</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?action=view">Voir</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?action=add">Ajouter un article</a>
      </li>
    </ul>
</nav>
<main>
<?php
if(isset($_GET['action'])) {
    switch($_GET['action']) {
        case 'view':
			include 'app/view-tpl.php';
      		include 'app/view-fct.php';
        break;
        case 'add':
			include 'app/add-fct.php';
        	include 'app/add-tpl.php';
        break;
        case 'new':
        	include 'app/add-fct.php';
            include 'app/add-tpl.php';
        break;
    }
}
else {
	?>
	<form method="post" action="index.php" class="d-print-none">
	<label>Ajout d'un nouveau projet</label> <input type="text" name="project" id="project" class="form-control"/>
	<input type="submit" name="submit" value="Envoyer" class="btn btn-primary mb-3"/>
</form>

<?php
	if(isset($_POST['project'])) {header('Location: index.php?project='.toslug($_POST['project'])); }
	echo '<h1>Choix du projet</h1>';
	echo '<ul class="list-group">';
	foreach(array_diff(scandir('data'), array('..', '.', 'data.db')) as $project) {
		  echo '<li class="list-group-item"><a href="index.php?project='.$project.'">'.$project.'</a></li>';
	}
	echo '</ul>';

}
?>
</main>
</body>
</html>
