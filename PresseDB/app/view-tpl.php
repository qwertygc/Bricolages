<?php
    $date_begin = (isset($_POST['date_begin'])) ? $_POST['date_begin'] : '1900-01-01';
    $date_end = (isset($_POST['date_end'])) ? $_POST['date_end'] : date('Y-m-d', time());
?>
<form method="post" class="d-print-none">
<label>Date de début</label><input type="date" name="date_begin" value="<?=$date_begin;?>" class="form-control" />
<label>Date de fin</label><input type="date" name="date_end" value="<?=$date_end;?>" class="form-control" />
<label>Journaux</label>       
<select class="form-select" multiple aria-label="multiple select" name="journal[]">
  <?php
        $journal = (isset($_POST['journal'])) ? $_POST['journal'] : array();
        $query = $db->query('SELECT DISTINCT journal FROM presse ORDER BY journal'); while($data = $query->fetch()):
       $selected = (in_array($data['journal'], $journal)) ? 'selected' : '';
  ?>
  <option value="<?=$data['journal'];?>" <?=$selected;?>><?=$data['journal'];?></option>
  <?php endwhile; ?>
</select>
<label>Catégories</label>
<select class="form-select" multiple aria-label="multiple select" name="category[]">
 <?php
    $category = (isset($_POST['category'])) ? $_POST['category'] : array();
    $query = $db->query('SELECT DISTINCT category FROM presse ORDER BY category'); while($data = $query->fetch()):
    $selected = (in_array($data['category'], $category)) ? 'selected' : '';
  ?>
  <option value="<?=$data['category'];?>" <?=$selected;?>><?=$data['category'];?></option>
  <?php endwhile; ?>
</select>
<label>Mot-clé</label> <input type="search" name="search" class="form-control"/>
<input type="submit" name="submit_html" value="Visualiser" class="btn btn-primary mb-3"/>
<input type="submit" name="submit_stats" value="Statistiques" class="btn btn-primary mb-3"/>
<input type="submit" name="submit_csv" value="Exporter en CSV et Iramuteq" class="btn btn-primary mb-3"/>
</form>
