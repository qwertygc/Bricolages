<form method="post" action="?action=add" class="d-print-none">
	<label>Ajout rapide</label> <input type="url" name="quick" class="form-control"/>
	<input type="submit" name="submit" value="Envoyer" class="btn btn-primary mb-3"/>
</form>
<form method="post" action="?action=add" class="d-print-none">
	<label>Titre</label> <input type="text" name="title" value="<?=$title;?>" class="form-control"/>
	<label>Contenu</label><textarea class="form-control" name="content" rows="5"><?=$content;?></textarea>
	<label>Date de publication</label><input type="date" value="<?=$date;?>" name="date" value="" class="form-control" />
	<label>Source</label> <input type="text" name="source" value="<?=$source;?>" class="form-control"/>
	<label>Journal</label>       
	<input type="text" name="journal" list="journal" value="<?=$journal;?>" class="form-control"/>
	<datalist id="journal">
	  <?php $query = $db->query('SELECT DISTINCT journal FROM presse ORDER BY journal'); while($data = $query->fetch()): ?>
	  <option value="<?=$data['journal'];?>" />
	  <?php endwhile; ?>
	</datalist>
	<label>Catégorie</label>
	<input type="text" name="category" list="category" value="<?=$category;?>" class="form-control"/>
	<datalist id="category">
	 <?php $query = $db->query('SELECT DISTINCT category FROM presse ORDER BY category'); while($data = $query->fetch()): ?>
	  <option value="<?=$data['category'];?>" />
	  <?php endwhile; ?>
	</datalist>
	<input type="hidden" name="id" id="id" value="<?=$id;?>"/>
	<input type="submit" name="submit" value="Envoyer" class="btn btn-primary mb-3"/>
</form>
