<?php
/*
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004
 
Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 
Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.
 
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 
 0. You just DO WHAT THE FUCK YOU WANT TO.
*/
/*
Peck it's a kiss, minimalist and lightweight blog software without comments.
For use Peck, upload (with a FTP software) in the directory data/ the file in html. One file = one article.
*/
##
/* functions */
function version() {
	return '&#945; 0.01';
}
function foldersize($path) {
	$total_size = 0;
	$files = scandir($path);
	$cleanPath = rtrim($path, '/'). '/';
    foreach($files as $t) {
		if($t<>"." && $t<>"..") {
			$currentFile = $cleanPath . $t;
			if (is_dir($currentFile)) {
				$size = foldersize($currentFile);
				$total_size += $size;
			}
			else {
				$size = filesize($currentFile);
				$total_size += $size;
			}
		}   
	}
	return $total_size;
}


function scan_dir($dir) { # source = http://stackoverflow.com/questions/11923235/scandir-to-sort-by-date-modified
    $ignored = array('.', '..', '.svn', '.htaccess');

    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);

    return ($files) ? $files : false;
}
function beginhtml() { #generate the beginning html code
	echo "<!doctype html>
<html>
\t<head>
\t\t<title>My Website</title>
\t\t<link rel=\"alternate\" type=\"application/rss+xml\" title=\"\" href=\"?rss\" />
\t</head>
\t<body>
";
}
function endhtml() { #generate the last html code
	echo "
\t</body>
</html>";
}
function viewarticle($id) { #Show the article
	beginhtml();
	if (file_exists('data/'.$id.'.html')) {
		readfile('data/'.$id.'.html');
		echo '<h1>'.format_title($id).'</h1>';
		echo date("F d Y H:i:s.", filemtime('data/'.$id));
		echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	}
	else {
		header("HTTP/1.0 404 Not Found");
		echo 'HTTP/1.0 404 Not Found';
	}
	endhtml();
}
function format_title($str) { #format title _ is space
	return strtr($str,'_',' ');
}
function is_dir_empty($dir) { # src = http://stackoverflow.com/questions/7497733/how-can-use-php-to-check-if-a-directory-is-empty
  if (!is_readable($dir)) return NULL; 
  $handle = opendir($dir);
  while (false !== ($entry = readdir($handle))) {
    if ($entry != "." && $entry != "..") {
      return FALSE;
    }
  }
  return TRUE;
}
function get_file_ext($file){ #source : http://www.astuces-webmaster.ch/page/recuperer-extension-fichier-php
	$ext = pathinfo($file, PATHINFO_EXTENSION);
	return $ext;
}
function get_pagination($page,$total) { #https://github.com/circa75/dropplets/blob/master/dropplets/functions.php

    $string = '';
    $string .= "<ul style=\"list-style:none;\">";

    for ($i = 1; $i<=$total;$i++) {
        if ($i == $page) {
            $string .= "<li style='display: inline-block;'><a href='#'>".$i."</a></li> ";
        } else {
            $string .= "<li style='display: inline-block;'><a href=\"?page=".$i."\">".$i."</a></li> ";
        }
    }
    
    $string .= "</ul>";
    return $string;
}


function viewlist($pagination) { #show the list of article
	if(file_exists('cache/index-'.$pagination.'.html') && filemtime('cache/index-'.$pagination.'.html') >time()-3600) {readfile('cache/index-'.$pagination.'.html');}
	else {
		ob_start();
		beginhtml();
		if(!is_dir_empty('data/')) {
			$nb = 0;
			$pagination = (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 1) ? $_GET['page'] : 1;
			$offset = ($pagination == 1) ? 0 : ($pagination - 1) * PAGINATION;
			$articles = array_slice(scan_dir('data/'),$offset,(PAGINATION > 0) ? PAGINATION : null);
			echo "\t\t<ul>".PHP_EOL;
			foreach($articles as $article) {
				$array=explode('.html',$article);
				if($array[0] !='.' AND $array[0] !='..' AND get_file_ext($article) == 'html') {
					echo "\t\t\t<li>".date("F d Y H:i:s.", filemtime('data/'.$article))."<a href=\"?id=$array[0]\">".format_title($array[0])."</a>";
					readfile('data/'.$array[0].'.html');
					echo "</li>".PHP_EOL;
					$nb +=1;
				}
			}
			echo "\t\t</ul>";
			$all_posts = scan_dir('data/');
			echo get_pagination($pagination,round(count($all_posts)/PAGINATION));
			echo count($all_posts).' article(s)';
		}
		else {
			echo "No article has been written yet.";
		}
		endhtml();
	}
	$page = ob_get_contents();
	ob_end_clean();
	file_put_contents('cache/index-'.$pagination.'.html', $page);
	echo $page;
}

function viewrss() {
	if(file_exists('cache/rss.xml') && filemtime('cache/rss.xml') >time()-3600) {readfile('cache/rss.xml');}
	else {
		echo '<?xml version="1.0" encoding="utf-8"?>'. PHP_EOL;
		echo '<rss version="2.0">'. PHP_EOL;
		echo '<channel>'.PHP_EOL;
		echo '<title>My Website</title>'. PHP_EOL;
		//echo '<description><![CDATA['.DESCRIPTION. ']]></description>'. "\n"; Facultatif
		echo '<description>Yes, it\'s Beautiful, no ?</description>'. PHP_EOL;
		echo '<link>http://example.org/</link>'.PHP_EOL;
			$articles= scan_dir('data/');
			foreach($articles as $article) {		
				$array=explode('.html',$article);
				if($array[0] !='.' AND $array[0] !='..') {
					echo "\t".'<item>'. "\n";
					echo "\t\t".'<title>'.$array[0]. '</title>'. PHP_EOL; 
					echo "\t\t".'<link>http://example.org/?id='.$array[0].'</link>'. PHP_EOL;
					echo "\t\t".'<description><![CDATA[';
					readfile('data/'.$article);
					echo ']]></description>'.PHP_EOL;
					echo "\t\t".'<pubDate>'.date(DATE_RSS, filemtime('data/'.$array[0].'.html')).'</pubDate>'.PHP_EOL;
					echo "\t\t".'</item>'."\n";	
			}
		}
		echo '</channel>'. PHP_EOL;
		echo '</rss>';
		$page = ob_get_contents();
		ob_end_clean();
		file_put_contents('cache/rss.xml', $page);
		echo $page;
	}
}
function bytes($size) { 
	switch(true) {
		case($size/1024 >= 0 AND $size/1024 <= 1):
			return ceil($size).' bytes';
		break;
		case($size/1024/1024 >= 0 AND $size/1024/1024 <= 1):
			return ceil($size/1024) .' Kib';
		break;
		case($size/1024/1024/1024 >= 0 AND $size/1024/1024/1024 <= 1):
			return ceil($size/1024/1024) .' Mib';
		break;
		case($size/1024/1024/1024/1024 >= 0 AND $size/1024/1024/1024/1024 <= 1):
			return ceil($size/1024/1024/1024) .' Gib';
		break;
	}
}
function debug($statut) {
	global $begin;
	if($statut == 'begin') {
		$begin = microtime(TRUE);
	}
	if($statut == 'end') {
		$end = microtime(TRUE);
		$return = '<p>'.round(($end - $begin),6).'seconds| Memory:'.bytes(memory_get_usage()).' | Peak of memory:'.bytes(memory_get_peak_usage()).' | Folder size:'.bytes(foldersize('data')).' | '.version().'</p>';
		echo $return;
	}	
}
function clear() {
	$files = glob('cache/*'); // get all file names
	foreach($files as $file){ // iterate files
		if(is_file($file))
			unlink($file); // delete file
	}
	header('Location: index.php');
}

function generate() {
	$i = 1;
	while ($i <= 100) {
		file_put_contents('data/'.uniqid().'.html', '<h1>'.uniqid().'</h1>');
		$i++;  
	}
	header('Location: index.php?clear');
	// echo '<script>javascript:location.reload(true);</script>';
}
/* start program */
error_reporting(E_ALL);
define('PAGINATION', 10);
debug('begin');
if(!is_dir('data')) { mkdir('data');}
if(!is_dir('cache')) { mkdir('cache');}
if(isset($_GET['rss'])) {
	viewrss();
}
elseif(isset($_GET['clear'])) {
	clear();
}
elseif(isset($_GET['generate'])) {
	generate();
}
else {
	$page = (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 1) ? $_GET['page'] : 1;
	(isset($_GET['id'])) ? viewarticle($_GET['id']) : viewlist($page);
}
debug('end');
/* and for you, a little poem of Victor Hugo
L'aurore s'allume

I

L'aurore s'allume ;
L'ombre épaisse fuit ;
Le rêve et la brume
Vont où va la nuit ;
Paupières et roses
S'ouvrent demi closes ;
Du réveil des choses
On entend le bruit.

Tout chante et murmure,
Tout parle à la fois,
Fumée et verdure,
Les nids et les toits ;
Le vent parle aux chênes,
L'eau parle aux fontaines ;
Toutes les haleines
Deviennent des voix !

Tout reprend son âme,
L'enfant son hochet,
Le foyer sa flamme,
Le luth son archet ;
Folie ou démence,
Dans le monde immense,
Chacun recommence
Ce qu'il ébauchait.

Qu'on pense ou qu'on aime,
Sans cesse agité,
Vers un but suprême,
Tout vole emporté ;
L'esquif cherche un môle,
L'abeille un vieux saule,
La boussole un pôle,
Moi la vérité !

II

Vérité profonde !
Granit éprouvé
Qu'au fond de toute onde
Mon ancre a trouvé !
De ce monde sombre,
Où passent dans l'ombre
Des songes sans nombre,
Plafond et pavé !

Vérité, beau fleuve
Que rien ne tarit !
Source où tout s'abreuve,
Tige où tout fleurit !
Lampe que Dieu pose
Près de toute cause !
Clarté que la chose
Envoie à l'esprit !

Arbre à rude écorce,
Chêne au vaste front,
Que selon sa force
L'homme ploie ou rompt,
D'où l'ombre s'épanche ;
Où chacun se penche,
L'un sur une branche,
L'autre sur le tronc !

Mont d'où tout ruisselle !
Gouffre où tout s'en va !
Sublime étincelle
Que fait Jéhova !
Rayon qu'on blasphème !
Oeil calme et suprême
Qu'au front de Dieu même
L'homme un jour creva !

III

Ô Terre ! ô merveilles
Dont l'éclat joyeux
Emplit nos oreilles,
Éblouit nos yeux !
Bords où meurt la vague,
Bois qu'un souffle élague,
De l'horizon vague
Plis mystérieux !

Azur dont se voile
L'eau du gouffre amer,
Quand, laissant ma voile
Fuir au gré de l'air,
Penché sur la lame,
J'écoute avec l'âme
Cet épithalame
Que chante la mer !

Azur non moins tendre
Du ciel qui sourit
Quand, tâchant d'entendre
Je cherche, ô nature,
Ce que dit l'esprit,
La parole obscure
Que le vent murmure,
Que l'étoile écrit !

Création pure !
Etre universel !
Océan, ceinture
De tout sous le ciel !
Astres que fait naître
Le souffle du maître,
Fleurs où Dieu peut-être
Cueille quelque miel !

Ô champs ! ô feuillages !
Monde fraternel !
Clocher des villages
Humble et solennel !
Mont qui porte l'air !
Aube fraîche et claire,
Sourire éphémère
De l'astre éternel !

N'êtes-vous qu'un livre,
Sans fin ni milieu,
Où chacun pour vivre
Cherche à lire un peu !
Phrase si profonde
Qu'en vain on la sonde !
L'oeil y voit un monde,
L'âme y trouve un Dieu !

Beau livre qu'achèvent
Les coeurs ingénus ;
Où les penseurs rêvent
Des sens inconnus ;
Où ceux que Dieu charge
D'un front vaste et large
Ecrivent en marge :
Nous sommes venus !

Saint livre où la voile
Qui flotte en tous lieux,
Saint livre où l'étoile
Qui rayonne aux yeux,
Ne trace, ô mystère !
Qu'un nom solitaire,
Qu'un nom sur la terre,
Qu'un nom dans les cieux !

Livre salutaire
Où le cour s'emplit !
Où tout sage austère
Travaille et pâlit !
Dont le sens rebelle
Parfois se révèle !
Pythagore épelle
Et Moïse lit !

Victor Hugo
Les chants du crépuscule
*/