	var fileInput = document.getElementById('file');
				fileInput.onchange = function() {
				var reader = new FileReader();
					reader.onload = function() {
						document.getElementById('filename').value = fileInput.files[0].name;
						document.getElementById('textarea').value = reader.result;
					};
					reader.readAsText(fileInput.files[0]);
				};
	// Affiche les notifications
	function notif(color, message) {
		document.getElementById('notif').innerHTML = message;
		document.getElementById('notif').style.backgroundColor = color;
		setTimeout(function() { document.getElementById('notif').innerHTML = ''; document.getElementById('notif').style.backgroundColor = "transparent";}, 7000);
	}
	// Gère tout le textarea
	var textarea = document.getElementById('textarea');
   textarea.onkeyup = function() {
       var chaine = document.getElementById('textarea').value; 
		chaine = chaine.replace(/--/, "—");
		chaine = chaine.replace(/<</, "« ");
		chaine = chaine.replace(/>>/, " »");
		chaine = chaine.replace(/ééé/, "É");
		chaine = chaine.replace(/èè/, "È");
		chaine = chaine.replace(/çç/, "Ç");
		chaine = chaine.replace(/àà/, "À");
		chaine = chaine.replace(/'/, "’");
		chaine = chaine.replace(/\.\.\./, "…");
		nb = chaine.split(/\b\w+\b/).length-1; 
		document.getElementById('textarea').value = chaine;
        document.getElementById("result").innerHTML = nb+' words ';
		document.getElementById("read").innerHTML =Math.floor(nb/200)+'mn ';
		document.getElementById("preview").innerHTML = Markdown(chaine);
		if(nb == "10") {notif('#2ECC71','Bravo, tu a atteint les 10 mots !');}
		if(nb == "100") {notif('#2ECC71', 'Bravo, tu a atteint les 100 mots !');}
		if(nb == "250") {notif('#2ECC71', 'Bravo, tu a atteint les 250 mots !');}
		if(nb == "400") {notif('#2ECC71', 'Bravo, tu a atteint les 400 mots !');}
		if(nb == "500") {notif('#2ECC71', 'Bravo, tu a atteint les 500 mots !');}
		if(nb == "750") {notif('#9B59B6', '<b>Trophé 3Pages.fr</b> : Bravo, tu a atteint les 750 mots !');}
		if(nb == "1000") {notif('#2ECC71', 'Bravo, tu a atteint les 1000 mots !');}
		if(nb == "1667") {notif('#9B59B6', '<b>Un jour NaNoWriMo</b> : Bravo, tu a atteint les 1667 mots !');}
		if(nb == "50000") {notif('#9B59B6', '<b>Trophé NaNoWriMo</b> : Bravo, tu a atteint les 50 000 mots !');}
	}
	// Gère le plein écran
	function toggleFullScreen() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    // alternative standard method
      (!document.mozFullScreen && !document.webkitIsFullScreen)) {               // current working methods
    if (document.documentElement.requestFullScreen) {
      document.documentElement.requestFullScreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullScreen) {
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}
//Télécharge le contenu du textarea
function doDL(s){ //http://stackoverflow.com/a/17486753
    function dataUrl(data) {return "data:x-application/text," + escape(data);}
    window.open(dataUrl(s));
}
//sauvegarde avec localstorage le contenu du textarea
var n = document.getElementsByTagName('textarea')[0];
n.onchange = function(){localStorage.setItem("n",n.value);}
n.value = localStorage.getItem("n");

var f = document.getElementsByTagName('filename')[0];
f.onchange = function(){localStorage.setItem("f",f.value);}
f.value = localStorage.getItem("f");

// affiche cache le truc
function toggle(object_id){
 var obj=document.getElementById(object_id)	
 if(obj.style.display == 'block')
  obj.style.display='none'
 else
  obj.style.display='block'
 }
 function getXMLHttpRequest() {
	var xhr = null;
	
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest();
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}
	
	return xhr;
}
var fileInput = document.getElementById('file');
fileInput.onchange = function() {
var reader = new FileReader();
	reader.onload = function() {
		document.getElementById('filename').value = fileInput.files[0].name;
		document.getElementById('textarea').value = reader.result;
		document.getElementById('filename').reload();
		document.getElementById('textarea').reload();
	};
	reader.readAsText(fileInput.files[0]);
};
function insertTag(startTag, endTag, textareaId, tagType) {
        var field  = document.getElementById(textareaId); 
        var scroll = field.scrollTop;
        field.focus();
        
        /* === Partie 1 : on récupère la sélection === */
        if (window.ActiveXObject) {
                var textRange = document.selection.createRange();            
                var currentSelection = textRange.text;
        } else {
                var startSelection   = field.value.substring(0, field.selectionStart);
                var currentSelection = field.value.substring(field.selectionStart, field.selectionEnd);
                var endSelection     = field.value.substring(field.selectionEnd);               
        }
        
        if (tagType) {
                switch (tagType) {
                        case "link":
							if (currentSelection) { // Il y a une sélection
									if (currentSelection.indexOf("http://") == 0 || currentSelection.indexOf("https://") == 0 || currentSelection.indexOf("ftp://") == 0 || currentSelection.indexOf("www.") == 0) {
											// La sélection semble être un lien. On demande alors le libellé
											var label = prompt("Quel est le libellé du lien ?") || "";
											startTag = "";									
											endTag = "";
											currentSelection = "["+label+"]("+ currentSelection +")";

											} else {
											// La sélection n'est pas un lien, donc c'est le libelle. On demande alors l'URL
											var URL = prompt("Quelle est l'url ?");
											startTag = "["+currentSelection+"](" + URL + ")";
											endTag = "";
											currentSelection = "";
									}
							} else { // Pas de sélection, donc on demande l'URL et le libelle
									var URL = prompt("Quelle est l'url ?") || "";
									var label = prompt("Quel est le libellé du lien ?") || "";
									currentSelection = "["+label+"](" + URL + ")";
									startTag = "";									
									endTag = "";
							}
                        break;
						case "picture":
							if (currentSelection) { // Il y a une sélection
									if (currentSelection.indexOf("http://") == 0 || currentSelection.indexOf("https://") == 0 || currentSelection.indexOf("ftp://") == 0 || currentSelection.indexOf("www.") == 0) {
											// La sélection semble être un lien. On demande alors le libellé
											var label = prompt("Quel est le libellé du lien ?") || "";
											startTag = "";									
											endTag = "";
											currentSelection = "!["+label+"]("+ currentSelection +")";

											} else {
											// La sélection n'est pas un lien, donc c'est le libelle. On demande alors l'URL
											var URL = prompt("Quelle est l'url ?");
											startTag = "!["+currentSelection+"](" + URL + ")";
											endTag = "";
											currentSelection = "";
									}
							} else { // Pas de sélection, donc on demande l'URL et le libelle
									var URL = prompt("Quelle est l'url ?") || "";
									var label = prompt("Quel est le libellé du lien ?") || "";
									currentSelection = "!["+label+"](" + URL + ")";
									startTag = "";									
									endTag = "";
							}
                        break;
                }
        }
        
        if (window.ActiveXObject) {
                textRange.text = startTag + currentSelection + endTag;
                textRange.moveStart("character", -endTag.length - currentSelection.length);
                textRange.moveEnd("character", -endTag.length);
                textRange.select();     
        } else {
                field.value = startSelection + startTag + currentSelection + endTag + endSelection;
                field.focus();
                field.setSelectionRange(startSelection.length + startTag.length, startSelection.length + startTag.length + currentSelection.length);
        } 

        field.scrollTop = scroll;     
}