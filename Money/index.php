<?php
include 'app/inc/system.php';
include 'app/inc/i18n/'.LANG.'.php';
session_start();
error_reporting(-1);
include 'app/tpl/header.php';
if(isset($_COOKIE['auth']) && !isset($_SESSION['user_id'])) {
	$auth = $_COOKIE['auth'];
	$auth = explode('---', $auth);
	$retour = $bdd->prepare("SELECT * FROM user WHERE id =?");
	$retour->execute(array($auth[0]));
	$data = $retour->fetch();
	$key = sha1($data['id'].$data['keyid'].$data['email'].$data['password']);
	if ($key == $auth[0]) {
		$_SESSION['login'] = $data['email'];
		$_SESSION['user_id'] = $tata['id'];
		$_SESSION['token'] = md5(uniqid(rand(), true));
	}
	else {
		setcookie('auth', '', time() + 365*24*3600, null, null, false, true);
	}
}
if(isset($_GET['action'])){
	switch ($_GET['action']){
		case 'signup': #inscription
			if(!isset($_SESSION['login'])) {
				echo '<h1>'.translate('signup').'</h1>';
				$form = New form();
				$form->startform(array('method'=>'post', 'action'=>'?action=signup'));
				$form->label('email', translate('email'));
				$form->input(array('type'=>'email', 'name'=>'email', 'required'=>'required', 'placeholder'=>'john.doe@example.org'));
				$form->label('password', translate('password'));
				$form->input(array('type'=>'password', 'name'=>'password', 'required'=>'required', 'placeholder'=>'d0_42D!'));
				$form->input(array('type'=>'submit'));
				$form->endform();
				if(isset($_POST['email']) && isset($_POST['password'])) {
					$retour = $bdd->prepare('SELECT email FROM user WHERE email = :email');
					$retour->execute(array('email'=> $_POST['email']));
					$member_exist=$retour->fetch();
					if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
						echo translate('mail_not_valid');
					}
					elseif($member_exist > 0) {
						echo translate('account_exist');
					}
					else {
						$mail = explode("@", $_POST['email']);
						sql('INSERT INTO user(email, password, pseudo, wallet, keyid) VALUES(?, ?, ?, 5, ?)', array($_POST['email'],hash('sha512', $_POST['password']),  $mail[0], uniqid()));
						sql('UPDATE user SET wallet = wallet + 50 WHERE id=0');
						echo translate('reccorded');
						send_mail($_POST['email'], 'no-reply@example.org', translate('mail_send_pwd_subject'), sprintf(translate('mail_send_pwd_body'), $_POST['password']));
					}
				}
			}
			else {
				redirect('index.php');
			}
		break;
		case 'signin': # connexion
			if(!isset($_SESSION['login'])) {
				echo '<h1>'.translate('signin').'</h1>';
				$form = New form();
				$form->startform(array('method'=>'post', 'action'=>'?action=signin'));
				$form->label('email',  translate('email'));
				$form->input(array('type'=>'email', 'name'=>'email', 'required'=>'required', 'placeholder'=>'john.doe@example.org'));
				$form->label('password',  translate('password'));
				$form->input(array('type'=>'password', 'name'=>'password', 'required'=>'required', 'placeholder'=>'d0_42D!'));
				$form->checkbox(array('name'=>'remember', 'id'=>'remember', 'type'=>'checkbox', 'style'=>'display:inline'), false);
				$form->label('remember',  translate('remember'));
				$form->input(array('type'=>'submit'));
				$form->endform();
				
				if(isset($_POST['email']) && isset($_POST['password'])) {
					$retour = $bdd->prepare("SELECT * FROM user WHERE email =? AND password=?");
					$retour->execute(array($_POST['email'], hash('sha512', $_POST['password'])));
					$data = $retour->fetch();
					if ($data == true) {
						$_SESSION['login'] = $data['email'];
						$_SESSION['user_id'] = $data['id'];
						$_SESSION['token'] = md5(uniqid(rand(), true));
						if(isset($_POST['remember'])) {
							setcookie('auth', $data['id'].'---'.sha1($data['id'].$data['keyid'].$data['email'].$data['password']), time() + 365*24*3600, null, null, false, true);
						}
						redirect('index.php');
					}
				}
			}
			else {
				redirect('index.php');
			}
	break;
	case 'logout':
		if(isset($_SESSION['login'])) {
			$_SESSION = array();
			session_destroy();
			redirect('index.php'); 
		}
		else {
			redirect('index.php'); 
		}
	break;
	case 'profile':
		if(isset($_SESSION['login'])) {
			$retour = $bdd->prepare('SELECT * FROM user WHERE id=?');
			$retour->execute(array($_SESSION['user_id']));
			$data = $retour->fetch();
			$form = New form();
			$form->startform(array('method'=>'post', 'action'=>'?action=profile'));
			$form->label('email',  translate('email'));
			$form->input(array('type'=>'email', 'name'=>'email', 'disabled'=>'disabled', 'value'=>$_SESSION['login']));
			$form->input(array('type'=>'text', 'disabled'=>'disabled', 'value'=>$data['keyid']));
			$form->label('password',  translate('password'));
			$form->input(array('type'=>'password', 'name'=>'password'));
			$form->label('password',  translate('pseudo'));
			$form->input(array('type'=>'text', 'name'=>'pseudo', 'value'=>$data['pseudo']));
			$form->textarea(array('readonly'=>'readonly'), '<iframe src="http://lab.etudiant-libre.fr.nf/money/don.php?id='.$data['id'].'" width="75" height="35" scrolling="no" frameborder="0" marginheight="0"></iframe>');
			$form->input(array('type'=>'submit'));
			$form->endform();
			if(isset($_POST['password']) OR isset($_POST['pseudo'])) {
				$password = (isset($_GET['password'])) ? hash('sha512', $_GET['password']) : $data['password'];
				sql('UPDATE user SET password=:password AND pseudo=:pseudo WHERE id=:id', array('password'=>$password, 'pseudo'=>$_POST['pseudo'], 'id'=>$_SESSION['user_id']));
				redirect('?action=profile');
			}
		}
	break;
	case 'transaction':
		if(isset($_SESSION['login'])) {
			if(isset($_GET['subaction'])) {
				if($_GET['subaction'] == 'send') {
					$retour = $bdd->prepare('SELECT pseudo, id, wallet FROM user');
					$retour->execute();
					$user = array();
					while($data = $retour->fetch()) {
						if($data['id'] != $_SESSION['user_id']) {
						$user[$data['id']] = $data['pseudo'];
						}
					}
					$retour = $bdd->prepare('SELECT wallet FROM user WHERE id=?');
					$retour->execute(array($_SESSION['user_id']));
					$data = $retour->fetch();
					if($data['wallet'] < 0) {echo translate('overdraft');}
					$form = New form();
					$form->startform(array('method'=>'post', 'action'=>'?action=transaction&subaction=send'));
					$form->label('to',  translate('to'));
					$form->select('to', $user, '', true);
					$form->label('number',  translate('value'));
					$form->input(array('type'=>'number', 'name'=>'value'));
					$form->label('text',  translate('text'));
					$form->textarea(array('name'=>'text', 'placeholder'=>'Lorem'), '');
					$form->input(array('type'=>'submit'));
					$form->endform();
					if(isset($_POST['to']) && isset($_POST['value']) && isset($_POST['text'])) {
						if(($data['wallet']-$_POST['value']) > 0) {
							$valid = ($_POST['to'] == 0 AND $_POST['value'] > 0) ? true : false;
							transaction($_SESSION['user_id'], $_POST['to'], $_POST['value'], $valid, strip_tags($_POST['text']));
							redirect('?action=transaction');
						}
						else {echo translate('overdraft');}
					}
				}
				if(($_GET['subaction'] == 'check') AND ($_GET['token'] == $_SESSION['token'])) {
					$retour = $bdd->prepare('SELECT * FROM transactions WHERE id=?');
					$retour->execute(array($_GET['id']));
					$data = $retour->fetch();
					if($_GET['statut'] == 1) {
						sql('UPDATE transactions SET statut = 1 WHERE id=?', array($_GET['id']));
						sql('UPDATE user SET wallet = wallet + ? WHERE id=?', array($data['value'], $_SESSION['user_id']));
						sql('UPDATE user SET wallet = wallet - ? WHERE id=?', array($data['value'], $data['t_from']));
						redirect('?action=transaction');
					}
					if($_GET['statut'] == 2) {
						sql('UPDATE transactions SET statut = 2 WHERE id=?', array($_GET['id']));
						redirect('?action=transaction');
					}
				}
			}
			else {
				echo '<table>';
				echo '<tr><th>'.translate('from').'</th><th>'.translate('to').'</th><th>'.translate('value').'</th><th>'.translate('text').'</th><th>'.translate('date').'</th><th></th></tr>';
				$retour = $bdd->prepare('SELECT * , t.id AS t_id, u_from.pseudo AS from_pseudo, u_to.pseudo AS to_pseudo
				FROM transactions t
				LEFT JOIN user u_from ON t.t_from = u_from.id
				LEFT JOIN user u_to ON t.t_to = u_to.id
				WHERE (t.t_from=? OR t.t_to=?)
				ORDER BY t.time DESC');
				$retour->execute(array($_SESSION['user_id'], $_SESSION['user_id']));
				$retour->execute();
				while($data = $retour->fetch()) {
					$command = href('?action=transaction&subaction=check&id='.$data['t_id'].'&token='.$_SESSION['token'].'&statut=1', '&#10003;').
					href('?action=transaction&subaction=check&id='.$data['t_id'].'&token='.$_SESSION['token'].'&statut=2', '&#10060;');
					
					if($data['t_from'] != $_SESSION['user_id']) {
						$statut = ($data['statut'] == 0) ? $command : (($data['statut'] == 1) ? '&#10003;' : '&#10060;');
						$value = ($data['value'] > 0) ? '<span style="color:green">+'.abs($data['value']).'</span>' : '<span style="color:red">-'.abs($data['value']).'</span>';
					}
					else {
						$statut = ($data['statut'] == 0) ? '…' : (($data['statut'] == 1) ? '&#10003;' : '&#10060;');
						$value = ($data['value'] > 0) ? '<span style="color:red">-'.abs($data['value']).'</span>' : '<span style="color:green">+'.abs($data['value']).'</span>';
					}
					
					
					echo '<tr>';
					echo '<td>'.$data['from_pseudo'].'</td>';
					echo '<td>'.$data['to_pseudo'].'</td>';
					echo '<td>'.$value.'</td>';
					echo '<td>'.$data['text'].'</td>';
					echo '<td>'.date('d/m/Y', $data['time']).'</td>';
					echo '<td>'.$statut.'</td>';
					echo '</tr>';
				}
				echo '</table>';
			}
		}
	break;
	case 'mention':
		include 'app/tpl/mention.php';
	break;
	case 'market':
		if(isset($_SESSION['login'])) {
			if(isset($_GET['subaction'])) {
				if($_GET['subaction'] == 'edit') {
					if(isset($_GET['id'])) {
						$retour = $bdd->prepare('SELECT * FROM market WHERE id=?');
						$retour->execute(array($_GET['id']));
						$data = $retour->fetch();
						$text = $data['text'];
						$price = $data['price'];
						$id = $_GET['id'];
					}
					else {
						$text = null;
						$price = null;
						$id = 0;
					}
					$form = New form();
					$form->startform(array('method'=>'post', 'action'=>'?action=market&subaction=edit'));
					$form->label('price',  translate('value'));
					$form->input(array('type'=>'number', 'name'=>'price', 'value'=>$price));
					$form->label('text',  translate('text'));
					$form->textarea(array('name'=>'text', 'placeholder'=>'Lorem'), $text);
					$form->input(array('type'=>'hidden', 'name'=>'id', 'value'=>$id));
					$form->input(array('type'=>'submit'));
					$form->endform();
					if(isset($_POST['price']) && isset($_POST['text'])) {
						if($_POST['id'] == 0) {
							sql('INSERT INTO `market`(`id_user`, `text`, `price`, `time`) VALUES (?, ?, ?, ?)', array($_SESSION['user_id'], strip_tags($_POST['text']),$_POST['price'], time()));
							redirect('?action=market&subaction=manage');
						}
						else {
							sql('UPDATE `market` SET `text`=?,`price`=?,`time`=? WHERE id=?', array(strip_tags($_POST['text']),$_POST['price'], time(), $_POST['id']));
							redirect('?action=market&subaction=manage');
						}
					}
				}
				if($_GET['subaction'] == 'manage') {
					echo href('?action=market&subaction=edit', translate('addmarket'));
					$retour = $bdd->prepare('SELECT * FROM market WHERE id_user=? ORDER BY time');
					$retour->execute(array($_SESSION['user_id']));
					echo '<table>';
					while($data = $retour->fetch()) {
						echo '<tr>';
						echo '<td>'.href('?action=market&subaction=edit&id='.$data['id'], translate('editmarket')).'</td>';
						echo '<td>'.$data['text'].'</td>';
						echo '<td>'.$data['price'].'ŧ</td>';
						echo '<td>'.date('d/m/Y', $data['time']).'</td>';
						echo '</tr>';
					}
					echo '</table>';
				}
			}
			else {
				$retour = $bdd->query('SELECT m.*, u.pseudo FROM market m, user u WHERE m.id_user = u.id ORDER BY time');
				echo '<table>';
				while($data = $retour->fetch()) {
					echo '<tr>';
					echo '<td>'.href('?action=contact&id='.$data['id_user'], $data['pseudo']).'</td>';
					echo '<td>'.$data['text'].'</td>';
					echo '<td>'.$data['price'].'ŧ</td>';
					echo '<td>'.date('d/m/Y', $data['time']).'</td>';
					echo '</tr>';
				}
				echo '</table>';
			}
		}
	break;
	case 'contact':
		if(isset($_GET['id'])) {
			$form = New form();
			$form->startform(array('method'=>'post', 'action'=>'?action=contact'));
			$form->label('text',  translate('text'));
			$form->textarea(array('name'=>'text', 'placeholder'=>'Lorem'), null);
			$form->input(array('type'=>'hidden', 'name'=>'id', 'value'=>$_GET['id']));
			$form->input(array('type'=>'submit'));
			$form->endform();
			if(isset($_POST['text'])) {
				$retour = $bdd->prepare('SELECT email FROM user WHERE id=?');
				$retour->execute(array($_POST['id']));
				$data = $retour->fetch();
				$retour2 = $bdd->prepare('SELECT email FROM user WHERE id=?');
				$retour2->execute(array($_SESSION['user_id']));
				$data2 = $retour->fetch();
				send_mail($data['email'],$data2['email'], '[CONTACT FROM TOKEN TIME FOR MARKET]', $_POST['text']);
				redirect('?action=market');
			}
		}
	break;
	case 'class':
		$plugins= scandir('app/inc/app/');
		$a=explode('.php',$_GET['subaction']);
		if (isset($_GET['subaction'])) {
			if (file_exists('app/inc/app/'.$a[0].'.php')) {
				include 'app/inc/app/'.$a[0].'.php';
			}
	}
	break;
	}
}
else {
	include 'app/tpl/index.php';
}
include 'app/tpl/footer.php';
?>