<?php
include 'app/inc/system.php';
include 'app/inc/i18n/'.LANG.'.php';
session_start();
if(isset($_GET['id'])) {
	if(isset($_GET['action'])) {
		if($_GET['action'] == 'give') {
			if(isset($_SESSION['user_id'])) {
				$retour = $bdd->prepare('SELECT wallet FROM user WHERE id=?');
				$retour->execute(array($_SESSION['user_id']));
				$data = $retour->fetch();
				if(($data['wallet']-1) < 0) {echo translate('overdraft');}
				else {
					$text = isset($_GET['text']) ? $_GET['text'] : '';
					sql('INSERT INTO transactions(t_from, t_to, value, text, time, statut) VALUES(?, ?, ?, ?, ?, ?)', array($_SESSION['user_id'],$_GET['id'],1, 'Don &#359;&#239;&#7549;&#949; :'.$text, time(), 0));
					echo "&#359;&#239;&#7549;&#949;";
				}
			}
			else {
				echo '<script type="text/javascript">window.open("index.php");</script>';
				echo '<script type="text/javascript">window.history.back()(-1);</script>';
				
			}
		}
	}
	else {
		$text = isset($_GET['text']) ? $_GET['text'] : '';
		?>
		<a href="don.php?action=give&id=<?php echo $_GET['id']; ?>&text=<?php echo $text; ?>" title="Love it" class="b">&#359;&#239;&#7549;&#949;</a>
		<?php
	}
}
?>
<style>
.b {
	box-shadow:inset 50px 50px 50px 50px #ffffff;
	background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
	background-color:#ffffff;
	border-radius:41px;
	border:1px solid #dcdcdc;
	display:inline-block;
	cursor:pointer;
	color:#666666;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
	box-sizing:border-content;
	text-shadow:50px 50px 50px #ffffff;
}
.b:hover {
	background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
	background-color:#f6f6f6;
}
.b:active {
	position:relative;
	top:1px;
}
</style>