<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<link href='http://fonts.googleapis.com/css?family=Tangerine' rel='stylesheet' type='text/css'>
<link rel="stylesheet"  type="text/css" href="app/tpl/design.css">
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>&#358;oken &#358;ime</title>
</head>
<body>
<header>
	<h1><a href="?">&#358;oken &#358;ime</a></h1>
	<div class="connexion">
	<?php
		if(!isset($_SESSION['login'])) {
			echo '<span class="button-signup">'.href('?action=signup', translate('join')).'</span>';
			$form = New form();
			$form->startform(array('method'=>'post', 'action'=>'?action=signin'));
			$form->input(array('type'=>'email', 'name'=>'email', 'required'=>'required', 'placeholder'=>'john.doe@example.org'));
			$form->input(array('type'=>'password', 'name'=>'password', 'required'=>'required', 'placeholder'=>'d0_42D!'));
			$form->input(array('type'=>'submit'));
			$form->endform();
		}
		else {
			echo '<div class="box-connected">';
			if(isset($_SESSION['login'])) {echo href('?action=profile', '<b>'.$_SESSION['login'].'</b>');}
			if(isset($_SESSION['login'])) {echo href('?action=logout', translate('logout'));}
			$retour = $bdd->prepare('SELECT wallet from user WHERE id=?');
			$retour->execute(array($_SESSION['user_id']));
			$data = $retour->fetch();
			echo href('?action=transaction', $data['wallet'].'ŧ');
			echo '</div>';
		}
	?>
</div>
	<nav>
<?php if(!isset($_SESSION['login'])) {echo href('?action=signup', translate('signup'));} ?>
<?php if(!isset($_SESSION['login'])) {echo href('?action=signin',translate('signin'));} ?>
<?php if(isset($_SESSION['login'])) {echo href('?action=transaction',translate('transactions'));} ?>
<?php if(isset($_SESSION['login'])) {echo href('?action=transaction&subaction=send',translate('send_money'));} ?>
<?php if(isset($_SESSION['login'])) {echo href('?action=market',translate('market'));} ?>
<?php if(isset($_SESSION['login'])) {echo href('?action=market&subaction=manage',translate('managemarket'));} ?>
<?php echo href('?action=mention', translate('mention')); ?>
</nav>
</header>

<main>