<?php
require_once('vendor/autoload.php');
require_once('lib/class.Bsky.php');
require_once('lib/Mastodon.php');
include 'config.php';
if(!is_dir('cache')) { mkdir('cache');}
if(!is_dir('data')) { mkdir('data');}

if(isset($config['bsky'])) {
	$BlueskyClient = new Bsky($config['bsky']['username'], $config['bsky']['password']);
	$bsky_connect = $BlueskyClient->connect();
	if(! $BlueskyClient->connected) {
		print_r($bsky_connect['_curl']);
		print_r($bsky_connect['_header']);
		print "error connecting to bsky: ".$bsky_connect['_curl']['http_code']."\n";
		exit;
	}
} 
if(isset($config['mastodon'])) {
	$mastodon = new MastodonAPI($config['mastodon']['token'], $config['mastodon']['server']);
		$statusData = [
		'visibility'     => $config['mastodon']['visibility'],
		'language'    => $config['mastodon']['language'],
	];
	$statusData['status'] = '';
}

foreach($config['feeds'] as $config_feed) {
	$feed = new \SimplePie\SimplePie();
	$feed->set_feed_url($config_feed);
	$feed->set_cache_location('cache');
	$feed->set_cache_duration(3600);
	$feed->init();
	$feed->handle_content_type();

	$hashes = file_exists('data/hash.txt') ? file_get_contents('data/hash.txt') : '';

	foreach ($feed->get_items() as $item) {
		$hash = md5($item->get_permalink());
		if (strpos($hashes, $hash) === false) {
			$post = '';
			$categories = $item->get_categories();
		    if ($categories) {
		        foreach ($categories as $category) {
		            $post .= '#'.$category->get_term() . ' ';
		        }
		    }
		    $post .= $item->get_title();
		    $post .= strip_tags($item->get_description());
		    
		    $post_mastodon = substr($post, -475).' '.$item->get_permalink();
		    $post_bluesky = substr($post, -275).' '.$item->get_permalink();
		    
		    if(isset($config['mastodon'])) {
				$statusData['status'] = $post_mastodon;
				$result = $mastodon->postStatus($statusData);
				print_r($result);
			}
			/*
			if(isset($config['bsky'])) {
				$response = $BlueskyClient->post($post_bluesky, [$config['bsky']['lang']]);
				print_r($response);
			} */


		    file_put_contents('data/hash.txt', $hash . PHP_EOL, FILE_APPEND);
		}
	}
}
