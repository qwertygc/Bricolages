<?php
$config['bsky']['username'] = 'XXXX.bsky.example'; // your bsky username
$config['bsky']['password'] = 'xxx-xxx-xxx'; // app-password to your bsky account (see https://bsky.app/settings/app-passwords)
$config['bsky']['lang'] = 'fr-FR'; 

$config['mastodon']['token'] = ''; /// Token of your Mastodon bot account
$config['mastodon']['server'] = '';  // URL of your instance (Do not include '/' at the end.)
$config['mastodon']['visibility'] = 'public'; // "Direct" means sending message as a private message. The four tiers of privacy for toots are public , unlisted, private, and direct
$config['mastodon']['language'] = 'fr'; // en for English, zh for Chinese, de for German etc.


$config['feeds'] = [
];
