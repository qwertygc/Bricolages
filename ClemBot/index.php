<?php
if(!file_exists('Mastodon_api.php')) {file_put_contents('Mastodon_api.php', file_get_contents('https://raw.githubusercontent.com/yks118/Mastodon-api-php/master/Mastodon_api.php'));} else {include 'Mastodon_api.php';}
require_once 'config.php';
/*define('INSTANCE', '');
define('EMAIL', '');
define('PASSWORD', '');*/
function smoothie() {
	$fruits = array(
		'Abricot',
		'Ananas',
		'Avocat',
		'Banane',
		'Cassis',
		'Cerise',
		'Chataigne',
		'Citron',
		'Clémentine',
		'Coing',
		'Datte',
		'Figue',
		'Fraise',
		'Framboise',
		'Griotte',
		'Groseille',
		'Kaki',
		'Kiwaï',
		'Kiwi',
		'Mandarine',
		'Marron',
		'Melon',
		'Mûre',
		'Myrtille',
		'Nèfle',
		'Noisette',
		'Noix',
		'Orange',
		'Pamplemousse',
		'Pastèque',
		'Pêche',
		'Pistache',
		'Poire',
		'Pomme',
		'Pruneau',
		'Mirabelle',
		'Raisin',
		'Cacao',
		'Fruit de la passion',
		'Litchi',
		'Mangue',
		'Noix de coco',
		'Yuzu',
		'Aubergine',
		'Chayotte',
		'Tomate',
		'Courge',
		'Potiron',
		'Citrouille',
		'Courgette',
		'Piment',
		'Poivron',
		'Café',
		'Chocolat',
		'Vanille',
		'Menthe',
		'Guimauve',
		'Chocolatine',
		'Patate douce',
		'Rhubarbe',
		'Céleri Rave',
		'Épinard',
		'Betterave',
		'Noix de Cajou',
		'Miel',
		'Lait',
		'Lait végétal',
		'Thé Matcha',
		'Grenade',
		"Fleurs d'oranger",
		"flocons d'avoine",
		"Cacao",
		'Chou Khale',
		'Amandes',
	);
	$rand = array_rand($fruits, 3);
	return $fruits[ $rand[0] ].'-'.$fruits[ $rand[1] ].'-'.$fruits[ $rand[2] ];
}
$mastodon_api = new Mastodon_api();
$mastodon_api->set_url('https://'.INSTANCE.'/');
$mastodon_api->set_client($create_app['html']['client_id'],$create_app['html']['client_secret']);
$login = $mastodon_api->login(EMAIL, PASSWORD);
$mastodon_api->set_token($login['html']['access_token'],$login['html']['token_type']);
$mastodon_api->post_statuses(array('status'=>'Qui veut un 🍹 smoothie '.smoothie().' ?'));
$notifications = $mastodon_api->notifications();
foreach($notifications['html'] as $toots) {
	if($toots['type'] == 'mention') {
		if(strtotime($toots['created_at']) >= file_get_contents('latest')) {
			$mastodon_api->post_statuses(array('status'=>'Prends donc ce smoothie 🍹'.smoothie().' @'.$toots['account']['username'].' !', 'in_reply_to_id'=>$toots['status']['id']));
			file_put_contents('latest', time());
		} 
	}	
}
