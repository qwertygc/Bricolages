<!DOCTYPE html>
<html lang="en">
<head>
    <title>Editor</title>
<style>
body {
max-width:800px;
margin:auto;
}
textarea, input {
	border: none;
	font-family: inherit;
	font-size: inherit;
	font-weight: inherit;
	line-height: inherit;
	-webkit-appearance: none;
	border: 1px #ccc solid;
	border-radius: 5px;
	padding: 6px 4px;
	outline: none;
	border-radius: 2px;
	color: #666;
	margin: 0;
	box-sizing: border-box;
	background: #fff;
	display: block;
	width:100%;
	border-color:transparent;
}
textarea:focus {
color:#000;
}
.notif {
border-radius:10px;
padding:10px;
margin:10px;
background-color:transparent;
}
</style>
</head>
<body>
     		<strong>Paragraphs: </strong><span id="result__paragraphs">0</span> - <strong>Words: </strong><span id="result__words">0</span> - <strong>Characters: </strong><span id="result__characters">0</span> - <strong>Characters (with spaces): </strong><span id="result__all">0</span>
				<div class="notif" id="notif"></div>
        <textarea id="textarea" rows="20" cols="40" autofocus placeholder="Start entering some text here"></textarea>
    <script src="http://raw.github.com/RadLikeWhoa/Countable/master/Countable.js"></script>
	<script>
	      var area = document.getElementById('textarea'),
          results = {
            paragraphs: document.getElementById('result__paragraphs'),
            words: document.getElementById('result__words'),
            characters: document.getElementById('result__characters'),
            all: document.getElementById('result__all')
          }

      Countable.live(area, function (counter) {
        if ('textContent' in document.body) {
          results.paragraphs.textContent = counter.paragraphs
          results.words.textContent = counter.words
          results.characters.textContent = counter.characters
          results.all.textContent = counter.all
        } else {
          results.paragraphs.innerText = counter.paragraphs
          results.words.innerText = counter.words
          results.characters.innerText = counter.characters
          results.all.textContent = counter.all
        }
		if(counter.words == "10") {notif('#2ECC71','Bravo, tu a atteint les 10 mots !');}
		if(counter.words == "100") {notif('#2ECC71', 'Bravo, tu a atteint les 100 mots !');}
		if(counter.words == "250") {notif('#2ECC71', 'Bravo, tu a atteint les 250 mots !');}
		if(counter.words == "400") {notif('#2ECC71', 'Bravo, tu a atteint les 400 mots !');}
		if(counter.words == "500") {notif('#2ECC71', 'Bravo, tu a atteint les 500 mots !');}
		if(counter.words == "750") {notif('#9B59B6', '<b>Trophé 3Pages.fr</b> : Bravo, tu a atteint les 750 mots !');}
		if(counter.words == "1000") {notif('#2ECC71', 'Bravo, tu a atteint les 1000 mots !');}
		if(counter.words == "1667") {notif('#9B59B6', '<b>Un jour NaNoWriMo</b> : Bravo, tu a atteint les 1667 mots !');}
		if(counter.words == "50000") {notif('#9B59B6', '<b>Trophé NaNoWriMo</b> : Bravo, tu a atteint les 50 000 mots !');}
		
      })

	function notif(color, message) {
		document.getElementById('notif').innerHTML = message;
		document.getElementById('notif').style.backgroundColor = color;
		setTimeout(function() { document.getElementById('notif').innerHTML = ''; document.getElementById('notif').style.backgroundColor = "transparent";}, 7000);
	}
	</script>
	<p>Le constat est simple : il est difficile de rédiger de longs articles. Ainsi, ce plugin va vous fixer des objectifs à atteindre : 750 mots (comme sur 3pages.fr), 1000 mots ou 50000 mots (comme sur NaNoWriMo).</p>
	</body>
</html>