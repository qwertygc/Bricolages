<?php
include 'app/functions.php';
##### UPLOAD ######
// curl -i -X POST -H "Content-Type: multipart/form-data"  -F "image=@4ba9b82ec3b76fff.png" http://qwerty.legtux.org/varia/wiki/api.php?do=upload
if(isset($_FILES['image'])) {
	$uploaddir = 'data/upload/';
	$filename = int_to_alph(time()).'_'.basename($_FILES['image']['name']);
	$uploadfile = $uploaddir.$filename;
	if(in_array($_FILES['image']['type'],$blacklist_mimetype)) {
		echo json_encode(array('error'=>'typeNotAllowed'));
	}
	if(!is_uploaded_file($_FILES['image']['tmp_name'])) {
		echo json_encode(array('error'=>'importError'));
		echo "can't upload";
	}
	if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
		echo json_encode(array('data'=> array('filePath'=>'data/upload/'.$filename)));
	}
}
