<?php
$blacklist_mimetype = array (
	'text/html',
	'text/javascript',
	'text/x-javascript',
	'application/x-shellscript',
	'application/x-php',
	'text/x-php',
	'text/x-python',
	'text/x-perl',
	'text/x-bash',
	'text/x-sh',
	'text/x-csh',
	'text/scriptlet',
	'application/x-msdownload',
	'application/x-msmetafile'
);

function int_to_alph($int) {
	$chrs = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '-', '_');
	$alph = null;
	$base = sizeof($chrs);
	do {
		$alph = $chrs[($int % $base)] . $alph;
	}
	while($int = intval($int / $base));
	return $alph;
}
if(!file_exists('data')) {
	mkdir('data/');
}
if(!file_exists('data/content')) {
	mkdir('data/content/');
}
if(!file_exists('data/upload')) {
	mkdir('data/upload/');
}
function create_html($title, $content) {
$txt = '
<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
	<title>'.$titme.'</title>
	<style>
	
body {
	background:transparent;
	color: #424242;
	font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu, Arial, "Open Sans", "Noto Sans", "Fira Sans", "Droid Sans", Cantarell,"Helvetica Neue",sans-serif,"Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Emoji Regular";
	font-size:1em;
	line-height:1.5;
	margin:auto;
	text-rendering:optimizelegibility;
	max-width:800px;
	margin:auto;
}

blockquote {
	border-left:5px solid #ccc;
	margin-bottom:1em;
	padding:1em 1em 1em 5em;
}

	img {
		max-width:100%;
	}
	a {
		color:#4292c6;
	}
	</style>
</head>
<body><h1>'.$title.'</h1>'.$content.'</body>
</html>';
	return $txt;
}
